package com.pineapple.filter.book.jdbc;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.pineapple.util.Utility;

public class HibernateUtil {
	private static SessionFactory sessionFactory = null;
	private static Session session;

	public static List<Object> getObjList(String sql) {
		Query query = null;
		try {
			query = getSession().createQuery(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Object> ret = query.list();
		closeSession();
		return ret;
	}

	public static void saveObject(Object obj) {
		Transaction transaction = null;
		try {
			transaction = getSession().beginTransaction();
			getSession().save(obj);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			closeSession();
			Utility.log(Utility.LOG_ERROR, HibernateUtil.class, "save object failed." + e);
		}
	}

	protected static Session getSession() {
		session = HibernateUtil.getSessionFactory().openSession();
		return session;
	}

	protected static SessionFactory getSessionFactory() {
		sessionFactory = buildSessionFactory();
		return sessionFactory;
	}

	protected static void closeSession() {
		session.close();
		sessionFactory.close();
	}

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration config = new Configuration().configure();
			final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
			return config.buildSessionFactory(serviceRegistry);
		} catch (HibernateException ex) {
			Utility.log(Utility.LOG_ERROR, HibernateUtil.class, "Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
}