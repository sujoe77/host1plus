package com.pineapple.filter.book.jdbc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class DbUtil {
	public static <T, E> List<T> getPropertyList(Collection<E> collection, ObjIntf<T, E> oi) {
		List<T> ret = new ArrayList<T>();
		Iterator<E> it = collection.iterator();
		while (it.hasNext()) {
			ret.add(oi.getItem(it.next()));
		}
		return ret;
	}

	public interface ObjIntf<T, E> {
		public T getItem(E obj);
	}
}
