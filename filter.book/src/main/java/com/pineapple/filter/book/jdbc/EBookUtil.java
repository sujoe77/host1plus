package com.pineapple.filter.book.jdbc;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.pineapple.persistent.book.Category;
import com.pineapple.persistent.book.EBook;


public class EBookUtil {
	private static final String SELECT_CATEGORY = "FROM Category ORDER BY CATEGORY";
	public static final String AND = "AND";
	public static final String OR = "OR";	

	public static List<EBook> getBooks(String keywords, String seperator) {
		String relation = ",".equals(seperator) ? AND : OR;
		return getBookListByKeyword(Arrays.asList(keywords.split(seperator)), relation);
	}

	public static List<EBook> getBookListByKeyword(List<String> keywords, String relation) {
		return (List) HibernateUtil.getObjList(getBookHqlByKeyword(keywords, relation));
	}
	
	public static List<EBook> getBookListByCategory(String[] categories) {
		return getBookListByCategory(Arrays.asList(categories));
	}

	public static List<EBook> getBookListByCategory(List<String> categories) {
		return (List) HibernateUtil.getObjList(getBookHqlByCategory(categories));
	}

	public static List<String> getAllCategory() {
		List<Category> objList = (List) HibernateUtil.getObjList(SELECT_CATEGORY);
		return DbUtil.getPropertyList(objList, (Category c) -> c.getCategory());
	}

	private static String getBookHqlByKeyword(List<String> keywords, String relation) {
		String hql = "FROM EBook where ";
		StringBuilder sb = new StringBuilder(hql);
		for (String kw : keywords) {
			sb.append(" lower(title) like '%").append(kw.toLowerCase()).append("%' ").append(relation);
		}
		sb.delete(sb.length() - 3, sb.length());
		sb.append("order by publish_date desc, id desc");
		return sb.toString();
	}

	private static String getBookHqlByCategory(List<String> categories) {
		return StringUtils.join("FROM EBook where category in (", getCategoryStr(categories), ") order by publish_date desc, id desc");
	}

	private static String getCategoryStr(List<String> categories) {
		StringBuilder sb = new StringBuilder("");
		for (String category : categories) {
			sb.append("'").append(category).append("'").append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}
}
