<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="com.pineapple.filter.book.jdbc.EBookUtil,com.pineapple.persistent.book.EBook,java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		int i = 0;
		String kw = request.getParameter("keyword");
		if(kw != null){
			List<EBook> bookList = EBookUtil.getBooks(kw, kw.contains(",") ? "," : ":");
			out.println("Total found: " + bookList.size() +" records. <br/><br/><br/>");
			for (EBook book : bookList) {
				out.println(i + ". " + book.getCategory() + "   "
						+ book.getPublisDate().toString().substring(0, 11)
						+ "  <a href=\"http://www.ebook3000.com"
						+ book.getLink() + "\">" + book.getTitle()
						+ "</a><br/><br/>");
				i++;
			}
		}
	%>

	<%
		int j = 0;
		String[] values = request.getParameterValues("category");
		if (values != null) {
			List<EBook> bookList1 = EBookUtil.getBookListByCategory(values);
			out.println("Total found: " + bookList1.size() +" records. <br/><br/><br/>");
			for (EBook book : bookList1) {
				out.println(j + ". " + book.getCategory() + "   "
						+ book.getPublisDate().toString().substring(0, 11)
						+ "  <a href=\"http://www.ebook3000.com"
						+ book.getLink() + "\">" + book.getTitle()
						+ "</a><br/><br/>");
				j++;
			}
		}
	%>
</body>
</html>