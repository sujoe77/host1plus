<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="com.pineapple.filter.book.jdbc.EBookUtil,java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Categories</title>

</head>
<body>
	<form action="Books.jsp">
		<%
			out.println("Keywords: <input type=\"text\" name=\"keyword\" value=\"\"/>");
		%>
		<input type="submit" value="Submit" /><br />
		<br />
		<br />
		<br />
	</form>
	<form action="Books.jsp">
        <table>
		<%
			EBookUtil util = new EBookUtil();
			List<String> categories = EBookUtil.getAllCategory();
			int i = 0;
			for (String cat : categories) {
                if(i % 10 == 0){
                    %><tr><%
                }
                %><td><%
				out.println("<input type=\"checkbox\" name=\"category\" value=\"" + cat + "\"/>" + cat);
                %></td><%
				i++;
                if(i % 10 == 0){
                    %></tr><%
                }
			}
		%>
        </table>
		<input type="submit" value="Submit" />
	</form>
</body>
</html>