#!/bin/bash
NOW=$(date +"%Y-%m-%d-%H%M%S")
cd /opt/crawler
if [ $1 = "itjb" ]; then
	java -Dlog4j.configuration=log4j.properties -cp "./:./crawler-1.0.2.jar:lib/*" com.pineapple.web.crawler.controller.BasicCrawlController ITJobBankCrawler > /var/www/html/job/dk_itjobbank.txt &
elif [ $1 = "jobidx" ]; then
	java -Dlog4j.configuration=log4j.properties -cp "./:./crawler-1.0.2.jar:lib/*" com.pineapple.web.crawler.controller.BasicCrawlController JobIndexCrawler > /var/www/html/job/dk_jobidx.txt &
else
	java -Dlog4j.configuration=log4j.properties -cp "./:./crawler-1.0.2.jar:lib/*" com.pineapple.web.crawler.controller.BasicCrawlController MonsterDe > /var/www/html/job/monster.txt &
fi


