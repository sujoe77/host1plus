#!/bin/bash
NOW=$(date +"%Y-%m-%d-%H%M%S")
file=/var/www/html/job/monster.txt
minimumsize=10000
actualsize=$(wc -c "$file" | cut -f 1 -d ' ')
if [ $actualsize -le $minimumsize ]; then
   /opt/crawler/start.sh mon &
else
   cp /var/www/html/job/monster.txt /var/www/html/job/old/monster_$NOW.txt
fi

cp /var/www/html/job/dk_itjobbank.txt /var/www/html/job/old/dk_itjobbank_$NOW.txt
cp /var/www/html/job/dk_jobidx.txt /var/www/html/job/old/dk_jobidx_$NOW.txt

find /var/www/html/job -maxdepth 1 -type f  -mmin +1440 | xargs mv -t /var/www/html/job/old

#Extract groups
grep -i java /var/www/html/job/dk* | grep -iv javascript > /var/www/html/job/extract/dk_java.txt
grep ^C /var/www/html/job/dk* > /var/www/html/job/extract/dk_c.txt
grep ^Y /var/www/html/job/dk* > /var/www/html/job/extract/dk_y.txt
grep http /var/www/html/job/dk* | grep -v :C | grep -v :Y | grep -iv Deleting | grep -iv java > /var/www/html/job/extract/dk_other.txt

grep -i java /var/www/html/job/monster.txt | grep -iv javascript > /var/www/html/job/extract/de_java.txt
grep ^C /var/www/html/job/monster.txt > /var/www/html/job/extract/de_c.txt
grep ^Y /var/www/html/job/monster.txt > /var/www/html/job/extract/de_y.txt

cd /var/www/html/job/extract/
python job_htm.py dk_c.txt htm/dk_c.htm
python job_htm.py dk_java.txt htm/dk_java.htm
python job_htm.py dk_y.txt htm/dk_y.htm
python job_htm.py dk_other.txt htm/dk_other.htm

python job_htm.py de_c.txt htm/de_c.htm
python job_htm.py de_java.txt htm/de_java.htm
python job_htm.py de_y.txt htm/de_y.htm
