package com.pineapple.web.crawler.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;

import com.pineapple.web.crawler.util.Utility;

public class SysException extends Exception {

  private static final long serialVersionUID = 1L;

  protected final Exception originalException;
  protected final String source;
  protected final String code;
  protected final String message;

  public SysException(String source, String message, Exception originalException) {
    this("SYS", source, message, originalException);
  }
  
  public SysException(String code, String source, String message) {
    this(code, source, message, null);
  }

  public SysException(String code, String source, String message, Exception originalException) {
    this.code = code;
    this.source = source;
    this.message = message;
    this.originalException = originalException;
    handleException(code, source, message, originalException);
  }

  protected void handleException(String code, String source, String message, Exception originalException) {
    try {
      Utility.log(Utility.LOG_ERROR, getClass(), code + source + message + originalException.getMessage());
    } catch (Exception ex) {
      Utility.log(Utility.LOG_ERROR, getClass(), ex.getMessage());
    }
  }

  protected String getExceptionName() {
    return getClass().getSimpleName();
  }

  @Override
  public String toString() {
    String res = "\tError Code: " + code + "\n\tSource: " + source + "\n\tMessage: " + message;
    if (this.originalException != null) {
      res = res + "\nOriginal Error: \n" + originalException.toString();
      res += SysException.stackTraceAsString(originalException, false);
    }
    return res;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public boolean isSQlException() {
    return originalException instanceof SQLException;
  }

  public Exception getOriginalException() {
    return originalException;
  }

  public SQLException getOriginalSQLException() {
    if (originalException == null) {
      return null;
    }
    if (originalException instanceof SQLException) {
      return (SQLException) originalException;
    }
    return null;
  }

  public String getCode() throws SysException {
    return this.code;
  }

  public String getSource() {
    return this.source;
  }

  public static String stackTraceAsString(Exception ex, boolean singleLine) {
    String result = null;
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    ex.printStackTrace(pw);
    result = sw.toString();
    if (singleLine && (result.indexOf(10) > -1 || result.indexOf(13) > -1)) {
      result = result.replaceAll("\r", "").replaceAll("\n", "<EOL>;");
    }
    return result;
  }
}
