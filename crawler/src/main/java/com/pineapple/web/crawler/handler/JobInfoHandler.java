package com.pineapple.web.crawler.handler;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.pineapple.web.crawler.entity.JobInfo;
import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.task.BaseTask;
import com.pineapple.web.crawler.util.Config;
import com.pineapple.web.crawler.util.Utility;

import edu.uci.ics.crawler4j.crawler.Page;

/**
 * Created by zsu on 3/17/2015.
 */
public final class JobInfoHandler implements EntityHandler<JobInfo> {
	private static final String SKIP = "skip";
	private static final String SEPARATOR = "\t";
	private static List<String> jobIdList = new ArrayList<String>();
	private static final String Y = "Y";
	private static final String C = "C";
	private static JobInfoHandler instance;

	static String[] skipCompany = { "adecco", "perit", "Ratbacher", "Huxley", "Personal", "Studien", "humancouncil", "MINT", "Computer Futures", "Vesterling", "Recruitment", "Hays", "Universi",
			"Rekrut", "Recruiters", "Logos", "CoreSearch", "ReSound", "netcom", "terma", "dtu", "Recruitment", "Human Resources", "Kommune" };
	static String[] skipJob = { "python", "php", "Trainee", "support", "c#", ".net", "sap", "microsoft", "dsb", "graduate", "student", "ios ", "sharepoint", "UX", "intern", "test", "Freelance" };
	static String[] interestJob = { "Entwick", "Android", "programmer", "developer", "java", "udvikler", "engineer", "software" };
	static String[] interestCompany = { "Lufthansa", "Intel", "Apple", "Leica", "Amazon", "TEXAS INSTRUMENTS", "Kabel Deutschland", "AIRBUS", "audi", "bmw", "siemens", "Insurance", "sdc", "oracle",
			"siemens", "Deloitte", "novo", "bank", "nordea", "nnit", "microsoft", "pfa", "topdanmark", "maersk", "Skandinavisk Data", "PensionDanmark", "ftf", "fdc", "acce" };

	private Map<String, Object> paramMap;

	private JobInfoHandler() {
	}

	public static JobInfoHandler getInstance() throws SysException {
		if (instance == null) {
			instance = new JobInfoHandler();
			instance.configCompanyJobList();
		}
		return instance;
	}

	private void configCompanyJobList() throws SysException {
		skipCompany = Config.getProperty("company.skip").split(",");
		interestCompany = Config.getProperty("company.interest").split(",");
		skipJob = Config.getProperty("job.skip").split(",");
		interestJob = Config.getProperty("job.interest").split(",");
	}

	@Override
	public void setCustomData(Object param) throws Exception {
		this.paramMap = (Map<String, Object>) param;
	}

	@Override
	public void handleEntityList(List<JobInfo> jobInfoList) throws SysException, UnsupportedEncodingException {
		for (JobInfo jobInfo : jobInfoList) {
			handleEntity(jobInfo);
		}
	}

	@Override
	public void handleEntity(JobInfo jobInfo) throws SysException, UnsupportedEncodingException {
		boolean newAdded = getInstance().checkAndAddToJobIdList(jobInfo);
		outputJobOrSkipInfo(newAdded, jobInfo);
	}

	private boolean checkAndAddToJobIdList(JobInfo jobInfo) {
		String jobId = jobInfo.getJobId();
		Utility.log(Utility.LOG_DEBUG, JobInfoHandler.class, "jobid: " + jobId);
		if (jobIdList.contains(jobId)) {
			return false;
		} else {
			jobIdList.add(jobId);
			return true;
		}
	}

	private void outputJobOrSkipInfo(boolean newAdded, JobInfo jobInfo) throws UnsupportedEncodingException {
		String output = shouldOutputJobInfo(newAdded, jobInfo) ? getJobInfoOutPutStr(jobInfo) : getOutputSkipDebugInfo(jobInfo);
		// writeToLog(output);
		if (!output.contains("skip")) {
			PrintStream out = new PrintStream(System.out, true, "UTF-8");
			out.println(output);
		}
	}

	private void writeToLog(String output) {
		if (StringUtils.isNotBlank(output)) {
			String category;
			if (output.indexOf(SKIP) >= 0) {
				category = Utility.LOG_DEBUG;
			} else {
				category = Utility.LOG_INFO;
			}
			Utility.log(category, JobInfoHandler.class, output);
		}
	}

	private boolean shouldOutputJobInfo(boolean newAdded, JobInfo jobInfo) {
		return newAdded && !Utility.contains(jobInfo.getCompany(), skipCompany) && !Utility.contains(jobInfo.getTitle(), skipJob);
	}

	private String getOutputSkipDebugInfo(JobInfo jobInfo) {
		String ret = "";
		if (Utility.contains(jobInfo.getCompany(), skipCompany)) {
			ret = "skip company " + jobInfo.getCompany();
		} else if (Utility.contains(jobInfo.getTitle(), skipJob)) {
			ret = "skip job " + jobInfo.getTitle();
		}
		return ret;
	}

	private String getJobInfoOutPutStr(JobInfo jobInfo) {
		StringBuilder output = new StringBuilder("");
		output.append(getVipValue(jobInfo.getTitle(), jobInfo.getCompany()));
		output.append(SEPARATOR).append(jobInfo.getLocation());
		output.append(SEPARATOR).append(jobInfo.getDateStr());
		output.append(SEPARATOR).append(jobInfo.getCompany());
		output.append(SEPARATOR).append(jobInfo.getTitle());
		output.append(SEPARATOR);
		if (!jobInfo.getLink().startsWith("http")) {
			output.append("http://").append(paramMap.get(BaseTask.DOMAIN));
		}
		output.append(jobInfo.getLink()).append(SEPARATOR);
		output.append(((Page) paramMap.get(BaseTask.SRC_PAGE)).getWebURL().getURL());
		return output.toString();
	}

	private String getVipValue(String title, String company) {
		StringBuilder ret = new StringBuilder("");
		ret.append(Utility.contains(company, interestCompany) ? C : "").append(Utility.contains(title, interestJob) ? Y : "");
		return ret.toString();
	}
}
