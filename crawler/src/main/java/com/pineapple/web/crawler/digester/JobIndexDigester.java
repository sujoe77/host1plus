package com.pineapple.web.crawler.digester;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pineapple.web.crawler.entity.JobInfo;

/**
 * Created by zsu on 3/17/2015.
 */
public final class JobIndexDigester implements Digester {

	private static final String RESULT = "result";
	private static final String HREF = "HREF";
	private static final String CITE2 = "CITE";
	private static final String B = "b";
	private static final String A = "A";
	private static final String PAID_JOB = "PaidJob";
	private static JobIndexDigester instance;

	protected JobIndexDigester() {
	}

	public static JobIndexDigester getInstance() {
		if (instance == null) {
			instance = new JobIndexDigester();
		}
		return instance;
	}

	@Override
	public List<JobInfo> digest(String htm) {
		return digest(htm, null);
	}

	public List<JobInfo> digest(String htm, String baseUrl) {
		List<JobInfo> ret = new ArrayList<JobInfo>();
		Document doc = StringUtils.isEmpty(baseUrl) ? Jsoup.parse(htm) : Jsoup.parse(htm, baseUrl);
//		System.out.println(htm);
		Elements resultList = doc.getElementsByClass(PAID_JOB);
		for (Element e : resultList) {
			processItem(ret, e);
		}
		return ret;
	}

	private void processItem(List<JobInfo> ret, Element e) {
		JobInfo job = new JobInfo();
		Element aTitle = e.getElementsByTag(A).get(1);
		Element aCompany = e.getElementsByTag(A).get(2);
		Element bTitle = aTitle.getElementsByTag(B).first();
		Element bCompany = aCompany.getElementsByTag(B).first();
		Element cite = e.getElementsByTag(CITE2).get(0);
		String dateStr = cite.ownText().replace("Indrykket ", "");//.split(",")[1].trim();
		job.setTitle(bTitle.ownText());
		job.setCompany(bCompany.ownText());
		job.setDateStr(dateStr);

		String link = aTitle.attr(HREF);
		job.setLink(link);

		String jobId = link;
		jobId = jobId.substring(jobId.indexOf("jobid") + 5);
		jobId = jobId.substring(0, jobId.indexOf("&"));
		job.setJobId(jobId);
		String location = e.getElementsByTag("p").get(0).ownText().replace(",", "").trim();
		if (!location.isEmpty()) {
			job.setLocation(location);
		}
		if (!ret.contains(job)) {
			ret.add(job);
		}
	}

	@Override
	public Elements getResultListFromDoc(Document doc) {
		return doc.getElementsByClass(RESULT);
	}
}
