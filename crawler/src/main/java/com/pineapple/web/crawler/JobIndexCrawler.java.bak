package com.pineapple.web.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.pineapple.web.crawler.digester.ITJobBankDigester;
import com.pineapple.web.crawler.digester.JobIndexDigester;
import com.pineapple.web.crawler.entity.DataObject;
import com.pineapple.web.crawler.entity.JobInfo;
import com.pineapple.web.crawler.util.Utility;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 */
public class JobIndexCrawler extends BaseCrawler {
	private static final String DOMAIN = "http://it.jobindex.dk";
	//private static final String ROOT_URL = "http://it.jobindex.dk/job/it/systemudvikling/koebenhavn/";
	private static ArrayList<String> jobIdList = new ArrayList<String>();

/*	public JobIndexCrawler() {
		config = new CrawlConfig();
		config.setPolitenessDelay(300);
		config.setMaxDepthOfCrawling(-1);
		config.setMaxPagesToFetch(10000);
		config.setResumableCrawling(false);
		config.setIncludeBinaryContentInCrawling(false);
		root = ROOT_URL;
	}	*/

//    @Override
//    public void init() throws IOException {
//        super.init();
//        configCompanyJobList();
//    }
	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */
	@Override
	public boolean shouldVisit(WebURL url) {
		String href = url.getURL().toLowerCase();
//		String pHref = url.getParentUrl();
		Utility.Log(Utility.LOG_DEBUG, getClass(), href);
		if(href.toLowerCase().contains("http://it.jobindex.dk/cgi/jobsearch.cgi")
				&& href.toLowerCase().contains("supid=1&".toLowerCase())
				&& href.contains("regionid=20")
				&& href.contains("subid")
				&& !href.contains("jobage")
				){
            Utility.Log(Utility.LOG_DEBUG, getClass(), "should visit: " + href);
			return true;
		} else if(Utility.BIN_FILTERS.matcher(href).matches()){
			return false;
		}
		return false;
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page) {
		Utility.printDebugInfo(page);

        if (shouldProcessPage(page)) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();
            printProcessPageInfo(htmlParseData.getText(), html, htmlParseData.getOutgoingUrls());
            List<DataObject> jobList = JobIndexDigester.getInstance().digest(html);
            for (DataObject itJobBankJobInfo : jobList) {
                //handleJobInfo((JobInfo) itJobBankJobInfo);
            }
        }
        Utility.Log(Utility.LOG_DEBUG, getClass(), "=============");

        //-----------------------old code below--------------------------------//
		if (page.getParseData() instanceof HtmlParseData && !DRY_RUN) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			String text = htmlParseData.getText();
			String html = htmlParseData.getHtml();
			List<WebURL> links = htmlParseData.getOutgoingUrls();

			//System.out.println("Text length: " + text.length());
			//System.out.println("Html length: " + html.length());
			//System.out.println("Number of outgoing links: " + links.size());
			while(html.indexOf("class=\"PaidJob") >= 0 || html.indexOf("class=\"jix_robotjob") >= 0){
				boolean exist_in_jobidlist = false;
				html = removeBeforeJobLink(html);
				String link = Utility.getStrBefore(html,"\"");
				String jobId = link.substring(link.indexOf("jobid")+6, link.indexOf("&amp"));
				if(DEBUG){
					System.out.println("jobid: "+jobId);
				}
				if(jobIdList.contains(jobId)){
					exist_in_jobidlist = true;
				} else {
					jobIdList.add(jobId);
				}
				html = removeBeforeNextJobTitle(html);
				String title = Utility.getStrBefore(html,"<");
				html = removeBeforeCompany(html);
				String company = Utility.getStrBefore(html,"<");				
				html = removeBeforeDate(html);
				String dateStr = Utility.getStrBefore(html,"<");
				dateStr = dateStr.replaceAll("&nbsp;", " ");
				String vip = "";
				if(Utility.contains(company,interestCompany)){
					vip += "C";
				}
				
				if(Utility.contains(title,interestJob)){
					vip += "Y";
				}
				if(!exist_in_jobidlist && !Utility.contains(company,skipCompany) && !Utility.contains(title,skipJob)){
					System.out.println(vip + "\t" + dateStr + "\t" + company + "\t" + title + "\t" + DOMAIN + link);	
				} else {
					if(DEBUG){
						if(Utility.contains(company,skipCompany)){
							System.out.println("skip company "+company);
						} else if(Utility.contains(title,skipJob)){
							System.out.println("skip job "+title);
						}
					}
				}
			}
			//System.out.println(html);
		} else {
			//savePage(page);
		}
        Utility.Log(Utility.LOG_DEBUG, getClass(), "=============");
	}
	
	private String removeBeforeDate(String html){
		if(html.indexOf("<CITE>") >= 0){
			html = Utility.getStrAfter(html,"<CITE>");
		} else {
			html = Utility.getStrAfter(html,"<cite>");
		}
		return Utility.getStrAfter(html,", ");
	}
	
	private String removeBeforeCompany(String html){
		if(html.indexOf("linkcompany") >= 0){
			html = Utility.getStrAfter(html,"linkcompany\">");
		} else{
			html = Utility.getStrAfter(html,"<br><b>");
		}
		return html;
	}
	
	private String removeBeforeJobLink(String html){
		if(html.indexOf("class=\"PaidJob") >= 0){
			html = Utility.getStrAfter(html,"class=\"PaidJob");
			html = Utility.getStrAfter(html,"<A HREF=\"");
			html = Utility.getStrAfter(html,"<A HREF=\"");
		} else {
			html = Utility.getStrAfter(html,"class=\"jix_robotjob");
			html = Utility.getStrAfter(html,"<a href=\"");
		}
		return html;
	}
	
	private String removeBeforeNextJobTitle(String html){
		if(html.indexOf("<B>") >= 0){
			html = Utility.getStrAfter(html,"<B>");
		} else {
			html = Utility.getStrAfter(html,"<strong>");
		}
		return html;
	}
}
