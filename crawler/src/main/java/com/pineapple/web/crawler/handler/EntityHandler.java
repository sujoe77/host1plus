package com.pineapple.web.crawler.handler;

import java.util.List;

import com.pineapple.web.crawler.exception.SysException;

public interface EntityHandler<T> {
	String E_BOOK = "EBook";
	String JOB_INFO = "JobInfo";

	void handleEntityList(List<T> entityList) throws Exception;

	void handleEntity(T entity) throws Exception;

	void setCustomData(Object data) throws Exception;
}