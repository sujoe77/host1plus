package com.pineapple.web.crawler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pineapple.web.crawler.digester.Digester;
import com.pineapple.web.crawler.digester.DigesterFactory;
import com.pineapple.web.crawler.digester.JobIndexDigester;
import com.pineapple.web.crawler.entity.DataObject;
import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.handler.EntityHandler;
import com.pineapple.web.crawler.handler.HandlerFactory;
import com.pineapple.web.crawler.task.BaseTask;
import com.pineapple.web.crawler.util.Utility;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class BaseCrawler extends WebCrawler {
	protected static final boolean DEBUG = false;
	protected static final boolean DRY_RUN = false;
	protected BaseTask task;

	public BaseCrawler() {
	}

	@Override
	public void init(int id, CrawlController crawlController) {
		super.init(id, crawlController);
		try {
			this.task = (BaseTask) crawlController.getCustomData();
		} catch (Exception e) {
			Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
		}
	}

	@Override
	public boolean shouldVisit(Page page, WebURL url) {
		boolean ret = false;
		String href = url.getURL().toLowerCase();
		try {
			ret = Utility.matchPatternInList(href, task.getExcludePatternList()) ? false : Utility.matchPatternInList(href, task.getIncludePatternList()) ? true : task.isVisitByDefault();
		} catch (SysException e) {
			Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
		}
		/*
		 * if (ret) { System.out.println(href); System.out.println(ret); }
		 */
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void visit(Page page) {
		Utility.printDebugInfo(page);
		// System.out.println("Visting: " + page.getWebURL().getURL());
		if (shouldProcessPage(page)) {
			try {
				List<? extends DataObject> objList = getObjListFromPage(page);
				EntityHandler handler = HandlerFactory.getHandler(task.getTypeParameterClass());
				handler.setCustomData(getParamMap(page));
				handler.handleEntityList(objList);
			} catch (Exception e) {
				Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
			}
		}
		Utility.log(Utility.LOG_DEBUG, getClass(), "=====End of com.pineapple.web.crawler.EbookCrawler.visit ========");
	}

	private Map<String, Object> getParamMap(Page page) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseTask.DOMAIN, task.getDomain());
		paramMap.put(BaseTask.SRC_PAGE, page);
		return paramMap;
	}

	private List<? extends DataObject> getObjListFromPage(Page page) {
		HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
		String html = htmlParseData.getHtml();
		printProcessPageInfo(htmlParseData.getText(), html, htmlParseData.getOutgoingUrls());
		Digester<DataObject> digester = DigesterFactory.getDigester(task.getDomain());
		List<? extends DataObject> objList = digester instanceof JobIndexDigester ? ((JobIndexDigester) digester).digest(html, page.getWebURL().getURL()) : digester.digest(html);
		return objList;
	}

	protected boolean shouldProcessPage(Page page) {
		return page.getParseData() instanceof HtmlParseData && !DRY_RUN;
	}

	protected void printProcessPageInfo(String text, String html, Set<WebURL> links) {
		Utility.log(Utility.LOG_DEBUG, getClass(), "Text length: " + text.length());
		Utility.log(Utility.LOG_DEBUG, getClass(), "Html length: " + html.length());
		Utility.log(Utility.LOG_DEBUG, getClass(), "Number of outgoing links: " + links.size());
	}
}
