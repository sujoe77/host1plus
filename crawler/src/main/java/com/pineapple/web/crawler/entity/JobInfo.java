package com.pineapple.web.crawler.entity;

public class JobInfo extends DataObject {

	private static final long serialVersionUID = 1L;
	private String link;
	private String jobId;
	private String title;
	private String company;
	private String location;
	private String dateStr;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	@Override
	public boolean equals(Object jobInfo) {
		return jobInfo instanceof JobInfo && ((JobInfo) jobInfo).getJobId().equals(getJobId());
	}

	@Override
	public int hashCode() {
		return jobId.hashCode();
	}
}
