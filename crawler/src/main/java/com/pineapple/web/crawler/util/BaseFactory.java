package com.pineapple.web.crawler.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.pineapple.web.crawler.digester.DigesterFactory;
import com.pineapple.web.crawler.exception.SysException;

public class BaseFactory {
	private static final String KEY_VALUE_SEPERATOR = "\\|";
	private static final String SEPERATOR = ";";
	protected static Map<String, String> nameMap;

	protected static void init(String key) throws SysException {
		if (nameMap == null || nameMap.get(key) == null) {
			nameMap = new HashMap<String, String>();
			String confStr = Config.getProperty(key);
			String[] namePairs = confStr.split(SEPERATOR);
			for (String pair : namePairs) {
				String[] names = pair.split(KEY_VALUE_SEPERATOR);
				nameMap.put(names[0], names[1]);
			}
		}
	}

	public static <T> T getInstance(Class<T> clazz, String confKey, String key) {
		try {
			init(confKey);
			Object instance = Class.forName(nameMap.get(key)).newInstance();
			return (T) instance;
		} catch (Exception e) {
			e.printStackTrace();
			Utility.log(Utility.LOG_ERROR, DigesterFactory.class, String.format("Object for : %s not properly configured!", key));
			Utility.log(Utility.LOG_ERROR, DigesterFactory.class, ExceptionUtils.getStackTrace(e));
			return null;
		}
	}

}
