package com.pineapple.web.crawler.digester;

import com.pineapple.web.crawler.util.BaseFactory;

public final class DigesterFactory extends BaseFactory {
	public static final String CONF_KEY = "digesters";

	private DigesterFactory() {
	}

	public static Digester getDigester(String domainName) {
		return getInstance(Digester.class, CONF_KEY, domainName);
	}
}
