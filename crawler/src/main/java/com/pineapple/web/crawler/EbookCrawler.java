package com.pineapple.web.crawler;

import java.util.List;

import com.pineapple.web.crawler.digester.Digester;
import com.pineapple.web.crawler.digester.DigesterFactory;
import com.pineapple.web.crawler.entity.EBook;
import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.handler.HandlerFactory;
import com.pineapple.web.crawler.util.Utility;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;

public class EbookCrawler extends BaseCrawler {

	@Override
	public void visit(Page page) {
		Utility.log(Utility.LOG_DEBUG, getClass(), "====== " + getClass().getName() + " begin=======");
		Utility.printDebugInfo(page);
		if (shouldProcessPage(page)) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			String html = htmlParseData.getHtml();
			printProcessPageInfo(htmlParseData.getText(), html, htmlParseData.getOutgoingUrls());
			List<EBook> bookList = DigesterFactory.getDigester(Digester.EBOOK).digest(html);
			try {
				HandlerFactory.getHandler(EBook.class.getName()).handleEntityList(bookList);
			} catch (Exception e) {
				Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
			}
		}
		Utility.log(Utility.LOG_DEBUG, getClass(), "====== " + getClass().getName() + " end=======");
	}
}
