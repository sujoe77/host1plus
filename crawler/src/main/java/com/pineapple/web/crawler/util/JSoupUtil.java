package com.pineapple.web.crawler.util;

import java.util.List;

import org.jsoup.nodes.Element;

public class JSoupUtil {
    private JSoupUtil(){
    }
    
    public static Element getFirstElementOfClass(Element element, String className){
        return getElementByClassWithIndex(element, className, 0);
    }
    
    public static Element getFirstElementOfTag(Element element, String tagName){
        return getElementByTagWithIndex(element, tagName, 0);
    }
    
    public static Element getElementWithIndex(List<Element> eList, int index){
        return eList.size() > index ? eList.get(index) : null;
    }
    
    public static Element getElementByClassWithIndex(Element element, String className, int index){
        List<Element> elementList = element.getElementsByClass(className);
        return getElementWithIndex(elementList, index);
    }
    
    public static Element getElementByTagWithIndex(Element element, String tagName, int index){
        List<Element> elementList = element.getElementsByTag(tagName);
        return getElementWithIndex(elementList, index);
    }
}
