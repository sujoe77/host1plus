package com.pineapple.web.crawler.persistent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.pineapple.web.crawler.entity.EBook;
import com.pineapple.web.crawler.util.Utility;

public class HibernateApp {

    private HibernateApp() {
    }

    public static void main(String[] args)
    {
        Utility.log(Utility.LOG_INFO, HibernateApp.class, "Maven + Hibernate + MySQL");
        Session session = HibernateUtil.getSessionFactory().openSession();

//        String hql = "FROM EBook where category = 'It'";
//        Query query = session.createQuery(hql);
//        List<EBook> results = query.list();
        List<String> categories = new ArrayList<String>();
        categories.add("Newspaper");
        categories.add("Economics, Business");
        List<EBook> results = EBookUtil.getBookListByCategory(categories);
        for (EBook book : results) {
            Utility.log(Utility.LOG_INFO, HibernateApp.class, book.toString());
        }
        HibernateUtil.shutdown();
    }

    public static void saveObject(Session session) {
        session.beginTransaction();
        EBook book = createBook();
        session.save(book);
        session.getTransaction().commit();
    }

    private static EBook createBook() {
        EBook book = new EBook();
        book.setCategory("It");
        book.setTitle("hello, Java");
        book.setLink("the link");
        book.setDescription("see something");
        book.setPublisDate(new Date());
        return book;
    }
}
