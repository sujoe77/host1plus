package com.pineapple.web.crawler.task;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.util.Config;
import com.pineapple.web.crawler.util.Utility;

public class BaseTask {
	public static final String NOT_DEFINED = "NOT_DEFINED";
	public static final String VISIT_DEFAULT = ".visit.default";
	public static final String INCLUDE_URL_PATTERN = ".include.url.pattern";
	public static final String EXCLUDE_URL_PATTERN = ".exclude.url.pattern";
	public static final String DOMAIN = "DOMAIN";
	public static final String SRC_PAGE = "SRC_PAGE";

	private String root;
	private List<Pattern> includePatternList;
	private List<Pattern> excludePatternList;
	private boolean visitByDefault;
	private String domain;
	private String entityClassName;
	private String name;
	private String digesterName;
	private String handlerName;

	public BaseTask(String name) {
		this.name = name;
	}

	public String getTypeParameterClass() {
		return entityClassName;
	}

	public void setTypeParameterClass(String typeParameterClass) {
		this.entityClassName = typeParameterClass;
	}

	public void init() throws SysException {
		root = Config.getProperty(name + ".root");
		domain = Config.getProperty(name + ".domain");
		entityClassName = Config.getProperty(name + ".entityClass");
		configUrlPattern();
	}

	protected void configUrlPattern() throws SysException {
		final String propertyKey = name + VISIT_DEFAULT;
		final String propertyValue = Config.getProperty(propertyKey);
		visitByDefault = Boolean.valueOf(propertyValue);
		getIncludePatternList();
		getExcludePatternList();
	}

	public List<Pattern> getExcludePatternList() throws SysException {
		excludePatternList = getPatternsFromConfig(excludePatternList, EXCLUDE_URL_PATTERN);
		return excludePatternList;
	}

	public List<Pattern> getIncludePatternList() throws SysException {
		includePatternList = getPatternsFromConfig(includePatternList, INCLUDE_URL_PATTERN);
		return includePatternList;
	}

	protected List<Pattern> getPatternsFromConfig(List<Pattern> pList, String configKey) throws SysException {
		return pList == null ? getRegExpFromConfig(configKey) : pList;
	}

	protected List<Pattern> getRegExpFromConfig(String configKey) throws SysException {
		List<Pattern> ret = new ArrayList<Pattern>();
		String regex = Config.getProperty(name + configKey);
		if (StringUtils.isBlank(regex)) {
			return getDefaultRegExpList(ret);
		} else {
			return getConfiguredRegExp(ret, regex);
		}
	}

	protected boolean matchIncludeUrl(String url) throws SysException {
		return Utility.match(getIncludePatternList(), url);
	}

	protected boolean matchExcludeUrl(String url) throws SysException {
		return Utility.match(getExcludePatternList(), url);
	}

	protected List<Pattern> getConfiguredRegExp(List<Pattern> ret, String regex) {
		String[] pArray = regex.split(";");
		for (String str : pArray) {
			ret.add(Pattern.compile(str));
		}
		return ret;
	}

	protected List<Pattern> getDefaultRegExpList(List<Pattern> ret) {
		ret.add(Pattern.compile(NOT_DEFINED));
		return ret;
	}

	public boolean isVisitByDefault() {
		return visitByDefault;
	}

	public void setVisitByDefault(boolean visitByDefault) {
		this.visitByDefault = visitByDefault;
	}

	public void setIncludePatternList(List<Pattern> includePatternList) {
		this.includePatternList = includePatternList;
	}

	public void setExcludePatternList(List<Pattern> excludePatternList) {
		this.excludePatternList = excludePatternList;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public String getDigesterName() {
		return digesterName;
	}

	public void setDigesterName(String digesterName) {
		this.digesterName = digesterName;
	}

	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}
}
