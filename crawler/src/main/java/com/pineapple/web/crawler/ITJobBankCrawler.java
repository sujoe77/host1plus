package com.pineapple.web.crawler;

import java.util.List;

import com.pineapple.web.crawler.digester.Digester;
import com.pineapple.web.crawler.digester.DigesterFactory;
import com.pineapple.web.crawler.entity.JobInfo;
import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.handler.HandlerFactory;
import com.pineapple.web.crawler.util.Utility;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 */
public class ITJobBankCrawler extends BaseCrawler {

  @Override
  public boolean shouldVisit(Page page, WebURL url) {
    String href = url.getURL().toLowerCase();
    if (href.toLowerCase().contains(task.getRoot()) && !href.contains("orderby") && !href.contains("zoom")) {
      Utility.log(Utility.LOG_DEBUG, getClass(), "should visit: " + href);
      return true;
    } else if (Utility.BIN_FILTERS.matcher(href).matches()) {
      return false;
    }
    return false;
  }

  @Override
  public void visit(Page page) {
    Utility.log(Utility.LOG_DEBUG, getClass(), "====== " + getClass().getName() + " begin=======");
    Utility.printDebugInfo(page);
    if (shouldProcessPage(page)) {
      HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
      String html = htmlParseData.getHtml();
      printProcessPageInfo(htmlParseData.getText(), html, htmlParseData.getOutgoingUrls());
      List<JobInfo> jobList = DigesterFactory.getDigester(Digester.IT_JOB_BANK).digest(html);
      try {
        HandlerFactory.getHandler(JobInfo.class.getName()).handleEntityList(jobList);
      } catch (Exception e) {
        Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
      }
    }
    Utility.log(Utility.LOG_DEBUG, getClass(), "====== " + getClass().getName() + " end=======");
  }
}
