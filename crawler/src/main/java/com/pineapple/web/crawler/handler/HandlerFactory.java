package com.pineapple.web.crawler.handler;

import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.util.BaseFactory;

public final class HandlerFactory extends BaseFactory {
	public static final String CONF_KEY = "handlers";

	private HandlerFactory() {
	}

	public static EntityHandler getHandler(String className) throws SysException {
		return getInstance(EntityHandler.class, CONF_KEY, className);
	}
}