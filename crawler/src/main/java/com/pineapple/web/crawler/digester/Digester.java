package com.pineapple.web.crawler.digester;

import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public interface Digester<T> {
  String IT_JOB_BANK = "www.it-jobbank.dk";
  String JOB_INDEX = "it.jobindex.dk";
  String EBOOK = "www.ebook3000.com";
  List<T> digest(String htm);
  Elements getResultListFromDoc(Document doc);
}
