package com.pineapple.web.crawler.persistent;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.pineapple.web.crawler.util.Utility;

public class HibernateUtil {
	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static Session session;

	public static Session getSession() {
		if (session == null || !session.isOpen() || !session.isConnected()) {
			session = HibernateUtil.getSessionFactory().openSession();
		}
		return session;
	}

	public static List<Object> getObjList(String sql) {
		Query query = null;
		try {
			String hql = "FROM EBook where category = 'It'";
			query = getSession().createQuery(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Object> ret = query.list();
		session.close();
		return ret;
	}

	public static void saveObject(Object obj) {
		Transaction transaction = null;
		try {
			transaction = getSession().beginTransaction();
			getSession().save(obj);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			session.close();
			Utility.log(Utility.LOG_ERROR, HibernateUtil.class, "save object failed." + e);
			// throw e;
		}
	}

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration config = new Configuration().configure();
			final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
			return config.buildSessionFactory(serviceRegistry);
		} catch (HibernateException ex) {
			Utility.log(Utility.LOG_ERROR, HibernateUtil.class, "Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}
}