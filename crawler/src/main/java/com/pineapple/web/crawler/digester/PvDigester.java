package com.pineapple.web.crawler.digester;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pineapple.web.crawler.entity.PvInfo;

public class PvDigester implements Digester<PvInfo> {

	@Override
	public List<PvInfo> digest(String htm) {
		List<PvInfo> ret = new ArrayList<PvInfo>();
		Document doc = Jsoup.parse(htm);
		Elements resultList = getResultListFromDoc(doc);
		for (Element e : resultList) {
			processItem(ret, e);
		}
		return ret;
	}

	@Override
	public Elements getResultListFromDoc(Document doc) {
		return doc.getElementsByClass("vid_bloczk");
	}

	private void processItem(List<PvInfo> ret, Element e) {
		PvInfo v = new PvInfo();
		Element link = e.getElementsByClass("vb_title").get(0).getElementsByClass("link").get(0);
		Element b = link.getElementsByTag("b").get(0);
		v.setName(b.text());
		v.setLink(link.attr("href"));
		ret.add(v);
	}
}
