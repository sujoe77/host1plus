package com.pineapple.web.crawler.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EBook extends DataObject {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String title;
    private String category;
    private String link;
    private String description;
    private Date publisDate;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdf.format(publisDate);
        sb.append(dateStr);
        sb.append("\t");
        sb.append(category);
        sb.append("\t");
        sb.append(title);
        sb.append("\t");
        sb.append(link);
        sb.append("\t");
        sb.append(description);
        return sb.toString();
    }

    @Override
    public boolean equals(Object book) {
        return book instanceof EBook && ((EBook) book).getTitle().equals(getTitle());
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublisDate() {
        return publisDate;
    }

    public void setPublisDate(Date publisDate) {
        this.publisDate = publisDate;
    }
}
