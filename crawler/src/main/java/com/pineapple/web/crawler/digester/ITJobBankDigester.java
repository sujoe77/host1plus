package com.pineapple.web.crawler.digester;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pineapple.web.crawler.entity.JobInfo;

public class ITJobBankDigester implements Digester<JobInfo> {
	
	protected ITJobBankDigester(){
    }

//    public static ITJobBankDigester getInstance() {
//        INSTANCE;
//    }

    @Override
    public List<JobInfo> digest(String htm) {
        List<JobInfo> ret = new ArrayList<JobInfo>();
        Document doc = Jsoup.parse(htm);
        Elements resultList = getResultListFromDoc(doc);
        for (Element e : resultList) {
            processItem(ret, e);
        }
        return ret;
    }

    @Override
    public Elements getResultListFromDoc(Document doc) {
      return doc.getElementsByClass("result");
    }

    private void processItem(List<JobInfo> ret, Element e) {
      JobInfo job = new JobInfo();
      Element divLeft = e.getElementsByClass("table-row").get(0).getElementsByClass("left").get(0);
      Element divTitle = divLeft.getElementsByClass("title").get(0);
      Element a = divTitle.getElementsByTag("a").get(0);
      Elements pLocation = divLeft.getElementsByClass("location");
      job.setTitle(a.ownText());
      job.setCompany(divLeft.getElementsByClass("info").get(0).ownText());
      job.setDateStr(divLeft.getElementsByTag("time").get(0).attr("datetime"));
      job.setLink(a.attr("HREF"));
      job.setJobId(job.getLink().substring(21, 29));
      if (!pLocation.isEmpty()) {
          job.setLocation(divLeft.getElementsByClass("location").get(0).ownText());
      }
      if (!ret.contains(job)) {
          ret.add(job);
      }
    }
}
