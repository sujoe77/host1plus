package com.pineapple.web.crawler.persistent;

import java.util.ArrayList;
import java.util.List;

import com.pineapple.web.crawler.entity.EBook;

public class EBookUtil {
	
	public static List<EBook> getBookListByCategory(List<String> categories){
		String hql = getBookByCategoryHql(categories);
		List<Object> objList = HibernateUtil.getObjList(hql); 
		List<EBook> ret = new ArrayList<EBook>();
		for (Object eBook : objList) {
			ret.add((EBook) eBook);
		}
		return ret;
	  }

	private static String getBookByCategoryHql(List<String> categories) {
		StringBuilder sb = new StringBuilder("FROM EBook where category in (");
		sb.append(getCategoryStr(categories)).append(")");
		return sb.toString();
	}

	private static String getCategoryStr(List<String> categories) {
		StringBuilder sb = new StringBuilder("");
		for(String category : categories){
			sb.append("'").append(category).append("'");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
}
