package com.pineapple.web.crawler.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import com.pineapple.web.crawler.entity.PvInfo;

public class PvHandler implements EntityHandler<PvInfo> {

	private static final String REG = ".*\\S-\\S.*";

	public static void main(String[] args) {
		String str = " - ";
		String reg = "\\s-\\s";
		System.out.println(str.matches(reg));
	}

	@Override
	public void handleEntityList(List<PvInfo> entityList) throws Exception {
		PvInfo[] result = 
				entityList.stream().filter(p -> p.getName().matches(REG)).toArray(PvInfo[]::new);
				//.map(p -> printString(p));
		for (PvInfo p : result) {
			handleEntity(p);
		}
	}

	/*private PvInfo printString(PvInfo input) throws IOException {
		String out = input.getName() + " : " + input.getLink();
		Files.write(Paths.get("c:\\Users\\sujoe\\Downloads\\pv.txt"), out.getBytes(), StandardOpenOption.APPEND);
		System.out.println(out);
		// System.out.println(input.getName().matches("\\S-\\S"));
		return input;
	}*/

	@Override
	public void handleEntity(PvInfo entity) throws Exception {
		String out = String.format("%s%s%s%s",entity.getName()," : ",entity.getLink(),"\n");
		Files.write(Paths.get("c:\\Users\\sujoe\\Downloads\\pv.txt"), out.getBytes(), StandardOpenOption.APPEND);
		System.out.println(out);
	}

	@Override
	public void setCustomData(Object data) throws Exception {
		// TODO Auto-generated method stub

	}

}
