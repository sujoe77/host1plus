package com.pineapple.web.crawler.digester;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pineapple.web.crawler.entity.EBook;
import com.pineapple.web.crawler.util.JSoupUtil;
import com.pineapple.web.crawler.util.Utility;

public final class EBookDigester implements Digester<EBook> {
	private static final String DATE_FORMAT = "dd MMM yyyy";
	public static final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

	protected EBookDigester() {
	}

	public static EBookDigester getInstance() {
		return new EBookDigester();
	}

	@Override
	public List<EBook> digest(String htm) {
		List<EBook> ret = new ArrayList<EBook>();
		Document doc = Jsoup.parse(htm);
		List<Element> indexBoxList = doc.getElementsByClass("index_box");
		for (Element indexBox : indexBoxList) {
			EBook book = new EBook();
			book.setTitle(getBookTitle(indexBox));
			// Element indexBoxTools =
			// JSoupUtil.getFirstElementOfClass(indexBox, "index_box_tools");
			// String dateStr = indexBoxTools.ownText();
			// SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT,
			// Locale.ENGLISH);
			// Date date=null;
			// try {
			// date = sdf.parse(dateStr.substring(17,17 +
			// DATE_FORMAT.length()));
			// } catch (ParseException e) {
			// Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
			// }
			book.setPublisDate(getPublishDate(indexBox));
//			Element aCategory = getCategory(indexBox);
			book.setCategory(getCategoryText(indexBox));
//			Element indexBoxInfo = getIndexBoxInfo(indexBox);
			book.setDescription(getDescription(indexBox));
			book.setLink(getBookLink(indexBox));
			ret.add(book);
		}
		return ret;
	}

	private String getDescription(Element indexBox) {
		return getIndexBoxInfo(indexBox).ownText();
	}

	private String getCategoryText(Element indexBox) {
		return getCategory(indexBox).ownText();
	}

	private String getBookLink(Element indexBox) {
		return getATitle(indexBox).attr("href");
	}

	private Element getIndexBoxInfo(Element indexBox) {
		return JSoupUtil.getFirstElementOfClass(indexBox, "index_box_info");
	}

	private Element getCategory(Element indexBox) {
		return JSoupUtil.getFirstElementOfTag(getIndexBoxTools(indexBox), "a");
	}

	protected Date getPublishDate(Element indexBox) {
		Element indexBoxTools = getIndexBoxTools(indexBox);
		String dateStr = indexBoxTools.ownText();
		Date date = null;
		try {
			date = sdf.parse(dateStr.substring(17, 17 + DATE_FORMAT.length()));
		} catch (ParseException e) {
			Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
		}
		return date;
	}

	private Element getIndexBoxTools(Element indexBox) {
		return JSoupUtil.getFirstElementOfClass(indexBox, "index_box_tools");
	}

	protected String getBookTitle(Element indexBox) {
		Element aTitle = getATitle(indexBox);
		return aTitle.ownText();
	}

	private Element getATitle(Element indexBox) {
		Element indexBoxTitle = JSoupUtil.getFirstElementOfClass(indexBox, "index_box_title");
		Element aTitle = JSoupUtil.getFirstElementOfTag(indexBoxTitle, "a");
		return aTitle;
	}

	@Override
	public Elements getResultListFromDoc(Document doc) {
		return doc.getElementsByClass("index_box");
	}
}
