package com.pineapple.web.crawler.controller;

import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.persistent.HibernateUtil;
import com.pineapple.web.crawler.task.BaseTask;
import com.pineapple.web.crawler.util.Config;
import com.pineapple.web.crawler.util.Utility;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 */
public class BasicCrawlController {
	private static final String BASE_CRAWLER = "BaseCrawler";
	private static final String BASIC_CRAWL_CONTROLLER = "BasicCrawlController";
	private static final String CRAWLER_NUMBER = "crawler.number";
	private static final String STORAGE_FOLDER = "storage.folder";
	private static final String DEFAULT_CRAWLER_PACKAGE = "com.pineapple.web.crawler.";
	private static final String USER_AGENT_KEY = "user.agent";

	private BasicCrawlController() {
	}

	public static void main(String[] args) {
		try {
			if (checkArguments(args)) {
				BaseTask task = new BaseTask(args[0]);
				task.init();
				startCrawling(initController(task));
				HibernateUtil.shutdown();
			}
		} catch (SysException e) {
			Utility.log(Utility.LOG_ERROR, BasicCrawlController.class, e.getMessage());
		}
	}

	public static CrawlController initController(BaseTask task) throws SysException {
		try {
			/*
			 * crawlStorageFolder is a folder where intermediate
			 * crawl data is stored.
			 */
			String crawlStorageFolder = Config.getProperty(STORAGE_FOLDER);

			/*
			 * numberOfCrawlers shows the number of concurrent
			 * threads that should be initiated for crawling.
			 */
			CrawlConfig config = getConfig();
			config.setCrawlStorageFolder(crawlStorageFolder);

			/*
			 * Be polite: Make sure that we don't send more than 1
			 * request per second (1000 milliseconds between
			 * requests).
			 */
			// config.setPolitenessDelay(300);

			/*
			 * You can set the maximum crawl depth here. The default
			 * value is -1 for unlimited depth
			 */
			// config.setMaxDepthOfCrawling(-1);

			/*
			 * You can set the maximum number of pages to crawl. The
			 * default value is -1 for unlimited number of pages
			 */
			// config.setMaxPagesToFetch(10000);

			/*
			 * Do you need to set a proxy? If so, you can use:
			 * config.setProxyHost("proxyserver.example.com");
			 * config.setProxyPort(8080);
			 * 
			 * If your proxy also needs authentication:
			 * config.setProxyUsername(username);
			 * config.getProxyPassword(password); Comment by
			 * 
			 * tharindu...@gmail.com, Sep 15, 2013: Isn't it
			 * proxy.setProxyPassword(password) ?
			 */

			/*
			 * This config parameter can be used to set your crawl
			 * to be resumable (meaning that you can resume the
			 * crawl from a previously interrupted/crashed crawl).
			 * Note: if you enable resuming feature and want to
			 * start a fresh crawl, you need to delete the contents
			 * of rootFolder manually.
			 */
			// config.setResumableCrawling(crawler.RESUME);

			/*
			 * Instantiate the controller for this crawl.
			 */
			PageFetcher pageFetcher = new PageFetcher(config);
			RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
			robotstxtConfig.setEnabled(false);
			RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
			CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);
			controller.setCustomData(task);

			/*
			 * For each crawl, you need to add some seed urls. These
			 * are the first URLs that are fetched and then the
			 * crawler starts following links which are found in
			 * these pages
			 */

			// config.setIncludeBinaryContentInCrawling(true);
			addSeeds(controller, task);
			return controller;
		} catch (Exception e) {
			throw new SysException(BASIC_CRAWL_CONTROLLER, e.getMessage(), e);
		}
	}

	public static void startCrawling(CrawlController controller) throws SysException {
		/*
		 * Start the crawl. This is a blocking operation, meaning that
		 * your code will reach the line after this only when crawling
		 * is finished.
		 */
		controller.start(getCrawlerClass(BASE_CRAWLER), getNumberOfCrawler());
	}

	@SuppressWarnings("unchecked")
	private static Class<WebCrawler> getCrawlerClass(String simpleClassName) throws SysException {
		try {
			return (Class<WebCrawler>) Class.forName(getCrawlerClassName(simpleClassName));
		} catch (ClassNotFoundException e) {
			throw new SysException(BasicCrawlController.class.getName(), e.getMessage(), e);
		}
	}

	private static String getCrawlerClassName(String simpleClassName) {
		return DEFAULT_CRAWLER_PACKAGE + simpleClassName;
	}

	protected static int getNumberOfCrawler() throws SysException {
		return Integer.parseInt(Config.getProperty(CRAWLER_NUMBER));
	}

	protected static boolean checkArguments(String[] args) {
		if (args.length < 1) {
			Utility.log(Utility.LOG_INFO, BasicCrawlController.class, "Please input task name.");
			return false;
		}
		return true;
	}

	public static CrawlConfig getConfig() throws SysException {
		CrawlConfig config = new CrawlConfig();
		config.setPolitenessDelay(300);
		config.setMaxDepthOfCrawling(-1);
		config.setMaxPagesToFetch(10000);
		config.setResumableCrawling(false);
		config.setIncludeBinaryContentInCrawling(false);
		config.setUserAgentString(Config.getProperty(USER_AGENT_KEY));
		return config;
	}
	
	private static void addSeeds(CrawlController controller, BaseTask task){
		String[] seeds = task.getRoot().split(";");
		for(String seed : seeds){
			controller.addSeed(seed);			
		}		
	}
}
