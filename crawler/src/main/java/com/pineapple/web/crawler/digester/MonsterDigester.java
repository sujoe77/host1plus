package com.pineapple.web.crawler.digester;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pineapple.web.crawler.entity.JobInfo;

public class MonsterDigester implements Digester<JobInfo> {

	@Override
	public List<JobInfo> digest(String htm) {
		List<JobInfo> ret = new ArrayList<JobInfo>();
		Document doc = Jsoup.parse(htm);
		// System.out.println(htm);
		Element resultsWrapper = doc.getElementById("resultsWrapper");
		Elements resultList = resultsWrapper.getElementsByClass("js_result_row");
		for (Element e : resultList) {
			processItem(ret, e);
		}
		return ret;
	}

	private void processItem(List<JobInfo> ret, Element e) {
		JobInfo item = new JobInfo();
		Element company = e.getElementsByClass("company").first();
		item.setCompany(company.getElementsByTag("span").first().text());
		item.setDateStr(e.getElementsByTag("time").first().text());
		Element jobLink = e.getElementsByClass("jobTitle").first().getElementsByTag("h2").first().getElementsByTag("a").first();
		item.setJobId(jobLink.attr("data-m_impr_j_jobid"));
		item.setLink(jobLink.absUrl("href"));		
		Elements location = e.getElementsByClass("location");
		item.setLocation(location.first().getElementsByTag("span").first().text());
		item.setTitle(jobLink.attr("title"));
		ret.add(item);
	}

	@Override
	public Elements getResultListFromDoc(Document doc) {
		// TODO Auto-generated method stub
		return null;
	}

}
