package com.pineapple.web.crawler.handler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pineapple.web.crawler.entity.EBook;
import com.pineapple.web.crawler.exception.SysException;
import com.pineapple.web.crawler.persistent.HibernateUtil;
import com.pineapple.web.crawler.util.Config;
import com.pineapple.web.crawler.util.Utility;

public class EBookHandler implements EntityHandler<EBook> {
	private static final int NUM_MONTH = -1;
	private static final int NUM_WEEK = -52;
	private static EBookHandler instance;
	private static final String BEGIN_DATE_STR = "2015-05-25";
	private static final String END_DATE_STR = "2015-05-31";
	private List<EBook> bookList = new ArrayList<EBook>();

	private EBookHandler() {
	}

	public static synchronized EBookHandler getInstance() throws IOException {
		if (instance == null) {
			instance = new EBookHandler();
		}
		return instance;
	}

	@Override
	public void handleEntityList(List<EBook> entityList) {
		for (EBook book : entityList) {
			handleEntity(book);
		}
	}

	@Override
	public void handleEntity(EBook book) {
		if (addToList(book)) {
			outputBookInfo(book);
			Date publisDate = book.getPublisDate();
			if (publisDate.after(getBeginDate()) && publisDate.before(getEndDate())) {
				HibernateUtil.saveObject(book);
			}
		}
	}

	protected void outputBookInfo(EBook book) {
		Utility.log(Utility.LOG_INFO, getClass(), book.toString());
	}

	protected boolean addToList(EBook book) {
		if (!bookList.contains(book)) {
			bookList.add(book);
			return true;
		}
		return false;
	}

	public Date getBeginDate() {
		/*
		 * Calendar calendar = Calendar.getInstance();
		 * calendar.add(Calendar.WEEK_OF_YEAR, NUM_WEEK); return
		 * calendar.getTime();
		 */
		return getConfigDate("beginDate");
	}

	public Date getEndDate() {
		/*
		 * Calendar calendar = Calendar.getInstance();
		 * calendar.add(Calendar.WEEK_OF_YEAR, NUM_WEEK); return
		 * calendar.getTime();
		 */
		return getConfigDate("endDate");
	}

	public Date getConfigDate(String key) {
		/*
		 * Calendar calendar = Calendar.getInstance();
		 * calendar.add(Calendar.WEEK_OF_YEAR, NUM_WEEK); return
		 * calendar.getTime();
		 */
		try {
			return str2Date(Config.getProperty("EbookCrawler." + key));
		} catch (SysException e) {
			Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
			return null;
		}
	}

	public Date str2Date(String str) {
		Date ret = null;
		try {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			ret = format.parse(str);
		} catch (ParseException e) {
			Utility.log(Utility.LOG_ERROR, getClass(), e.getMessage());
		}
		return ret;
	}

	@Override
	public void setCustomData(Object data) throws Exception {
	}
}
