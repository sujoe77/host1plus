package com.pineapple.util;

import java.util.Properties;

import com.pineapple.exception.SysException;

public final class Config {
    public static final String CONFIG_PROPERTIES = "config.properties";
    private static Config instance = new Config();
    private Properties properties;
    
    private Config(){
    }
    
    public Properties getProperties() throws SysException {
        if(properties == null){
            properties = Utility.loadPropFromFile();
        }
        return properties;
    }

    public void setProperties(final Properties properties) {
        this.properties = properties;
    }
    
    private static Config getInstance(){
        return instance;
    }
    
    private String getPropertyValue(final String key) throws SysException {
        return getProperties().getProperty(key);
    }
        
    public static String getProperty(final String key) throws SysException {
        return getInstance().getPropertyValue(key);
    }
}
