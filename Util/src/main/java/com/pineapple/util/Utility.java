package com.pineapple.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.pineapple.exception.SysException;

public final class Utility {
	public static final String LOG_ERROR = "ERROR";
	public static final String LOG_INFO = "INFO";
	public static final String LOG_DEBUG = "DEBUG";
	public static final Pattern BIN_FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g" + "|png|tiff?|mid|mp2|mp3|mp4" + "|wav|avi|mov|mpeg|ram|m4v|pdf" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
	public static final boolean DEBUG = false;

	private Utility() {
	}

	// public static void savePage(String folder, Page page) {
	// String path = page.getWebURL().getPath();
	// if (path.length() > 1) {
	// byte[] encoded = page.getContentData();
	// Utility.log(Utility.LOG_DEBUG, Utility.class, "=============saving page
	// ========================");
	// try {
	// String localFile = path.substring(1).replace("/", "_");
	// FileOutputStream output = new FileOutputStream(new File(folder +
	// localFile));
	// IOUtils.write(encoded, output);
	// output.close();
	// } catch (IOException e) {
	// Utility.log(Utility.LOG_DEBUG, Utility.class, e.getMessage());
	// }
	// }
	// }

	public static boolean contains(String str, String[] array) {
		String input = str.toLowerCase(Locale.ENGLISH);
		for (String keyWord : array) {
			if (input.contains(keyWord.toLowerCase(Locale.ENGLISH))) {
				return true;
			}
		}
		return false;
	}

	public static String getStrBefore(String inputStr, String strToFind) {
		int idx = inputStr.indexOf(strToFind);
		if (idx >= 0) {
			return inputStr.substring(0, idx);
		} else {
			return "";
		}
	}

	public static String getStrAfter(String inputStr, String strToFind) {
		int idx = inputStr.indexOf(strToFind);
		if (idx >= 0) {
			return inputStr.substring(idx + strToFind.length());
		} else {
			return "";
		}
	}

	// public static void printDebugInfo(Page page) {
	// int docid = page.getWebURL().getDocid();
	// String url = page.getWebURL().getURL();
	// String parentUrl = page.getWebURL().getParentUrl();
	//
	// Utility.log(Utility.LOG_DEBUG, Utility.class, "Docid: " + docid);
	// Utility.log(Utility.LOG_DEBUG, Utility.class, "URL: " + url);
	// Utility.log(Utility.LOG_DEBUG, Utility.class, "Parent page: " +
	// parentUrl);
	// }

	public static String removeNewLineSpace(String input) {
		return input.replaceAll("\n", "").replaceAll("\r", "").trim();
	}

	public static Properties loadPropFromFile() throws SysException {
		return loadPropFromFile(Config.CONFIG_PROPERTIES);
	}

	public static Properties loadPropFromFile(String fileName) throws SysException {
		Properties prop = new Properties();
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
		if (inputStream == null) {
			String msg = "property file '" + fileName + "' not found in the classpath";
			throw new SysException(Utility.class.getName(), msg, new FileNotFoundException(msg));
		} else {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				throw new SysException(Utility.class.getName(), "Error loading from file " + fileName, e);
			}
		}
		return prop;
	}

	public static boolean match(Pattern p, String str) {
		Matcher m = p.matcher(str);
		return m.matches();
	}

	public static boolean match(List<Pattern> pList, String str) {
		for (Pattern pattern : pList) {
			if (match(pattern, str)) {
				return true;
			}
		}
		return false;
	}

	public static void log(String level, Class clazz, String message) {
		final Logger LOGGER = Logger.getLogger(clazz);
		if (LOG_DEBUG.equals(level)) {
			LOGGER.debug(message);
		} else if (LOG_ERROR.equals(level)) {
			LOGGER.error(message);
		} else {
			LOGGER.info(message);
		}
	}

	public static boolean matchPatternInList(String str, List<Pattern> patternList) {
		for (Pattern p : patternList) {
			Matcher m = p.matcher(str);
			if (m.matches()) {
				return true;
			}
		}
		return false;
	}
}
