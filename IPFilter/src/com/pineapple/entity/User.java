package com.pineapple.entity;

public class User {
	String userId;
	public static final String COOKIE_USE_ID = "useId";
	public static final String TABLE_NAME = "APP_USERS";
	
	public User(String id){
		this.userId = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
