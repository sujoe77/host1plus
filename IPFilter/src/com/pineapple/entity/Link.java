package com.pineapple.entity;

public class Link {
	private long nodeId1;
	private long nodeId2;
	private String ip1;
	private String ip2;
	public static final String TABLE_NAME = "LINKS";
	
	public Link(Node n1, Node n2){
		nodeId1 = n1.getNodeId();
		nodeId2 = n2.getNodeId();
		ip1 = n1.getIp();
		ip2 = n2.getIp();
	}
	
	public long getNodeId1() {
		return nodeId1;
	}
	public void setNodeId1(long nodeId1) {
		this.nodeId1 = nodeId1;
	}
	public String getIp1() {
		return ip1;
	}
	public void setIp1(String ip1) {
		this.ip1 = ip1;
	}
	public String getIp2() {
		return ip2;
	}
	public void setIp2(String ip2) {
		this.ip2 = ip2;
	}
	public long getNodeId2() {
		return nodeId2;
	}
	public void setNodeId2(long nodeId2) {
		this.nodeId2 = nodeId2;
	}
	
	@Override
	public String toString(){
		return "NodeId1: " + nodeId1 + " IP1: " + ip1 + " NodeId2: " + nodeId2 + " IP2: " + ip2;
	}
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Link)){
			return false;
		}
		Link link = (Link) obj;
		return ip1.equals(link.getIp1()) && ip2.equals(link.getIp2()) 
				&& nodeId1 == link.getNodeId1() && nodeId2 == link.getNodeId2();
	}
}
