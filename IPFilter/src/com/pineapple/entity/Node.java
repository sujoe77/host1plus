package com.pineapple.entity;

import com.pineapple.util.Util;

public class Node {
	private String ip;
	private long nodeId;
	public static final String IP = "IP";
	public static final String NODE_ID = "NODE_ID";
	public static final String TABLE_NAME = "IP";
	
	public Node(String ip, long nodeId){
		this.ip = ip;
		this.nodeId = nodeId;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public long getNodeId() {
		return nodeId;
	}
	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Node)){
			return false;
		}
		Node node = (Node) obj;
		return Util.isNodeValid(this) && Util.isNodeValid(node) && ip.equals(node.getIp()) && nodeId == node.getNodeId();
	}

	@Override
	public String toString(){
		return "NodeId: " + nodeId + " IP: " + ip;
	}
}
