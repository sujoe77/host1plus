package com.pineapple.collection;

import java.util.ArrayList;

import com.pineapple.entity.Link;

public class LinkList {
	private static LinkList instance;
	private ArrayList<Link> theList;
	
	public ArrayList<Link> getTheList() {
		return theList;
	}

	private LinkList(){
		theList = new ArrayList<Link>();
	}
	
	public static LinkList getInstance(){
		if(instance == null){
			instance = new LinkList();
		}
		return instance;
	}
	
	public void addToList(Link link){
		theList.add(link);
	}
	
	public boolean isLinkExist(String ip, long nodeId){
		for (Link link : theList) {
			if(link.getNodeId1() == nodeId && link.getIp2().equals(ip)
					||link.getNodeId2() == nodeId && link.getIp1().equals(ip)){
				return true;
			}
		}
		return false;
	}
	
	public boolean contains(Link link){
		return theList.contains(link);
	}
}
