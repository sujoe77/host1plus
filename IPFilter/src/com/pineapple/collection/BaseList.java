package com.pineapple.collection;

import java.util.ArrayList;

public class BaseList<T> {
	private ArrayList<T> theList;
	
	public ArrayList<T> getTheList() {
		return theList;
	}

	private BaseList(){
		theList = new ArrayList<T>();
	}

	public void addToList(T obj){
		theList.add(obj);
	}
	
	public boolean contains(T obj){
		return theList.contains(obj);
	}
}
