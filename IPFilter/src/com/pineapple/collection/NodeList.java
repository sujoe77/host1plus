package com.pineapple.collection;

import java.util.ArrayList;

import com.pineapple.entity.Node;

public class NodeList {
	private static NodeList instance;
	private ArrayList<Node> theList;
	
	public ArrayList<Node> getTheList() {
		return theList;
	}

	private NodeList(){
		theList = new ArrayList<Node>();
	}
	
	public static NodeList getInstance(){
		if(instance == null){
			instance = new NodeList();
		}
		return instance;
	}
	
	public void addToList(Node node){
		theList.add(node);
	}
	
	public boolean isIPExist(String ip){
		return isNodeExist(ip,Node.IP);
	}
	
	public boolean isNodeIdExist(String nodeId){
		return isNodeExist(nodeId,Node.NODE_ID);
	}
	
	protected boolean isNodeExist(String value, String type){
		return getNodeIdx(value,type) > -1;
	}
	
	public Node getNode(String value, String type){
		int idx = getNodeIdx(value,type);
		return idx > -1 ? theList.get(idx) : null;
	}
	
	public boolean contains(Node node){
		return theList.contains(node);
	}
	
	public static boolean contains(String value, String type){
		return getInstance().getNodeIdx(value, type) >= 0;
	}
	
	private int getNodeIdx(String value, String type){
		for (int i=0; i < theList.size(); i++) {
			Node node = theList.get(i);
			if(node.getIp().equals(value) && type.equals(Node.IP)
					|| String.valueOf(node.getNodeId()).toString().equals(value)&& type.equals(Node.NODE_ID)){
				return i;
			}
		}
		return -1;
	}
}
