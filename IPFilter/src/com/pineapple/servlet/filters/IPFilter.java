package com.pineapple.servlet.filters;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pineapple.collection.LinkList;
import com.pineapple.collection.NodeList;
import com.pineapple.entity.Link;
import com.pineapple.entity.Node;
import com.pineapple.entity.User;
import com.pineapple.util.UrlUtil;
import com.pineapple.util.Util;

/**
 * Servlet Filter implementation class IPFilter
 */
@WebFilter(
		filterName = "IPFilter1", 
		description = "IPFilter", 
		urlPatterns = { 
				"/IPFilter", 
				"/"
		})
public class IPFilter implements Filter {

	/**
     * Default constructor. 
     */
    public IPFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		try {
			Util.writeListToDB(Node.TABLE_NAME, false);
			Util.writeListToDB(Link.TABLE_NAME, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rsp = (HttpServletResponse)response;
		String url = req.getRequestURI();
		System.out.println("Get: " + url);
		HashMap<String, String> cookieMap = (HashMap<String, String>) handleCookies(req,rsp);
		handleIP(req,rsp);
		String ip = request.getRemoteAddr();
		long nodeId = UrlUtil.getNIdFromUrl(url);//request.getParameter(NID);
		Node newNode = NodeList.getInstance().getNode(ip, Node.IP);
		Node oldNode = NodeList.getInstance().getNode(String.valueOf(nodeId), Node.NODE_ID);
		if (oldNode == null || !ip.equals(oldNode.getIp())) {
			if (newNode == null) {
				newNode = Util.createNewNode(ip);
			}
			if (oldNode != null	&& !LinkList.getInstance().isLinkExist(ip, nodeId)) {
				Util.createNewLink(oldNode, newNode);
			}
//			StringBuilder builder = new StringBuilder(url);
//			builder.append("?").append(NID).append("=").append(newNode.getNodeId());
//			Enumeration<String> emu = request.getParameterNames();
//			while (emu.hasMoreElements()) {
//				String pName = emu.nextElement();
//				String value = request.getParameter(pName);
//				if (Util.isNotBlank(value) && !pName.equals(NID)){
//					builder.append("&").append(pName).append("=").append(value);
//				}
//			}
			url = UrlUtil.getRedirectUrl(req, newNode.getNodeId());
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			System.out.println("Redirect to: " + url);
			httpResponse.sendRedirect(url);
		} else {
			RequestDispatcher dispatcher = req.getRequestDispatcher(UrlUtil.removeAppName(UrlUtil.getOriginalUrl(url)));
			//RequestDispatcher dispatcher = request.getRequestDispatcher("/wx/ma.jsp");
			req.getSession().setAttribute("url", url);
			dispatcher.forward(req, response);
			//chain.doFilter(request, response);
		}
		//chain.doFilter(request, response);
	}
	
	private Map<String,String> handleCookies(HttpServletRequest req, HttpServletResponse rsp) {
		HashMap<String,String> ret	 = new HashMap<String,String>();
		Cookie[] cookies = req.getCookies();
		if (cookies != null) {
			System.out.println("<h2> Found Cookies Name and Value</h2>");
			for (Cookie cookie : cookies) {
				System.out.print("Name : " + cookie.getName() + ",  ");
				System.out.print("Value: " + cookie.getValue() + " <br/>");
				ret.put(cookie.getName(), cookie.getValue());
			}
		} else {
			System.out.println("<h2>No cookies founds</h2>");
			String uuid = Util.getUUID();
			rsp.addCookie(new Cookie(User.COOKIE_USE_ID, uuid));
			ret.put(User.COOKIE_USE_ID, uuid);
		}
		return ret;
	}
	
	private void handleIP(HttpServletRequest req, HttpServletResponse rsp){
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		Util.loadListFromDB(Node.TABLE_NAME);
		Util.loadListFromDB(Link.TABLE_NAME);
	}
}
