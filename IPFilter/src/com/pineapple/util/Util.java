package com.pineapple.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import com.pineapple.collection.LinkList;
import com.pineapple.collection.NodeList;
import com.pineapple.entity.Link;
import com.pineapple.entity.Node;

public class Util {
	private static final String JDBC_URL = "jdbc:mysql://localhost/ipf?user=ipf&password=ipf";
	private static Connection connect = null;
	
	public static Connection getConnect() throws SQLException, ClassNotFoundException {
		if (connect == null || connect.isClosed()) {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection(JDBC_URL);
		}
		return connect;
	}

	public static Long getUUIDLong(){
		Long uuid = UUID.randomUUID().getMostSignificantBits();
		return uuid;
	}
	
	public static String getUUID(){
		return UUID.randomUUID().toString();
	}
	
	public static boolean isBlank(String str){
		return str == null || "".equals(str);
	}
	
	public static boolean isNotBlank(String str){
		return !isBlank(str);
	}
	
	public static Node createNewNode(String ip){
		Node newNode = new Node(ip,Util.getUUIDLong());
		NodeList.getInstance().addToList(newNode);
		return newNode;
	}
	
	public static Link createNewLink(Node n1,Node n2){
		if(!n1.equals(n2) && isNodeValid(n1) && isNodeValid(n2)){
			Link link = new Link(n1,n2);
			LinkList.getInstance().addToList(link);
			return link;
		} else {
			return null;
		}
	}
	
	public static boolean isNodeValid(Node node){
		return isNotBlank(node.getIp()) && node.getNodeId() != 0L;
	}
	
	public static int getLinkNum(){
		return  LinkList.getInstance().getTheList().size();
	}
	
	public static int getNodeNum(){
		return  NodeList.getInstance().getTheList().size();
	}
	
	public static String printAllLinks(String order, int count){
		StringBuilder builder = new StringBuilder("");
		ArrayList<Link> theList = LinkList.getInstance().getTheList();
		int size = theList.size();
		for(int i = 0; i < Math.min(size,count); i++){
			Link link;
			if("asc".equalsIgnoreCase(order)){
				link = theList.get(i);
			} else {
				link = theList.get(size - 1 - i);
			}
			builder.append(link);
			builder.append("<br/>");
		}
		return builder.toString();
	}
	
	public static String printAllNodes(String order, int count){
		StringBuilder builder = new StringBuilder("");
		ArrayList<Node> theList = NodeList.getInstance().getTheList();
		int size = theList.size();
		for(int i = 0; i < Math.min(size,count); i++){
			Node node;
			if("asc".equalsIgnoreCase(order)){
				node = theList.get(i);
			} else {
				node = theList.get(size - 1 - i);
			}
			builder.append(node);
			builder.append("<br/>");
		}
		return builder.toString();
	}
	
	private static void close(AutoCloseable c) {
		try {
			if (c != null) {
				c.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static int doWriteDB(boolean updateExist,
			PreparedStatement preparedStatementSel,
			PreparedStatement preparedStatementDel,
			PreparedStatement preparedStatementIns) throws SQLException {
		int ret = 0;
		ResultSet resultSet = null;
		resultSet = preparedStatementSel.executeQuery();
		resultSet.next();
		if (updateExist || (resultSet.getInt(1) == 0 && !updateExist)) {
			if (updateExist) {
				preparedStatementDel.executeUpdate();
			}
			ret = preparedStatementIns.executeUpdate();
		}
		return ret;
	}
	
	public static int writeListToDB(String tableName, boolean updateExist) throws SQLException{
		int ret = 0;
		PreparedStatement preparedStatementIns = null;
		PreparedStatement preparedStatementDel = null;
		PreparedStatement preparedStatementSel = null;
		
		String sqlIns = "";
		String sqlDel = "";
		String sqlSel = "";
		if(Node.TABLE_NAME.equalsIgnoreCase(tableName)){
			sqlSel = "SELECT count(*) FROM " + Node.TABLE_NAME + " where UUID = ? and IP = ?";
			sqlIns = "insert into  " + Node.TABLE_NAME + "(UUID,IP,LEVEL,DESCRIPTION) values (?, ?, ?, ?)";
			sqlDel = "delete from " + Node.TABLE_NAME + " where UUID = ? and IP=? ";
			preparedStatementDel = connect.prepareStatement(sqlDel);
			preparedStatementIns = connect.prepareStatement(sqlIns);
			preparedStatementSel = connect.prepareStatement(sqlSel);
			for(Node node : NodeList.getInstance().getTheList()){
				preparedStatementSel.setLong(1, node.getNodeId());
				preparedStatementSel.setString(2, node.getIp());
				preparedStatementDel.setLong(1, node.getNodeId());
				preparedStatementDel.setString(2, node.getIp());
				preparedStatementIns.setLong(1, node.getNodeId());
				preparedStatementIns.setString(2, node.getIp());
				preparedStatementIns.setInt(3, -99);
				preparedStatementIns.setString(4, null);
				ret += doWriteDB(updateExist, preparedStatementSel, preparedStatementDel, preparedStatementIns);
			}
		} else if(Link.TABLE_NAME.equalsIgnoreCase(tableName)){
			sqlSel = "SELECT count(*) FROM " + Link.TABLE_NAME + " where NID1 = ? and IP1 = ? and NID2 = ? and IP2 = ? ";
			sqlIns = "insert into  " + Link.TABLE_NAME + "(NID1,IP1,NID2,IP2) values (?, ?, ?, ?)";
			sqlDel = "delete from " + Link.TABLE_NAME + " where NID1 = ? and IP1 = ? and NID2=? and IP2=? ";
			preparedStatementDel = connect.prepareStatement(sqlDel);
			preparedStatementIns = connect.prepareStatement(sqlIns);
			preparedStatementSel = connect.prepareStatement(sqlSel);
			for(Link link : LinkList.getInstance().getTheList()){
				preparedStatementSel.setLong(1, link.getNodeId1());
				preparedStatementSel.setString(2, link.getIp1());
				preparedStatementSel.setLong(3, link.getNodeId2());
				preparedStatementSel.setString(4, link.getIp2());
				preparedStatementDel.setLong(1, link.getNodeId1());
				preparedStatementDel.setString(2, link.getIp1());
				preparedStatementDel.setLong(3, link.getNodeId2());
				preparedStatementDel.setString(4, link.getIp2());				
				preparedStatementIns.setLong(1, link.getNodeId1());
				preparedStatementIns.setString(2, link.getIp1());
				preparedStatementIns.setLong(3, link.getNodeId2());
				preparedStatementIns.setString(4, link.getIp2());					
				ret += doWriteDB(updateExist, preparedStatementSel, preparedStatementDel, preparedStatementIns);
			}
		}		
		return ret;
	};
	
	public static int loadListFromDB(String tableName) {
		int ret = 0;
		Statement statement = null;
		ResultSet resultSet = null;
		if(Util.isBlank(tableName)){
			return -1;
		}		
		try {
			statement = getConnect().createStatement();
			resultSet = statement.executeQuery("select * from " + tableName);
			while(resultSet.next()){
				if (Node.TABLE_NAME.equalsIgnoreCase(tableName)) {
					Node node = new Node(resultSet.getString("IP"), resultSet.getLong("UUID"));
					if (!NodeList.getInstance().contains(node)) {
						NodeList.getInstance().addToList(node);
						ret++;
					}
				} else if(Link.TABLE_NAME.equalsIgnoreCase(tableName)){
					Node node1 = new Node(resultSet.getString("IP1"), resultSet.getLong("NID1"));
					Node node2 = new Node(resultSet.getString("IP2"), resultSet.getLong("NID2"));
					Link link = new Link(node1,node2);
					if (!LinkList.getInstance().contains(link)) {
						LinkList.getInstance().addToList(link);
						ret++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(statement);
			close(resultSet);
		}
		return ret;
	}
	
	public static int getRowNumFromDB(String tableName){
		int ret = 0;
		Statement statement = null;
		ResultSet resultSet = null;
		if(Util.isBlank(tableName)){
			return -1;
		}		
		try {
			statement = getConnect().createStatement();
			resultSet = statement.executeQuery("select count(*) from " + tableName);
			while(resultSet.next()){
				ret = resultSet.getInt(1);
				return ret;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(statement);
			close(resultSet);
		}
		return ret;
	}
}
