package com.pineapple.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

public class UrlUtil {
	
	private static final String HTM = ".htm";
	private static final String JSP = ".jsp";
	private static final String _ = "_";

	public static String getOriginalUrl(String url){
		if(url.endsWith(JSP) || url.endsWith(HTM)){
			return url;
		}
		int idx = url.indexOf(_);
		String ret = url.substring(0,idx);
		if(idx < 0){
			return "";
		} else if(ret.endsWith(JSP) || ret.endsWith(HTM)){
			return ret;
		} else {
			return ret + JSP;
		}
	}
	
	public static long getNIdFromUrl(String url){
		int idx = url.indexOf(_);
		return idx < 0  ? 0 : Long.parseLong(url.substring(idx + 1));		
	}
	
	public static String removeAppName(String url){
		int idx = url.substring(1).indexOf("/");
		return idx < 0  ? "" : url.substring(idx + 1);
	}
	
	public static String getRedirectUrl(HttpServletRequest request, Long nodeId){
		String tmp = getOriginalUrl(request.getRequestURI());
		if(Util.isBlank(tmp)){
			return "";
		}
		//tmp = tmp.substring(0, tmp.length()-4);
		StringBuilder builder = new StringBuilder(tmp);
		builder.append(_);
		builder.append(nodeId);
		builder.append("?");
		Enumeration<String> emu = request.getParameterNames();
		while (emu.hasMoreElements()) {
			String pName = emu.nextElement();
			String value = request.getParameter(pName);
			if (Util.isNotBlank(value)){
				builder.append(pName).append("=").append(value).append("&");
			}
		}
		return builder.toString();
	}
}
