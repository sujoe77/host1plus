<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.pineapple.collection.*" %>
<%@ page import="com.pineapple.entity.*" %>
<%@ page import="com.pineapple.util.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.lang.Class"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%!

public int writeListToDB(String tableName, boolean updateExist) throws SQLException, ClassNotFoundException {
	Class.forName("com.mysql.jdbc.Driver");
	Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/ipf?user=ipf&password=ipf");
	int ret = 0;
	PreparedStatement preparedStatementIns = null;
	PreparedStatement preparedStatementDel = null;
	PreparedStatement preparedStatementSel = null;
	
	String sqlIns = "";
	String sqlDel = "";
	String sqlSel = "";
	if("IP".equalsIgnoreCase(tableName)){
		sqlSel = "SELECT count(*) FROM " + "IP" + " where UUID = ? and IP = ?";
		sqlIns = "insert into  " + "IP" + "(UUID,IP,LEVEL,DESCRIPTION) values (?, ?, ?, ?)";
		sqlDel = "delete from " + "IP" + " where UUID = ? and IP=? ";
		preparedStatementDel = connect.prepareStatement(sqlDel);
		preparedStatementIns = connect.prepareStatement(sqlIns);
		preparedStatementSel = connect.prepareStatement(sqlSel);
		for(Node node : NodeList.getInstance().getTheList()){
			preparedStatementSel.setLong(1, Long.parseLong(node.getNodeId()));
			preparedStatementSel.setString(2, node.getIp());
			preparedStatementDel.setLong(1, Long.parseLong(node.getNodeId()));
			preparedStatementDel.setString(2, node.getIp());
			preparedStatementIns.setLong(1, Long.parseLong(node.getNodeId()));
			preparedStatementIns.setString(2, node.getIp());
			preparedStatementIns.setInt(3, -99);
			preparedStatementIns.setString(4, null);
			ret += doWriteDB(updateExist, preparedStatementSel, preparedStatementDel, preparedStatementIns);
		}
	} else if("LINKS".equalsIgnoreCase(tableName)){
		sqlSel = "SELECT count(*) FROM " + "LINKS" + " where NID1 = ? and IP1 = ? and NID2 = ? and IP2 = ? ";
		sqlIns = "insert into  " + "LINKS" + "(NID1,IP1,NID2,IP2) values (?, ?, ?, ?)";
		sqlDel = "delete from " + "LINKS" + " where NID1 = ? and IP1 = ? and NID2=? and IP2=? ";
		preparedStatementDel = connect.prepareStatement(sqlDel);
		preparedStatementIns = connect.prepareStatement(sqlIns);
		preparedStatementSel = connect.prepareStatement(sqlSel);
		for(Link link : LinkList.getInstance().getTheList()){
			preparedStatementSel.setLong(1, Long.parseLong(link.getNodeId1()));
			preparedStatementSel.setString(2, link.getIp1());
			preparedStatementSel.setLong(3, Long.parseLong(link.getNodeId2()));
			preparedStatementSel.setString(4, link.getIp2());
			preparedStatementDel.setLong(1, Long.parseLong(link.getNodeId1()));
			preparedStatementDel.setString(2, link.getIp1());
			preparedStatementDel.setLong(3, Long.parseLong(link.getNodeId2()));
			preparedStatementDel.setString(4, link.getIp2());				
			preparedStatementIns.setLong(1, Long.parseLong(link.getNodeId1()));
			preparedStatementIns.setString(2, link.getIp1());
			preparedStatementIns.setLong(3, Long.parseLong(link.getNodeId2()));
			preparedStatementIns.setString(4, link.getIp2());					
			ret += doWriteDB(updateExist, preparedStatementSel, preparedStatementDel, preparedStatementIns);
		}
	}		
	return ret;
};

private static int doWriteDB(boolean updateExist,
		PreparedStatement preparedStatementSel,
		PreparedStatement preparedStatementDel,
		PreparedStatement preparedStatementIns) throws SQLException {
	int ret = 0;
	ResultSet resultSet = null;
	resultSet = preparedStatementSel.executeQuery();
	resultSet.next();
	if (updateExist || (resultSet.getInt(1) == 0 && !updateExist)) {
		if (updateExist) {
			preparedStatementDel.executeUpdate();
		}
		ret = preparedStatementIns.executeUpdate();
	}
	return ret;
}
%>
<%
writeListToDB("IP",false);
writeListToDB("LINKS",false);
%>
</body>
</html>