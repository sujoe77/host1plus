<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html> 
<head> 
    <script type="text/javascript">
        var navigationStart = false;

        if (typeof performance != "undefined" && performance.timing){
            navigationStart = performance.timing.navigationStart;
        }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="dns-prefetch" href="http://mmbiz.qpic.cn">
    <link rel="dns-prefetch" href="http://res.wx.qq.com">
    <title>一名赣州车主处理“碰瓷”过程，值得认真学习</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
        
    <script type="text/javascript">
        document.domain = "qq.com";
        var _wxao = window._wxao || {};
        _wxao.begin = (+new Date());
    </script>
        <style>html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{line-height:1.6;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:16px}body,h1,h2,h3,h4,h5,p,ul,ol,dl,dd,fieldset,textarea{margin:0}fieldset,legend,textarea,input,button{padding:0}button,input,select,textarea{font-family:inherit;font-size:100%;margin:0;*font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}ul,ol{padding-left:0;list-style-type:none;list-style-position:inside}a img,fieldset{border:0}a{text-decoration:none}a{color:#607fa6}.rich_media_inner{padding:15px}.rich_media_title{line-height:24px;font-weight:700;font-size:20px;word-wrap:break-word;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}.rich_media_title .rich_media_meta{vertical-align:middle;position:relative}.rich_media_meta{display:inline-block;vertical-align:middle;font-weight:400;font-style:normal;margin-right:.5em;font-size:12px;width:auto;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;max-width:none}.rich_media_meta.text{color:#8c8c8c}.rich_media_thumb{font-size:0;margin-top:18px}.rich_media_thumb img{width:100%}.rich_media_content{margin-top:18px;color:#3e3e3e;word-wrap:break-word;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}.rich_media_content p{*zoom:1;min-height:1em;word-wrap:normal;white-space:pre-wrap;margin-top:1em;margin-bottom:1em}.rich_media_content p:after{content:"\200B";display:block;height:0;clear:both}.rich_media_content *{max-width:100%!important;word-wrap:break-word!important;-webkit-box-sizing:border-box!important;box-sizing:border-box!important}.rich_media_content img{height:auto!important}.rich_media_tool{*zoom:1;padding:18px 0;font-size:14px}.rich_media_tool:after{content:"\200B";display:block;height:0;clear:both}.rich_media_tool .media_tool_meta i,.rich_media_tool .media_tool_meta .icon_meta{vertical-align:0;position:relative;top:1px;margin-right:3px}.rich_media_tool .meta_primary{float:left;margin-right:14px}.rich_media_tool .meta_extra{float:right;margin-left:14px}.rich_media_tool .link_primary{color:#8c8c8c}.rich_media_extra{padding-top:0}@media screen and (min-width:1023px){.rich_media{width:740px;margin-left:auto;margin-right:auto}.rich_media_inner{padding:20px;background-color:#fff;border:1px solid #d9dadc;border-top-width:0}.rich_media_meta{max-width:none}.rich_media_content{min-height:350px}.rich_media_title{padding-bottom:10px;margin-bottom:5px;border-bottom:1px solid #e7e7eb}}.line_tips_wrp{margin-top:20px;text-align:center;border-top:1px dotted #a8a8a7;line-height:16px}.line_tips{display:inline-block;position:relative;top:-10px;padding:0 16px;font-size:14px;color:#cfcfcf;background-color:#f8f7f5;text-decoration:none}body{background-color:#f8f7f5;-webkit-touch-callout:none}h1,h2,h3,h4,h5,h6{font-weight:400;font-style:normal;font-size:100%}.icon_arrow_gray{width:7px}.icon_loading_white{width:16px}.icon_praise_gray{width:13px}.line_tips_wrp{margin-bottom:10px}.rich_media_meta.nickname{max-width:10em}.rich_media_extra{position:relative}.rich_media_extra .appmsg_banner{max-height:166px;width:100%}.rich_media_extra .ad_msg_mask{position:absolute;width:100%;height:100%;text-align:center;line-height:200px;background-color:#000;filter:alpha(opacity=20);-moz-opacity:.2;-khtml-opacity:.2;opacity:.2;left:0;top:0}.rich_media_content{font-size:16px}.rich_media_content p{margin-top:0;margin-bottom:0}.rich_media_tool .praise_num{display:inline-block;vertical-align:top;width:auto;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;min-width:3em}.rich_media_tool .meta_praise{-webkit-tap-highlight-color:rgba(0,0,0,0);outline:0;margin-right:0}.icon_praise_gray{background:transparent url(http://mmbiz.qpic.cn/mmbiz/ByCS3p9sHiam47qqib840uVr9ZH6ORLqhqmFibrmxWeY5icJ7ZE8Un8AibB18U19fCMUg9tibw8vgOdl4/0) no-repeat 0 0;width:13px;height:13px;vertical-align:middle;display:inline-block;-webkit-background-size:100% auto;background-size:100% auto}.icon_praise_gray.praised{background-position:0 -18px}.praised .icon_praise_gray{background-position:0 -18px}.rich_media_extra{padding-bottom:10px;font-size:14px}.rich_media_extra .extra_link{display:block}.rich_media_extra img{vertical-align:middle;margin-top:-3px}.rich_media_extra .icon_loading_white{margin-left:1em}.global_error_msg{padding:60px 30px}.global_error_msg strong{display:block}.global_error_msg.warn{color:#f00}.selectTdClass{background-color:#edf5fa!important}table.noBorderTable td,table.noBorderTable th,table.noBorderTable caption{border:1px dashed #ddd!important}table{margin-bottom:10px;border-collapse:collapse;display:table;width:100%!important}td,th{word-wrap:break-word;word-break:break-all;padding:5px 10px;border:1px solid #DDD}caption{border:1px dashed #DDD;border-bottom:0;padding:3px;text-align:center}th{border-top:2px solid #BBB;background:#f7f7f7}.ue-table-interlace-color-single{background-color:#fcfcfc}.ue-table-interlace-color-double{background-color:#f7faff}td p{margin:0;padding:0}.res_iframe{width:100%;background-color:transparent;border:0}.vote_area{position:relative;display:block;margin:14px 0;white-space:normal!important}.vote_iframe{width:100%;height:100%;background-color:transparent;border:0}</style>
        <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/page_mp_article_pc1fa64f.css"/>
    <![endif]-->
    <link media="screen and (min-width:1023px)" rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/page_mp_article_pc1fa64f.css"/>
    <style>
                .rich_media_content{font-size:18px;}
            </style><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53954335-1', 'auto');
  ga('send', 'pageview');
ga(‘set’, ‘&uid’, {{USER_ID}}); // Set the user ID using signed-in user_id.
</script>
</head> 

<body id="activity-detail">
    <script type="text/javascript">
        var write_sceen_time = (+new Date());
    </script>
        <div class="rich_media">
        <div class="rich_media_inner">

            <h2 class="rich_media_title" id="activity-name">
                一名赣州车主处理“碰瓷”过程，值得认真学习 
            </h2>

            <div class="rich_media_meta_list">
                <em id="post-date" class="rich_media_meta text">2014-08-13</em>

                                <a class="rich_media_meta link nickname" href="javascript:viewProfile();" id="post-user">掌上石家庄</a>
            </div>

            <div id="page-content">
                <div id="img-content">
                    
                                                            
                                        <div class="rich_media_thumb" id="media">
                        <img onerror="this.parentNode.removeChild(this)" src="http://mmbiz.qpic.cn/mmbiz/iblyVZZUyqusfF8ql0YeUqYTib2K0rXgNsYEJCsS1oIm0rYHSY7HiamvKk1PFQTJJcqsibt3fP4eYIdIWDOKwbcQIA/0" />
                    </div>
                                        
                    <div class="rich_media_content" id="js_content"><p>中国是一个知法犯法的国度，执法的都在犯法，但是没人懂太多的法律，就是一帮愚民~~惧怕官老爷习惯了，今天推荐《一名赣州车主处理“碰瓷”过程》，值得认真学习。</p><p><br  /></p><p><strong><span style="color: rgb(255, 0, 0);">第一篇 事故过程和处理</span></strong></p><p><strong><span style="color: rgb(0, 176, 240);">第一章 事故发生</span></strong></p><p>14日早8点左右，我从厚德路由东向西正常行驶，越过厚德路小学路口约10米后，前方一电动车突然左转横过机动车道然后逆行欲进入厚德路小学，由于她车速太快，电动车后座上反向乘坐的小女孩将左腿伸出车外，致小女孩左小腿撞在我车的右前大灯上。</p><p><br  /></p><p>事故发生后，小女孩拼命哭叫，我无法判断伤情（虽然我知道肯定没大事），对方也不提如何处理，对于这种手段老辣的人，为避免被敲诈，我决定报警、出险走正当程序。</p><p><br  /></p><p>警察、保险先后到场，警察也不主动提调解，我也就不提，因为我认为是对方全责，保险的希望我私了，还说不私了对我很不利，拖车、停车费是不赔的，当时有点心动想私了，但很不甘心，对方也不说私了的事，还强说我撞他，小孩又被120拉走，内心深处其实很想完整的走次事故程序（还没出过双方事故），也很不服拖车、停车费问题，想和交警较量下，再考虑到马上放假不太用车，最后还是决定走正常程序。</p><p><br  /></p><p>交警看双方不能现场调解，于是叫车来拖对方的电动车，扣下我的驾照，叫人开我的车去停车场，我也去直属大队事故科领回扣车单据。</p><p><br  /></p><p>回家马上查法规，等看到法律规定（《中华人民共和国行政强制法》第二十五条、第二十六条）停车、鉴定、检验费用由行政机关承担，心里踏实多了，也明白保险公司的险恶，他当然希望当事人私了，他就可以不费时、不费力、不出钱，还拿一般人很害怕的停车、拖车费来吓人，还好没吃他那套。继续查到事故责任认定书应当在10个工作日做出，事故责任认定书作出后就可以拿回车子，感觉更没什么了，也就半个月吧（当然各人情况不同，好车或没车不行的人另当别论），咱能等。</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">策略：事故发生后迅速判断各种因素，做出最有利选择</span></p><p><br  /></p><p><span style="color: rgb(0, 176, 240);"><strong>第二章 对扣车的行政强制行为提出行政复议</strong></span></p><p>怎么讲车都是被扣了，心里总还是不舒服也不服气的，其实车主都明白，交警扣车无非就是希望机动车主能赔钱结案，警察省事又不容易发生信访事件。很反感交警动不动就扣车，于是继续查法规，发现法律只规定“因为收集证据的需要”（《中华人民共和国道路交通安全法》第七十二条）才能扣押事故车辆而且扣押车辆需要履行很多手续（《中华人民共和国行政强制法》第十八条、第十九条、第二十四条），当时想不通这么简单也没有车辆损失的事故还要收集什么证据，而且扣押财物是要履行必要的法定程序的，于是马上写好行政复议申请书，下午就去交警支队。</p><p><br  /></p><p>到了支队进门要登记，一开始说去法制科，门卫说没这个科，于是说去行政复议办公室，门卫居然不知道有行政复议办公室，瞎指挥让我去7楼，整个7楼没有挂牌的办公室还都锁着，还好碰上2个穿制服的警察，问了以后说在4楼，于是去4楼，其实4楼是对交通事故责任认定书提出复核的地方，那里一个领导模样的人开始也以为是复核，问清是对行政强制行为复议后，打了电话问别人才说要去6楼秩序科，于是明白，很少有人提起行政复议啊，大家业务都不熟啊，中国真是刁民太少，天天在网上费唾沫没用啊，不如实际做点什么，赣州每天多少事故多少车被扣啊，别说多，每天有一个人复议，交警支队门斗挤破了，如果每个人对发生在自己身上的不公都提出行政复议、行政诉讼，中国早就官不聊生，早就不是现在这个样子了。</p><p><br  /></p><p>终于到了6楼，马上发现有个办公室挂着行政复议办公室的牌子，心里骂门卫：文盲加法盲，怎么TMD都这素质啊。可门锁着，正好以前坐电梯上来的几个警察也走过来，一问是行政复议的，马上进了秩序科办公室。和每次行政复议一样，对方总是推脱说些理由，一番唇枪舌剑后，最后表示什么也别说了，按行政复议法规定的程序办理吧，对方终于没办法说受理了。</p><p><br  /></p><p>一等就10多天，什么消息也没有，其实早就知道这样的结果，因为每次复议哪个行政机关都这样，25号过完节也放暑假了，于是再去交警支队，直奔秩序科，进门一说要拿行政复议受理通知书，对方和以前一样搬出领导不在要审批所以还没办等等说辞，我也冷处理，只问什么时候能拿到，一个主管模样的说今天一定会联系你，可今天26号了，还没动静，准备明天再去联系。</p><p><br  /></p><p>咱也不急，先施加压力，看事故中队这边责任认定书怎么出，如果不认定对方全责（其实全不全责都是保险赔，我只是找理由和警察PK），我就两边一起动作，同时打2场行政诉讼；如果认定对方全责，还要看拿回车子的过程是不是没有障碍，不爽的话还是打官司（呵呵，我无责也要打官司，要交警好看）。</p><p><br  /></p><p><span style="color: rgb(0, 176, 240);"><strong>第三章 调解、检验等前处理程序</strong></span></p><p>15日下午，到事故中队调解，我早考虑好了，直接说调解就是各走各路，看着对方拿着各种片子和单据想问我要钱的样子，我很想告诉他：首先钱应该保险公司出，如果是抢救（什么是抢救法律也是有明确定义的）费用交警会责令保险公司垫付，也是与我无关的，只有超出交强险限额的部分我才承担，其次，事故责任认定书出来后你也是与保险公司协商赔偿，我可以不介入的，如果对保险公司的赔偿不满只能向法院起诉，法院判保险赔多少让它赔，超出交强险限额的部分按责任法院判我赔多少我才赔，我的损失只是用车不方便，这么小的事故而且你全责，我很欢迎你去法院起诉，说不定我反诉你，我还可以拿点钱呢。当然没说，反正我没出1分钱，懒得和他怄气。</p><p><br  /></p><p>直接谈崩回家，其实真的是为了对方好也为了交警好，自我拔高下，呵呵：如果我赔了钱，对方下次还会违章，这次我是谨慎驾驶车速很慢，下次碰上个飙车的那结局很难想象，你就是赔了钱，人没了有什么意思？交警也就只图自己轻松，现在道路交通拥堵、事故多很多原因是行人、非机动车违章多又难管理、难处罚，好嘛，管不好行人、非机动车，交警就压迫机动车方，扣车迫你就范，让我这个机动车主免费帮你们教训下违章非机动车有什么不好啊。</p><p><br  /></p><p>21号去事故中队做笔录，本来也没啥说的，交警叫我复印下被扣的驾照，可气的是去4楼复印居然要1块1张，还是个律师事务所的，中国就这帮黑心的狗腿子搞坏了，懂法执法的人都不守法。浪费他2张纸，然后问他要FP，他不给FP，我也就转身去楼下找店家，靠，狗还真多，紧挨事故中队一家复印店先问复什么，一看是驾驶证也叫价1块，插，哪天我办完这件事，把这两家店一起投诉到工商局去。继续往前走，很快就找到家2角的，既锻炼了身体又省了钱，还战胜了黑心狗，还是蛮愉快的（咱是个闲人，时间多，其他人慎重），只是悲哀中国狗太多还有人闲着没事拯救狗的（赤那），更悲哀中国良民太多，那个律师事务所明显占用国家资源（办公室居然在行政机关内），与交警肯定有利益关联，而且非法超出范围经营（律所没有复印的营业执照的），也没一个人去投诉，难道我又要成为第一个吃螃蟹的人？</p><p><br  /></p><p>做笔录的时候，来了个处理事故的，可怜兮兮的样子，上来就发烟，还给我1支（我不抽烟，也没发烟给过交警），那警察也够坏，拿出好几份鉴定报告让那人去复印，还要那人去交鉴定费，那人很老实没有一句怨言地去了，我看着他的背影心底无限悲哀，***也只能现在这个样子了，人民就这水平啊，这样的水是不可能覆舟的，同时心底打定主意，以后决不去复印、更不可能交任何费用。</p><p><br  /></p><p>26号下午通知我去拿车辆性能检验报告，办案协警还是叫我去复印，我先拿过来看完检验结论合格，然后装傻，怎么叫我去复印啊，也许是昨天去支队要复议通知书的消息传到这边，起了效果，主办的正式民警知道我难对付，就叫协警别啰嗦自己去复印（我判断一定是去4楼那律所不要钱复印来的，黑啊），然后，告知我如果双方对检验结果都没有异议，就可以拿车了，还说不管你交不交费，都会放车。呵呵。</p><p><br  /></p><p><span style="color: rgb(0, 176, 240);"><strong>第四章 车辆技术性能鉴定报告</strong></span></p><p>拿到鉴定报告第一眼看结论，合格，2月年检了，意料之中，但也算放心了；可看见日期很不爽，送检居然写15号，这么简单的鉴定居然要11天到26号才出结果，很显然是交警与司法鉴定所穿一条裤子，要么是更改送检时间包庇交警（因为法律规定要在事故发生3个工作日内送检），要么是故意拖延时间，配合交警长时间扣车，法律对鉴定的期限规定太宽松，《交通事故处理程序规定》是20天，《司法鉴定管理条例》是15天，鉴定时间还不算入办案时间。</p><p><br  /></p><p>我的策略是去司法局投诉，虽然没有证据，但就按我自己猜测的写，既不要钱，写错了也不犯法，呵呵，投诉立案了司法局就会去查，查出问题自然好，一般来说总会有点问题的，特别是鉴定程序上，这些官办鉴定机构也是老爷作风；司法局这边也可能出错，反正司法局要出书面处理结果，到时再找他们的漏洞，不过司法鉴定的行政官司很难赢，法律基本空白，但可以行政复议，反正不要钱，但也止于行政复议。</p><p><br  /></p><p><strong><span style="color: rgb(0, 176, 240);">第五章 高潮：拿回自己的车</span></strong></p><p>26号拿鉴定报告，27、28、29三天双方都没有对鉴定结论提出异议，7月2号鉴定结论确定，但当天因有事去南昌没能去拿车，7月3号，办完事10点来的交警办公室，等到10点半大仙终于回来了，向办案的正式民警提出拿车的要求，这个家伙估计是早准备好的，居然说要等鉴定结论确定5天后才能领车，因为这几天对方可能申请法院诉前财产保全（浑蛋法律术语还知道点啊，可法律不行）；本来倒也早就预料交警会找借口，没什么，但可气的是边上一个凯莱律师事务所来办事的律师居然为讨好交警帮着他说些违背法律的说辞（唉，TMD狗真多，完全如先生狂人日记的情形）；我随即反驳：第一，法条明文规定是鉴定结论确定5日内领车，没有5日后领车的法条，要继续扣车请提供法律依据；第二，法院与你们不是一个系统，如果法院委托你们扣车请提供委托书。丫的居然还嘴硬，于是向他们领导投诉，回来交警改口了，自己还找个台阶：领导这么说了怎么办呢？好无辜的样子，勒个去的，过会打印车辆放行呈报表的时候审批理由里就是我说的法条，他根本就是知法犯法，丫的天天都在这么办还来故意刁难我，好吧，看我的。</p><p><br  /></p><p>吃完饭直奔吉埠新村停车场，交了放行单，如我所料守门的要我交19天20元/天的停车费，和开车来停车场的开车费50元，共计430元，于是我迅速拿回放行单，“还要交钱啊，交警没说啊”，守门的当然不肯。于是不废话，转身出来拨打110，报警说有人在吉埠新村非法扣押我的车辆并向我敲诈勒索，接警的也想混，说你们这是纠纷啊，自己解决啊。我说我有车辆放行单，车辆就处于合法自由状态，任何人阻拦就属于非法扣押私人财物，接着问你受理不受理，会不会出警？她没办法，说你等会。很快沙石派出所打来电话（这一点还是要肯定的，110必须出警现在规定的很严），还是一番唇枪舌剑，派出所的总推是纠纷，我坚持即使有纠纷任何人也不能非法扣押私人财产，没办法派出所答应出警。因为是午间休息时间，停车场那边与交警领导暂时沟通不上，于是民警说那就等会他们与交警联系了再说吧，我于是坚持回派出所作报案笔录（这点要强烈提醒各位朋友，千万别在现场傻等，一定要去派出所做笔录，否则，很可能警察出警后会记录为调解处理，很可能就没结果了，你要再提也还是要去派出所作笔录，被对方看出你不精于法律，再打交道就锐气尽失了），因为一旦形成笔录，这个案子就不是纠纷而是治安甚至刑事案件，派出所要结案（不按非法扣押财物处理，你可以行政复议），材料很多涉及面很广，派出所不愿意，自然会给停车场施压（毕竟其行为涉嫌犯法），交警也知道自己这么承包出去不合法，万一媒体曝光就很麻烦。</p><p><br  /></p><p>做完笔录再去停车场，还在路上，派出所的警官就打来电话：你去找停车场，我已经沟通好了。到了停车场还顺利，车都摆好位置了（第一次来我的车是被堵在里面的），老板娘来了，很气也很无奈的诅咒：我们这了敞开大门欢迎你常来，我回复：谢谢您吉言，心里说，我常来你也收不到钱。开车回家，第一件事洗车，去晦气；第二件事，发帖，哈哈。</p><p><br  /></p><p><span style="color: rgb(0, 176, 240);"><strong>第六章 余味：总结</strong></span></p><p><span style="color: rgb(112, 48, 160);">1、关于行政复议：</span>个人认为行政投诉和行政复议是公民个人维权最有效的手段，当发生侵害您权利的事件时（如您楼下某餐馆排油烟），确定侵权人管辖的行政机关和违反的法律条文，然后向行政机关写投诉书或现场投诉（如上例向卫生局、环保局投诉），如果行政机关不处理就向其上级机关行政复议，以我个人的经历基本全部能解决，当然这需要一定的法律技巧。特别的，行政复议是法律规定的行政机关必须受理处理的程序，在这个程序中，行政机关必须提供做出行政行为或不作出行政行为的法律依据和证据，行政复议必须受理且必须制作复议决定书，那么你就可以审查复议决定书是否合法，以及其依据的事实和理由是否合法，这样你就不用自己去搜集证据。这整个过程没有任何费用，花的时间也是很有限的，而收益是巨大的，很有效。</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">2、关于车辆技术性能鉴定：</span>依据法律规定，交警只有在需要收集证据的情况下才能扣押肇事车辆，而目前交警能提供的收集证据的最主要理由就是车辆技术性能鉴定，其实这是交警的借口，无非就是要扣车牟利，那么P民的策略也很简单，就是不出鉴定费，鉴定费这个东东可不像停车费，你不给就拿车很麻烦，交警一定会叫鉴定所先鉴定，因为这是法定的证据和程序，你不交鉴定费，交警也必须按法律规定的时限结案，所以在这点上，交警耗不起你，他是很怕你耍无赖的，像我的案例一样，交警看我难缠连要我交鉴定费的要求都不敢提。</p><p><br  /></p><p>如果每个车主都不交鉴定费（这其实真的很容易做到），那么交警就会内牛满面，你想想，如果车主不交钱，就只能按法律规定由交警承担，而交警会把招待、旅游、公车费压缩出来交给鉴定所去吗？不给的话，又有哪家鉴定所会与交警合作免费鉴定呢？而不鉴定，交警扣车的法定理由就消失了，所以，只要每个车主坚持不出鉴定费，很快，大家的车辆就不会被扣了，大家只要花很少的努力就可以有很大的收获，QS们要努力啊。</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">3、关于110：</span>个人的实践总结，目前110是维权的一个强有力手段，如果你是合法权益一方，你应该不会惧怕110民警到场，而110的到场会为确定现场证据、控制事态恶化、威慑侵权一方等等起到决定性的作用，目前110接警必须出警的规定非常有利于合法权益方保护自己。当然110报警需要一些小技巧：1、适当严重化事件，这不是诬告陷害，只要事实存在，你对事实性质无论怎么理解和严重定义，都不是诬告陷害，是不负法律责任的；2、如果你确定希望民警到场，你就只需要坚持要求民警到场就行，不需要多说理由，就问你受不受理、出不出警、什么时间出警就行了，110是不敢不出警的。</p><p><br  /></p><p>对于没有明显暴力事件，110一般都会按纠纷调解，如果你对处理不满一定要坚持报案，去派出所做笔录，笔录的时候亦可以按你的理解以最严重的罪名报案，坚持按你说的记录（因为这是你的笔录，你说了算），不要轻信民警的引导和对你措辞的修改，否则不签名，民警是不敢不做笔录的，因为这是110处警的必要法律文件。不要怕做笔录会浪费时间，这与你今后维权自己收集证据相比是完全值得的。</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">4、关于停车费：</span>法律有明确规定，只有你坚持，然后选择适合自己的策略和方式坚持自己的权益，交警是不敢向你开票收费的，而你真的没有义务向停车场缴费，因为你其实与停车场完全没有法律关系。特别重要的，如果没一个车主都不交停车费，那还有谁会免费为交警管理肇事违章车辆，你相信交警会从自己的经费里拿出钱来吗？如果没有停车费收入，每个停车场都要倒闭，那每个车主就都不会被扣车了，这实在是广大车主的最强福音啊。这会很难吗？</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">5、关于停车场：</span>本案中停车场挂出的牌子是“赣州市肇事车辆违章车辆管理处”，这个牌子会吓倒很多P民，其实，单从名称上分析，我就可以下结论，这是一个三无的非法组织。1、名称中没有冠以“赣州市公安局交通警察支队”说明不是交警的下属单位，现场也没有一个穿正规警服配警号的人员，在派出所联系交警时，交警也明确承认停车场是完全社会化的；我们也不可能在行政事业机构编制中找到肇事车辆违章车辆管理处这个单位；那么它是社会团体或企业法人吗，单从名称上“管理处”这个名字就不可能登记为任何社会团体或企业法人；所以毫无疑问的“赣州市肇事车辆违章车辆管理处”只是一块吓唬人的野鸡牌子，没有这个单位也就不可能有营业执照，也就不可能有收费许可证，那么它收费就完全站不住脚。在整个过程中停车场根本没有悬挂、出示过营业执照和收费许可证，这就是本案中停车场会屈服的根本原因。</p><p><br  /></p><p>因为我没有交费所以不知道以前的朋友们缴费单是什么样的，上面盖的是什么单位的章，如果你现在还保留着单据，又想拿回这些钱的话，给您支个招：1、先看是什么单据，应该必须是行政事业性收费单，如果是FP（其实FP是不合法的，因为你与停车场不存在合同关系，停车场收钱属于不当得利，可以民事诉讼，当然那个更麻烦，就不多说了）必须加盖公章；如果是行政事业性收费单，可以去财政查询单据的真实性和时效性，如果是FP可以去税务查（没FP直接投诉）；个人直接下定论，这些票据如果不是假的也一定存在瑕疵（如过期、与收费项目不符、收付单位与登记单位不同，因为根本没有停车场这个单位等等），然后要求财政或税务查处；3、进一步的可以去派出所报案诈骗，这个可以成立的，因为对方捏造了一个不存在的单位（也就是伪造事实），在没有合法收费依据的情况下使用虚假的单据，骗取财物，这个涉案金额很大啊，很可怕啊。再吹下牛，如果是我，坚持报案和行政复议（如果派出所不立案的话），一定会成功。</p><p><br  /></p><p>你说如果这件事变刑事案件了，或者媒体把这些法律关系曝光了，别说停车场怕，交警也要尿裤子啊。</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">6、其他途径：</span>至少，您可以向工商部门投诉非法经营。前面已经说了，以我的判断全国这类停车场90没有营业执照，100没有收费许可证或者收费项目不包含停车费，注意这里的停车费是指交警扣车发生的，不是自愿停车发生的。如果你耗不起车辆被扣时间，也可以在交费后，拿票据向工商投诉非法经营和强制交易（你与停车场没有合同关系），如果没有票据或票据有瑕疵还可以向税务投诉，很多投诉都只要打个电话不会很难。</p><p><br  /></p><p><span style="color: rgb(255, 0, 0);"><strong>第二篇 保</strong><strong>险赔</strong><strong>偿:事故责任认定复核</strong></span></p><p>7月6日下午，交警通知我去拿事故责任认定书，交警以“对道路的交通动态注意不够，忽视安全” “违反《中华人民共和国道路交通安全法》第二十二条第一款”为由，认定我负次要责任，很是无语，中国法律中口袋罪很多，对执法者的限定很少，好在我有准备，写好复核申请，9号上午就交去支队事故科。事故科的人说会把受理通知和结果寄给我，正好要上山度假1个月，也就不去管它，期间木有任何关于交通事故的电话给我。</p><p><br  /></p><p><span style="color: rgb(112, 48, 160);">道路交通事故认定复核申请书</span></p><p>申请人：***，男，19**年*月*日生，住址：赣州市章贡区红旗大道***，身份证号：****，电话：****</p><p><br  /></p><p>被申请人：赣州市公安局交通警察支队直属大队，地址：赣州市东郊路，电话：8123138</p><p><br  /></p><p>第三人： 陈XX，女，19**年*月*日生，住址：赣州市章贡区贸易广场西区*** ，身份证号：***，电话：***</p><p><br  /></p><p>申请人因为不服赣州市公安局交通警察支队直属大队赣市公交直认字[2012]第272号《道路交通事故认定书》，申请复核。</p><p><br  /></p><p><span style="color: rgb(0, 176, 240);">复核请求</span></p><p>依法撤销赣州市公安局交通警察支队直属大队赣市公交直认字[2012]第272号《道路交通事故认定书》，对本起交通事故重新作出责任认定，认定申请人不承担本次事故的责任。</p><p><br  /></p><p><span style="color: rgb(0, 176, 240);">事实和理由</span></p><p>一、认定道路交通事故发生经过与事实不符</p><p>原认定书认定： “2012年6月14日7时55分左右，陈XX（对方）驾驶赣州A404XX号电动自行车（搭载叶X）沿厚德路由西向东行驶至厚德路小学门前路段时，在左转弯过程中，叶X左脚与相对方向直行由XXX（本人）驾驶的赣BHXXXX号轿车擦刮”。</p><p><br  /></p><p>申请人认为与事实不符：</p><p>1、从现场示意图可知，碰撞点位于厚德路小学路口西端以西10米外，距道路北侧边缘1.5米处，第三人左转横过机动车道后因送女儿叶X上学欲进入厚德路小学路口，故沿道路北侧向东逆行，并非如原认定书所述“在左转弯过程中”。碰撞点距道路边缘仅1.5米，而且是叶X反向骑乘，左脚伸出电动车尾部发生擦刮，而该电动车的长度已经超过1.5米，在这样的距离内，第三人不可能继续横过道路向道路边缘（道路北侧）运动，因而第三人必然已经改变运动方向，而从第三人送女儿上学的目的可以判定：第三人必然沿道路北侧向东逆行欲进入厚德路小学入口。</p><p><br  /></p><p>2、从“叶X左脚与相对方向直行由XXX驾驶的赣BHXXXX号轿车擦刮”的认定可知，叶X为反向骑乘，因为，如果叶X正向骑乘，双方车辆右侧碰撞只能伤及叶X右侧肢体，而叶X左脚受伤故为反向骑乘，由于叶X反向骑乘而第三人车辆后座有较大的靠背，致使叶X左脚不得不外伸，超出第三人车辆车把宽度与车尾，因而与申请人车辆擦刮。被申请人对如此重要的事实没有认定是严重失职。</p><p><br  /></p><p>二、认定交通事故成因错误</p><p>原认定书认定：“当事人XXX当日驾驶机动车行驶至学校门前路段时，对道路的交通动态注意不够，忽视安全”。</p><p><br  /></p><p>申请人认为是错误的：</p><p><br  /></p><p>1、被申请人没有任何证据证明申请人“对道路的交通动态注意不够，忽视安全”。</p><p><br  /></p><p>2、申请人在观察到第三人突然骑行左转后，立即采取了制动措施，申请人车辆已经停止，是第三人逆行主动向申请人运动靠近发生擦刮，申请人已经做到了注意观察、安全驾驶。</p><p><br  /></p><p>3、申请人不可能对第三人违反交通规则突然骑行快速横过机动车道的“交通动态”进行预判和注意，在第三人横过机动车道后其合理的运动方向应该是沿道路北侧向西与申请人同向运动，那么两车的相对运动速度为零甚至远离，是不可能发生碰撞事故的；申请人更不可能预见第三人在违章横过道路后又继续违章逆行，被申请人的认定违反常识。</p><p><br  /></p><p>三、适用法律错误</p><p>原认定书认为：“当事人XXX违反《中华人民共和国道路交通安全法》第二十二条第一款”，申请人认为适用法律错误：在司法实践中《中华人民共和国道路交通安全法》第二十二条第一款属于兜底条款，只适用于有明确违法过错行为但法律法规没有与之相对应的具体规定的情况，被申请人在没有任何证据证明申请人有违法过错行为的情况下适用该条款属于滥用职权。</p><p><br  /></p><p>四、严重失职，遗漏重要证据</p><p>1、本事故发生后，被申请人暂扣了双方的车辆，但只对申请人的机动车进行了技术性能检验，未对第三人驾驶的电动自行车进行技术性能检验，故不能排除本事故是由于第三人电动自行车技术性能不合格引发的。应责令被申请人补充证据。</p><p><br  /></p><p>2、未验证第三人电动自行车是否超载</p><p><br  /></p><p>根据《2010年国民体质监测公报》（国家体育总局，2011年9月2日，网址：<span style="color: rgb(0, 0, 0);">http://www.gov.cn/test/2012-04/19/content_2117320.htm）的数据，7岁女童的平均体重为23.8千克，35-39岁成年女性的平均体重为56.9千克，发生事故时第三人（39岁女性）搭载叶X（7岁女童），按平均体重计算2人总重达80.7千克，而国家规定电动自行车载重为75千克，所以发生事故时第三人电动自行车可能出于超载状态，不能排除本事故是因为第三人电动自行车超载造成方向和刹车失控引发的。实际上，第三人体型明显偏胖，目测体重就超过75千克，但被申请人对该事实视而不见，玩忽职守，恶意遗漏重要证据。</span> </p><p><br  /></p><p>综上所述，1、本事故主要原因是由于第三人在违章横过道路后又强行违章逆行，主动靠近申请人车辆；次要原因是第三人搭载的叶X反向骑乘左脚伸出车外，致使叶X左脚与申请人车辆发生擦刮；2、申请人遵章正常行驶，已尽到注意观察、安全驾驶义务，且采取了制动停车行为，没有任何不当行为，不应承担责任；3、被申请人遗漏了可能影响认定结论的重要证据，应责令补充。</p><p><br  /></p><p>故请依法依法撤销赣州市公安局交通警察支队直属大队赣市公交直认字[2012]第272号《道路交通事故认定书》，对本起交通事故重新作出责任认定，认定申请人不承担本次事故的责任</p><p><img style="height: auto !important; visibility: visible !important;" data-src="http://mmbiz.qpic.cn/mmbiz/OoJVctT8ACF2NZPct2GRhGOd7viaWv53qkPoCiabZDZP0lyKW58zzknWuaeCR36GRpxqHtjfWlTPOibK1meFIUuYA/0" data-src="http://mmbiz.qpic.cn/mmbiz/OoJVctT8ACF2NZPct2GRhGOd7viaWv53qkPoCiabZDZP0lyKW58zzknWuaeCR36GRpxqHtjfWlTPOibK1meFIUuYA/0" data-ratio="0.03762376237623762" data-w=""  /></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><strong style="line-height: 2em; text-indent: 28px; font-family: verdana, arial, helvetica, sans-serif; font-size: 14px; max-width: 100%; orphans: 2; widows: 2;"><span style="color: rgb(255, 0, 0); font-size: 16px; max-width: 100%;"><strong style="color: rgb(62, 62, 62); line-height: 2em; max-width: 100%;"><span style="color: rgb(255, 0, 0); font-size: 14px; max-width: 100%;">也想每天收到这样的文章吗？到本文最上面一键免费关注我吧</span></strong></span></strong><strong style="max-width: 100%;"><span style="color: rgb(255, 0, 0); max-width: 100%;"><span style="font-size: 14px; max-width: 100%;">↑</span></span></strong></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><img style="border: 0px currentColor; border-image: none; height: auto !important; font-family: verdana, arial, helvetica, sans-serif; font-size: medium; visibility: visible !important;" data-src="http://mmsns.qpic.cn/mmsns/vVEysRjVVwLWSWhIqMbDf8Cy9QHWgrrXicdAjroLsJ62m3DvyHfIBVA/0" data-src="http://mmsns.qpic.cn/mmsns/vVEysRjVVwLWSWhIqMbDf8Cy9QHWgrrXicdAjroLsJ62m3DvyHfIBVA/0" data-ratio="0.041584158415841586" data-w="" data_ue_src="http://mmsns.qpic.cn/mmsns/vVEysRjVVwLWSWhIqMbDf8Cy9QHWgrrXicdAjroLsJ62m3DvyHfIBVA/0"  /></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><span style="color: rgb(255, 0, 0); max-width: 100%; background-color: rgb(255, 255, 0);"><strong style="max-width: 100%;"><span style="line-height: 25px; text-indent: 28px; font-size: 14px; max-width: 100%; orphans: 2; widows: 2;">关于我们</span></strong></span></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><span style="color: rgb(255, 255, 255); max-width: 100%; background-color: rgb(0, 0, 0);"><span style="line-height: 25px; text-indent: 28px; font-size: 14px; max-width: 100%; orphans: 2; widows: 2;"></span></span><strong style="max-width: 100%;"><span style="font-size: 14px; max-width: 100%;">微信名：掌上石家庄</span></strong></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><strong style="max-width: 100%;"><span style="font-size: 14px; max-width: 100%;">微信号：</span><span style="color: rgb(255, 0, 0); font-size: 14px; max-width: 100%;">sjz400</span></strong></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><strong style="max-width: 100%;"><span style="font-size: 14px; max-width: 100%;">简介</span></strong><span style="font-size: 14px; max-width: 100%;">：<span style="color: rgb(51, 51, 51); line-height: 25px; text-indent: 28px; max-width: 100%;">懂生活、更有趣。这里是石家庄人的生活指南，爱血拼、找美食、买房子、搞装修、切旅游、要结婚、生小孩、养宠物......只有便民的，一掌尽收。乐在掌上，玩转石家庄！</span><span style="color: rgb(0, 176, 80); max-width: 100%;">（本文最上面可一键免费关注）</span></span></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><span style="color: rgb(255, 0, 0); max-width: 100%; background-color: rgb(255, 255, 0);"><strong style="max-width: 100%;"><span style="line-height: 25px; text-indent: 28px; font-size: 14px; max-width: 100%; orphans: 2; widows: 2;"><span style="max-width: 100%;"><strong style="max-width: 100%;"><span style="max-width: 100%;">正在做的事</span></strong></span></span></strong></span></p><p style="padding: 0px; color: rgb(62, 62, 62); line-height: 2em; font-family: 宋体; margin-top: 0px; margin-bottom: 0px; white-space: normal; min-height: 1.5em; max-width: 100%; background-color: rgb(255, 255, 255);"><span style="font-size: 14px; max-width: 100%;"><span style="color: rgb(51, 51, 51); line-height: 25px; text-indent: 28px; max-width: 100%;"> 通过<span style="color: rgb(255, 0, 0); max-width: 100%;"><strong style="max-width: 100%;">掌上石家庄</strong></span>把整个石家庄形象打包，以新媒体形式让更多人了解、喜欢她。</span></span></p><p><br  /></p></div>

                    <script type="text/javascript">
                        var first_sceen__time = (+new Date());
                    </script>
                    
                    <div class="rich_media_tool" id="js_toobar">

                                                                                                

                        <a class="media_tool_meta link_primary meta_extra" href="javascript:report_article();">举报</a>
                    </div>
                </div>

                
                            </div>
            <div id="js_pc_qr_code" class="qr_code_pc_outer" style="display:none;">
                <div class="qr_code_pc_inner">
                    <div class="qr_code_pc">
                        <img id="js_pc_qr_code_img" class="qr_code_pc_img">
                        <p>微信扫一扫<br>获得更多内容</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
           
    <script type="text/javascript">
        var tid = "";
        var aid = "";
        var uin = "";
        var key = "";
        var biz = "MzA3MjY1OTIwMQ==";
        var mid = "200466770";
        var idx = "2";
        var itemidx = "";
        var nickname = "掌上石家庄";
        var user_name = "gh_2fdf3c5652b2";
        var fakeid   = "MTUyNjQwMTc0Mw==";
        var version   = "";
        var is_limit_user   = "0";
        var msg_title = "一名赣州车主处理“碰瓷”过程，值得认真学习";
        var msg_desc = "中国是一个知法犯法的国度，执法的都在犯法，但是没人懂太多的法律，就是一帮愚民~~惧怕官老爷习惯了，今天推荐《";
        var msg_cdn_url = "http://mmbiz.qpic.cn/mmbiz/iblyVZZUyqusfF8ql0YeUqYTib2K0rXgNsYEJCsS1oIm0rYHSY7HiamvKk1PFQTJJcqsibt3fP4eYIdIWDOKwbcQIA/0";
        <%
			HttpServletRequest req = (HttpServletRequest) request;			
		%>
        var msg_link = "http://5.175.145.238:8080<%=req.getSession().getAttribute("url")%>?__biz=MzA3MjY1OTIwMQ==&amp;mid=200466770&amp;idx=2&amp;sn=09d8359b4ef2ae3792fca461f87924fe#rd";
        var msg_source_url = '';
        var networkType;
        var appmsgid = '' || '200466770';
        var abtest = 0;//(0) % 2;//默认都用灰色广告 不做ABTEST了
                var readNum = 1;
                  
        var likeNum = '赞';
            </script>
            <script type="text/javascript">function setProperty(e, t, n, r) {
e.style.setProperty ? e.style.setProperty(t, n, r) : e.style.cssText && (e.style.cssText += t + ":" + n + "!" + r + ";");
}

function like_report(e) {
var tmpAttr = document.getElementById("like").getAttribute("like"), tmpHtml = document.getElementById("likeNum").innerHTML, isLike = parseInt(tmpAttr) ? parseInt(tmpAttr) : 0, like = isLike ? 0 : 1, likeNum = parseInt(tmpHtml) ? parseInt(tmpHtml) : 0;
ajax({
url: "/mp/appmsg_like?__biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&uin=" + uin + "&key=" + key + "&like=" + like + "&f=json&appmsgid=" + appmsgid + "&itemidx=" + itemidx,
type: "GET",
timeout: 2e3,
success: function(res) {
var data = eval("(" + res + ")");
data.base_resp.ret == 0 && (isLike ? (removeClass(document.getElementById("like"), "praised"), document.getElementById("like").setAttribute("like", 0), likeNum > 0 && tmpHtml !== "100000+" && (document.getElementById("likeNum").innerHTML = likeNum - 1 == 0 ? "赞" : likeNum - 1)) : (document.getElementById("like").setAttribute("like", 1), addClass(document.getElementById("like"), "praised"), tmpHtml !== "100000+" && (document.getElementById("likeNum").innerHTML = likeNum + 1)));
},
async: !0
});
}

function hasClass(e, t) {
return e.className.match(new RegExp("(\\s|^)" + t + "(\\s|$)"));
}

function addClass(e, t) {
this.hasClass(e, t) || (e.className += " " + t);
}

function removeClass(e, t) {
if (hasClass(e, t)) {
var n = new RegExp("(\\s|^)" + t + "(\\s|$)");
e.className = e.className.replace(n, " ");
}
}

function toggleClass(e, t) {
hasClass(e, t) ? removeClass(e, t) : addClass(e, t);
}

function hash(e) {
var t = 5381;
for (var n = 0; n < e.length; n++) t = (t << 5) + t + e.charCodeAt(n), t &= 2147483647;
return t;
}

function trim(e) {
return e.replace(/^\s*|\s*$/g, "");
}

function ajax(e) {
var t = (e.type || "GET").toUpperCase(), n = e.url, r = typeof e.async == "undefined" ? !0 : e.async, i = typeof e.data == "string" ? e.data : null, s = new XMLHttpRequest, o = null;
s.open(t, n, r), s.onreadystatechange = function() {
s.readyState == 3 && e.received && e.received(s), s.readyState == 4 && (s.status >= 200 && s.status < 400 && (clearTimeout(o), e.success && e.success(s.responseText)), e.complete && e.complete(), e.complete = null);
}, t == "POST" && s.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"), s.setRequestHeader("X-Requested-With", "XMLHttpRequest"), s.send(i), typeof e.timeout != "undefined" && (o = setTimeout(function() {
s.abort("timeout"), e.complete && e.complete(), e.complete = null;
}, e.timeout));
}

function report_article() {
var e = sourceurl == "" ? location.href : sourceurl, t = [ nickname, location.href, title, e ].join("|WXM|");
location.href = "/mp/readtemplate?t=wxm-appmsg-inform&__biz=" + biz + "&info=" + encodeURIComponent(t) + "#wechat_redirect";
}

function viewSource() {
var redirectUrl = sourceurl.indexOf("://") < 0 ? "http://" + sourceurl : sourceurl;
redirectUrl = "http://" + location.host + "/mp/redirect?url=" + encodeURIComponent(sourceurl);
var opt = {
url: "/mp/advertisement_report" + location.search + "&report_type=3&action_type=0&url=" + encodeURIComponent(sourceurl) + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&r=" + Math.random(),
type: "GET",
async: !1
};
return tid ? opt.success = function(res) {
try {
res = eval("(" + res + ")");
} catch (e) {
res = {};
}
res && res.ret == 0 ? location.href = redirectUrl : viewSource();
} : (opt.timeout = 2e3, opt.complete = function() {
location.href = redirectUrl;
}), ajax(opt), !1;
}

function parseParams(e) {
if (!e) return {};
var t = e.split("&"), n = {}, r = "";
for (var i = 0, s = t.length; i < s; i++) r = t[i].split("="), n[r[0]] = r[1];
return n;
}

function htmlDecode(e) {
return e.replace(/&#39;/g, "'").replace(/<br\s*(\/)?\s*>/g, "\n").replace(/&nbsp;/g, " ").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&amp;/g, "&");
}

function report(e, t, n) {
var r = e.split("?").pop();
r = r.split("#").shift();
if (r == "") return;
var i = [ r, "action_type=" + n, "uin=" + t ].join("&");
ajax({
url: "/mp/appmsg/show",
type: "POST",
timeout: 2e3,
data: i
});
}

function reportTimeOnPage() {
var e = location.href, t = e.split("?").pop();
t = t.split("#").shift();
if (t == "") return;
var n = [ t, "start_time=" + _wxao.begin, "end_time=" + (new Date).getTime(), "uin=" + fakeid, "title=" + encodeURIComponent(title), "action=pagetime" ].join("&");
ajax({
url: "/mp/appmsg/show?" + n,
async: !1,
timeout: 2e3
});
}

function share_scene(e, t) {
var n = "";
tid != "" && (n = "tid=" + tid + "&aid=" + 54);
var r = e.split("?")[1] || "";
r = r.split("#")[0];
if (r == "") return;
var i = [ r, "scene=" + t ];
return n != "" && i.push(n), r = i.join("&"), e.split("?")[0] + "?" + r + "#" + (e.split("#")[1] || "");
}

function get_url(e, t) {
t = t || "";
var n = e.split("?")[1] || "";
n = n.split("#")[0];
if (n == "") return;
var r = [ n ];
return t != "" && r.push(t), n = r.join("&"), e.split("?")[0] + "?" + n + "#" + (e.split("#")[1] || "");
}

function viewProfile() {
typeof WeixinJSBridge != "undefined" && WeixinJSBridge.invoke && WeixinJSBridge.invoke("profile", {
username: user_name,
scene: "57"
});
}

function gdt_click(e, t, n, r, i, s, o, u, a) {
if (has_click[i]) return;
has_click[i] = !0;
var f = document.getElementById("loading_" + i);
f && (f.style.display = "inline");
var l = +(new Date);
ajax({
url: "/mp/advertisement_report?report_type=2&type=" + e + "&url=" + encodeURIComponent(t) + "&tid=" + i + "&rl=" + encodeURIComponent(n) + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&pt=" + a + "&r=" + Math.random() + abtest_param,
type: "GET",
timeout: 1e3,
complete: function(n) {
has_click[i] = !1, f && (f.style.display = "none");
if (e == "5") location.href = "/mp/profile?source=from_ad&tousername=" + t + "&ticket=" + s + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&tid=" + i; else {
var r = "source=4&tid=" + i + "&idx=" + idx + "&mid=" + mid + "&appuin=" + biz + "&pt=" + a + "&aid=" + u;
t.indexOf("mp.weixin.qq.com") == -1 && (r = ""), location.href = get_url(t, r);
}
},
async: !0
});
}

function addEvent(e, t, n) {
window.addEventListener ? e.addEventListener(t, n, !1) : window.attachEvent ? e.attachEvent("on" + t, function(e) {
return function(t) {
n.call(e, t);
};
}(e)) : e["on" + t] = n;
}

function log(e) {
var t = document.getElementById("log");
if (t) {
var n = t.innerHTML;
t.innerHTML = n + "<div>" + e + "</div>";
}
}

function initpicReport() {
function e(e) {
var t = [];
for (var n in e) t.push(n + "=" + encodeURIComponent(e[n] || ""));
return t.join("&");
}
if (!networkType) return;
var t = window.performance || window.msPerformance || window.webkitPerformance, n = null;
if (!t || typeof t.getEntries == "undefined") return;
var r, i = 100, s = document.getElementsByTagName("img"), o = s.length, u = navigator.userAgent, a, f = !1;
/micromessenger\/(\d+\.\d+)/i.test(u), a = RegExp.$1;
for (var l = 0, c = s.length; l < c; l++) {
r = parseInt(Math.random() * 100);
if (r > i) continue;
var h = s[l].getAttribute("src");
if (h.indexOf("mp.weixin.qq.com") >= 0) continue;
var p = t.getEntries(), d;
for (var v = 0; v < p.length; v++) {
d = p[v];
if (d.name == h) {
ajax({
type: "POST",
url: "/mp/appmsgpicreport?__biz=" + biz + "&uin=" + uin + "&key=" + key + "#wechat_redirect",
data: e({
rnd: Math.random(),
uin: uin,
version: version,
client_version: a,
device: navigator.userAgent,
time_stamp: parseInt(+(new Date) / 1e3),
url: h,
img_size: s[l].fileSize || 0,
user_agent: navigator.userAgent,
net_type: networkType,
appmsg_id: window.appmsgid || "",
sample: o > 100 ? 100 : o,
delay_time: parseInt(d.duration)
})
}), f = !0;
break;
}
}
if (f) break;
}
}

var ISWP = !!navigator.userAgent.match(/Windows\sPhone/i), sw = 0, imgs_info = {}, abtest_param = "&ab_test_id=color&ab_test_value=gray", outsize_link = 0;

(function() {
parseInt(readNum) > 1e5 ? readNum = "100000+" : "", parseInt(likeNum) > 1e5 ? likeNum = "100000+" : "", document.getElementById("readNum") && (document.getElementById("readNum").innerHTML = readNum), document.getElementById("likeNum") && (document.getElementById("likeNum").innerHTML = likeNum);
})(), function() {
var e = document.getElementById("js_content");
if (!e) return !1;
var t = e.getElementsByTagName("a") || [];
for (var n = 0, r = t.length; n < r; ++n) (function(e) {
var n = t[e], r = n.getAttribute("href");
if (!r) return !1;
r.indexOf("http://mp.weixin.qq.com") != 0 && r.indexOf("http://mp.weixin.qq.com") != 0 && outsize_link++;
var i = 0, s = n.innerHTML;
/^[^<>]+$/.test(s) ? i = 1 : /^<img[^>]*>$/.test(s) && (i = 2), r.indexOf("http://mp.weixin.qq.com/mp/redirect") != 0 && (r = "http://" + location.host + "/mp/redirect?url=" + encodeURIComponent(r) + "&action=appmsg_redirect" + "&uin=" + uin + "&biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&type=" + i + "&scene=0"), n.addEventListener ? n.addEventListener("click", function(e) {
e.stopPropagation && e.stopPropagation(), e.preventDefault && e.preventDefault(), location.href = r;
}, !0) : n.attachEvent && n.attachEvent("click", function(e) {
e.stopPropagation && e.stopPropagation(), e.preventDefault && e.preventDefault(), location.href = r;
}, !0);
})(n);
}(), function() {
var e = document.getElementById("gdt_area");
if (abtest == 1 && !!e) {
var t = e.getElementsByTagName("a");
for (var n = 0, r = t.length; n < r; ++n) t[n].setAttribute("class", "random_empha");
}
}(), function() {
var e = document.getElementById("js_pc_qr_code_img");
if (!!e && navigator.userAgent.indexOf("MicroMessenger") == -1) {
var t = 10000004, n = document.referrer;
n.indexOf("http://weixin.sogou.com") == 0 ? t = 10000001 : n.indexOf("https://wx.qq.com") == 0 && (t = 10000003), e.setAttribute("src", "/mp/qrcode?scene=" + t + "&size=102&__biz=" + biz), document.getElementById("js_pc_qr_code").style.display = "block";
}
}(), function() {
var e = document.getElementById("gdt_area");
!!e && navigator.userAgent.indexOf("MicroMessenger") == -1 && (e.style.display = "none");
}(), function() {
function e(e) {
var t = 0;
e.contentDocument && e.contentDocument.body.offsetHeight ? t = e.contentDocument.body.offsetHeight : e.Document && e.Document.body && e.Document.body.scrollHeight ? t = e.Document.body.scrollHeight : e.document && e.document.body && e.document.body.scrollHeight && (t = e.document.body.scrollHeight);
var n = e.parentElement;
!n || (e.style.height = t + "px");
}
var t = document.getElementsByTagName("iframe"), n;
for (var r = 0, i = t.length; r < i; ++r) {
n = t[r];
var s = n.getAttribute("data-src"), o = n.getAttribute("class");
!!s && (s.indexOf("http://mp.weixin.qq.com/mp/appmsgvote") == 0 && o.indexOf("js_editor_vote_card") >= 0 || s.indexOf("http://mp.weixin.qq.com/bizmall/appmsgcard") == 0 && o.indexOf("card_iframe") >= 0) && (n.setAttribute("src", s.replace("#wechat_redirect", [ "&uin=", uin, "&key=", key ].join(""))), function(t) {
t.onload = function() {
e(t);
};
}(n), n.appmsg_idx = r);
}
window.iframe_reload = function(r) {
for (var i = 0, s = t.length; i < s; ++i) {
n = t[i];
var o = n.getAttribute("src");
!!o && o.indexOf("http://mp.weixin.qq.com/mp/appmsgvote") == 0 && e(n);
}
};
}();

if (ISWP) {
var profile = document.getElementById("post-user");
profile && profile.setAttribute("href", "weixin://profile/" + user_name);
}

var cookie = {
get: function(e) {
if (e == "") return "";
var t = new RegExp(e + "=([^;]*)"), n = document.cookie.match(t);
return n && n[1] || "";
},
set: function(e, t) {
var n = new Date;
n.setDate(n.getDate() + 1);
var r = n.toGMTString();
return document.cookie = e + "=" + t + ";expires=" + r, !0;
}
}, title = trim(htmlDecode(msg_title)), sourceurl = trim(htmlDecode(msg_source_url));

msg_link = htmlDecode(msg_link), function() {
function e() {
var e = "", t = msg_cdn_url, n = msg_link, r = htmlDecode(msg_title), i = htmlDecode(msg_desc);
i = i || n, WeixinJSBridge.call("hideToolbar"), "1" == is_limit_user && WeixinJSBridge.call("hideOptionMenu"), WeixinJSBridge.on("menu:share:appmessage", function(s) {
var o = 1;
s.scene == "favorite" && (o = 4), WeixinJSBridge.invoke("sendAppMessage", {
appid: e,
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, o),
desc: i,
title: r
}, function(e) {
report(n, fakeid, o);
});
}), WeixinJSBridge.on("menu:share:timeline", function(e) {
report(n, fakeid, 2), WeixinJSBridge.invoke("shareTimeline", {
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, 2),
desc: i,
title: r
}, function(e) {});
});
var s = "";
WeixinJSBridge.on("menu:share:weibo", function(e) {
WeixinJSBridge.invoke("shareWeibo", {
content: r + share_scene(n, 3),
url: share_scene(n, 3)
}, function(e) {
report(n, fakeid, 3);
});
}), WeixinJSBridge.on("menu:share:facebook", function(e) {
report(n, fakeid, 4), WeixinJSBridge.invoke("shareFB", {
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, 4),
desc: i,
title: r
}, function(e) {});
}), WeixinJSBridge.on("menu:general:share", function(s) {
var o = 0;
switch (s.shareTo) {
case "friend":
o = 1;
break;
case "timeline":
o = 2;
break;
case "weibo":
o = 3;
}
s.generalShare({
appid: e,
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, o),
desc: i,
title: r
}, function(e) {
report(n, fakeid, o);
});
});
var o = {
"network_type:fail": "fail",
"network_type:edge": "2g",
"network_type:wwan": "3g",
"network_type:wifi": "wifi"
};
typeof WeixinJSBridge != "undefined" && WeixinJSBridge.invoke && WeixinJSBridge.invoke("getNetworkType", {}, function(e) {
networkType = o[e.err_msg], initpicReport();
});
}
typeof WeixinJSBridge == "undefined" ? document.addEventListener ? document.addEventListener("WeixinJSBridgeReady", e, !1) : document.attachEvent && (document.attachEvent("WeixinJSBridgeReady", e), document.attachEvent("onWeixinJSBridgeReady", e)) : e();
}(), function() {
var e = null, t = 0, n = msg_link.split("?").pop(), r = hash(n);
window.addEventListener ? (window.addEventListener("load", function() {
t = cookie.get(r), window.scrollTo(0, t);
}, !1), window.addEventListener("unload", function() {
cookie.set(r, t), reportTimeOnPage();
}, !1), window.addEventListener("scroll", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1), document.addEventListener("touchmove", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1)) : window.attachEvent && (window.attachEvent("load", function() {
t = cookie.get(r), window.scrollTo(0, t);
}, !1), window.attachEvent("unload", function() {
cookie.set(r, t), reportTimeOnPage();
}, !1), window.attachEvent("scroll", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1), document.attachEvent("touchmove", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1));
}(), function() {
function e(e) {
typeof window.WeixinJSBridge != "undefined" && WeixinJSBridge.invoke("imagePreview", {
current: e,
urls: n
});
}
function t() {
var t = document.getElementById("img-content");
t = t ? t.getElementsByTagName("img") : [];
for (var r = 0, i = t.length; r < i; r++) {
var s = t.item(r), o = s.getAttribute("data-src") || s.getAttribute("src");
o && (!!s.dataset && !!s.dataset.s && o.indexOf("http://mmbiz.qpic.cn") == 0 && (o = o.replace(/\/640$/, "/0")), n.push(o), function(t) {
s.addEventListener ? s.addEventListener("click", function() {
e(t);
}, !1) : s.attachEvent && s.attachEvent("click", function() {
e(t);
}, !1);
}(o));
}
}
var n = [];
window.addEventListener ? window.addEventListener("load", t, !1) : window.attachEvent && (window.attachEvent("load", t), window.attachEvent("onload", t));
}();

var has_click = {};

(function() {
var e = document.getElementById("gdt_area");
if (!e) return !1;
var t = e.getElementsByClassName("js_ad_link") || [];
for (var n = 0, r = t.length; n < r; ++n) (function(e) {
var n = t[e], r = n.dataset, i = r.type, s = r.url, o = r.rl, u = r.apurl, a = r.tid, f = r.ticket, l = r.group_id, c = r.aid, h = r.pt;
n.addEventListener ? n.addEventListener("click", function(e) {
e.stopPropagation && e.stopPropagation(), e.preventDefault && e.preventDefault(), gdt_click(i, s, o, u, a, f, l, c, h);
}, !0) : n.attachEvent && n.attachEvent("click", function(e) {
e.stopPropagation && e.stopPropagation(), e.preventDefault && e.preventDefault(), gdt_click(i, s, o, u, a, f, l, c, h);
}, !0);
})(n);
})(), function() {
function detect() {
var e = (window.pageYOffset || document.documentElement.scrollTop) - 20, t = +(new Date);
for (var n = 0, r = images.length; n < r; n++) {
var i = images[n];
imgs_info[t] = imgs_info[t] || [];
var s = i.el.offsetTop;
if (!i.show && e < s + i.height && e + height > s) {
var o = i.src;
!!i.el.dataset && !!i.el.dataset.s && o.indexOf("http://mmbiz.qpic.cn") == 0 && (o = o.replace(/\/0$/, "/640")), i.el.onload = function() {
var e = this;
setProperty(e, "height", "auto", "important");
}, i.el.setAttribute("src", o), imgs_info[t].push(o), i.show = !0, setProperty(i.el, "visibility", "visible", "important");
}
ISWP && i.el.width * 1 > sw && (i.el.width = sw);
}
}
function onScroll() {
clearTimeout(timer), timer = setTimeout(detect, 300);
var performance = window.performance || window.msPerformance || window.webkitPerformance;
!has_report_img_time && !!performance && typeof performance.getEntries != "undefined" && function() {
var e = window.pageYOffset || document.documentElement.scrollTop, t = document.getElementById("js_toobar"), n = t.offsetTop;
if (e + innerHeight >= n) {
var r = performance.getEntries(), i = new Image, s = "http://isdspeed.qq.com/cgi-bin/r.cgi?flag1=7839&flag2=7&flag3=1", o = 0, u = 0, a = 0, f = 0, l = [], c = [], h = {};
for (var p = 0, d = r.length; p < d; ++p) {
var v = r[p], m = v.name;
!!m && v.initiatorType == "img" && (m.indexOf("http://mmbiz.qpic.cn") == 0 && (u++, f += v.duration), o++, a += v.duration, h[m] = {
startTime: v.startTime,
responseEnd: v.responseEnd
});
}
if (o > 0 || u > 0) {
o > 0 && (c.push("1=" + Math.round(a / o)), c.push("2=" + o * 1e3), c.push("3=" + Math.round(a))), u > 0 && (c.push("4=" + Math.round(f / u)), c.push("5=" + u * 1e3), c.push("6=" + Math.round(f)));
var g = 0, y = 0, b = 0;
for (var p in imgs_info) if (imgs_info.hasOwnProperty(p)) {
var w = imgs_info[p], E = 0, S = Infinity, x = 0, T = Infinity, N = !1, C = !1;
for (var k = 0, L = w.length; k < L; ++k) {
var m = w[k], A = h[m];
!A || (N = !0, E = Math.max(E, A.responseEnd), S = Math.min(S, A.startTime), m.indexOf("http://mmbiz.qpic.cn") == 0 && (x = Math.max(x, A.responseEnd), T = Math.min(T, A.startTime), C = !0));
}
N && (y += Math.round(E - S)), C && (b += Math.round(x - T));
}
c.push("7=" + y), c.push("8=" + b), i.src = s + "&" + c.join("&");
}
has_report_img_time = !0;
}
}();
var gdt_area = document.getElementById("gdt_area");
if (!!gdt_area && !ping_apurl) {
var scrollTop = window.pageYOffset || document.documentElement.scrollTop, offsetTop = gdt_area.offsetTop;
if (scrollTop + innerHeight > offsetTop) {
var gdt_a = document.querySelectorAll("#gdt_area a");
if (gdt_a.length) {
gdt_a = gdt_a[0];
if (gdt_a.dataset && gdt_a.dataset.apurl) {
ping_apurl = !0;
var gid = gdt_a.dataset.gid, tid = gdt_a.dataset.tid;
ajax({
url: "/mp/advertisement_report?report_type=1&tid=" + tid + "&adver_group_id=" + gid + "&apurl=" + encodeURIComponent(gdt_a.dataset.apurl) + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&r=" + Math.random() + abtest_param,
success: function(res) {
try {
res = eval("(" + res + ")");
} catch (e) {
res = {};
}
res && res.ret != 0 && (ping_apurl = !1);
},
async: !0
});
}
}
}
}
}
function onLoad() {
var e = document.getElementsByTagName("img"), t = document.getElementById("page-content");
t.currentStyle ? sw = t.currentStyle.width : typeof getComputedStyle != "undefined" && (sw = getComputedStyle(t).width), sw = 1 * sw.replace("px", "");
for (var n = 0, r = e.length; n < r; n++) {
var i = e.item(n);
if (!i.getAttribute("data-src")) continue;
images.push({
el: i,
src: i.getAttribute("data-src"),
height: i.offsetHeight,
show: !1
});
var s = 100;
if (!!i.dataset && !!i.dataset.ratio) {
var o = i.dataset.ratio * 1, u = i.dataset.w * 1 || imgMaxW;
typeof o == "number" && o > 0 && (u = i.offsetWidth || (u <= imgMaxW ? u : imgMaxW), s = u * o);
}
setProperty(i, "height", s + "px", "important"), setProperty(i, "visibility", "hidden", "important");
}
detect(), initpicReport();
}
var timer = null, innerHeight = window.innerHeight || document.documentElement.clientHeight, imgMaxW = document.getElementById("page-content").offsetWidth, height = innerHeight + 40, images = [], ping_apurl = !1, has_report_img_time = !1;
window.addEventListener ? (window.addEventListener("scroll", onScroll, !1), window.addEventListener("load", onLoad, !1), document.addEventListener("touchmove", onScroll, !1)) : (window.attachEvent("onscroll", onScroll), window.attachEvent("onload", onLoad));
}(), function() {
function e() {
var e = document.getElementsByTagName("a"), t = !1;
if (!!e) {
var r = e.length;
for (var i = 0; i < r; ++i) {
var s = e[i], o = s.getAttribute("href"), u = /^(http|https):\/\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)(\/)?/g, a = u.exec(o);
a && a[2] && a[2] != "mp.weixin.qq.com" && (t = t || [], t.push(o), n.push(encodeURIComponent(o)));
}
}
return t.length == outsize_link ? !1 : t;
}
function t() {
var e = document.getElementsByTagName("form");
if (e) for (var t = 0; t < e.length; ++t) {
var n = e[t];
if (n) {
var i = n.outerHTML || "";
r.push(encodeURIComponent(n.getAttribute("action") + i.substr(0, 400)));
}
}
return e ? e.length : 0;
}
var n = [], r = [], i = [];
e() && i.push(1), t() && i.push(2), i.length > 0 && (n = "&url=" + n.join("||"), r = "&furl=" + r.join("||"), ajax({
url: "/mp/hijack?type=" + i.join(",") + "&r=" + Math.random() + n + r,
type: "POST",
timeout: 2e3,
data: ""
}));
}();</script>
        <script type="text/javascript">
        var report_time_url = 'http://isdspeed.qq.com/cgi-bin/r.cgi?flag1=7839&flag2=7&flag3=1';
        var report_param = [];
        if (Math.random() < 0.1){
            var page_endtime = (+new Date());
            var page_time = page_endtime - _wxao.begin;
            var is_link = "2" == "1";
            var oss_key = is_link ? 9 : 10;
            report_param.push("&" + oss_key + "=" + page_time);
        }
        if (Math.random() < 0.5 && false !== navigationStart){//如果有performance接口 就把打点数据记录起来 这个采样高一点
            report_param.push("&12=" + (write_sceen_time - navigationStart));
            report_param.push("&13=" + (first_sceen__time - navigationStart));
        }
        if (report_param.length != 0){
            var _img = new Image(1, 1);
            _img.src = report_time_url + report_param.join("");
        }
    </script>
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1252997000'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/stat.php%3Fid%3D1252997000' type='text/javascript'%3E%3C/script%3E"));</script>        
</body>
</html>

