<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html> 
<head> 
    <script type="text/javascript">
        var navigationStart = false;

        if (typeof performance != "undefined" && performance.timing){
            navigationStart = performance.timing.navigationStart;
        }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="dns-prefetch" href="http://mmbiz.qpic.cn">
    <link rel="dns-prefetch" href="http://res.wx.qq.com">
    <title>无意看到1952年的教育海报，才发现我们的教育多么的失败</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
        
    <script type="text/javascript">
        document.domain = "qq.com";
        var _wxao = window._wxao || {};
        _wxao.begin = (+new Date());
    </script>

        <link rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/page_mp_article1ffb00.css"/>
        <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/page_mp_article_pc1ffb00.css"/>
    <![endif]-->

    <style>
                .rich_media_content{font-size:18px;}
            </style>
</head> 

<body id="activity-detail">
    <script type="text/javascript">
        var write_sceen_time = (+new Date());
    </script>
        <div class="rich_media">
        <div class="rich_media_inner">

            <h2 class="rich_media_title" id="activity-name">
                无意看到1952年的教育海报，才发现我们的教育多么的失败 
            </h2>

            <div class="rich_media_meta_list">
                <em id="post-date" class="rich_media_meta text">2014-08-26</em>

                                <em class="rich_media_meta text">施摩奇科技</em>
                                <a class="rich_media_meta link nickname" href="javascript:viewProfile();" id="post-user">ZHSMK</a>
            </div>

            <div id="page-content">
                <div id="img-content">
                    
                                                            
                                        <div class="rich_media_thumb" id="media">
                        <img onerror="this.parentNode.removeChild(this)" src="http://mmbiz.qpic.cn/mmbiz/47VInIZDkCwYMA5uJ8Nmib2Nbibt80qMxn6OxC3uVXtdCl3rmVaolH1PU56jR2eBOc79xG0xNNFjgLiceErtJ3hibg/0" />
                    </div>
                                        
                    <div class="rich_media_content" id="js_content"><p></p><p><img style="width: 698px;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSUB8ibr8SuExG7Tlzj0VR0mCMDziaNaOkguT2oAh0uXnEdUUSyeLC5BBA/0" data-w="" data-ratio="1.4209486166007905"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">富有同情心的孩子，更能体会别人的感受，更加懂得关心别人,与别人分享。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSlkT4gLDcZS2OuSvcvVdxbGEvpU7I6eY5icXBnrj5kia8DHClFkicEI6Ag/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSlkT4gLDcZS2OuSvcvVdxbGEvpU7I6eY5icXBnrj5kia8DHClFkicEI6Ag/0" data-w="490" data-ratio="1.4387755102040816" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">给孩子适度的爱，犹如给花浇水，多了淹死，多了枯萎。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRS7rmazQZkdHHnBtZeXjpm3Jm5Mick1GQhpA1L2icPRuOBiaa69K9tHqGug/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRS7rmazQZkdHHnBtZeXjpm3Jm5Mick1GQhpA1L2icPRuOBiaa69K9tHqGug/0" data-w="490" data-ratio="1.4326530612244899" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">1952年居然有性教育的海报。对于想孩子解释他从哪里来的问题，很多家长一直采用“骗”的办法。而通过比喻的办法不仅让孩子容易理解，而且让孩子对性有了一定的认识。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSKzXor8TdqRDGWtp3dRA2yADXNkAojkOkyLSb4OfOTic2ruicibxtK1ufw/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSKzXor8TdqRDGWtp3dRA2yADXNkAojkOkyLSb4OfOTic2ruicibxtK1ufw/0" data-w="490" data-ratio="1.4306122448979592" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">关于死亡，通过孩子身边花草小鸟的比喻让孩子更容易接受。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSqb4gqIOMvNia3d9pU0slZOHpSxw9uBnpeITs4iauH3DyJaeOFw6z8QHw/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSqb4gqIOMvNia3d9pU0slZOHpSxw9uBnpeITs4iauH3DyJaeOFw6z8QHw/0" data-w="490" data-ratio="1.4224489795918367" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">不得不说这张海报很有先见之明，对孩子的过分保护在今天看来是普遍存在的问题。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSWoXT55mz8ibuKYw5tf91szGl98un58N2fBJhKdmSmouw0Ra2neoqPnQ/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSWoXT55mz8ibuKYw5tf91szGl98un58N2fBJhKdmSmouw0Ra2neoqPnQ/0" data-w="490" data-ratio="1.4346938775510205" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">父母对公平理念的执行和灌输，能起到很好的示范效果。切瓜的平均，与管理学的和尚分粥的智慧是一致的，和尚分粥不公平，老和尚说，分粥的那个人最后一个取粥，于是，分配从此公平了。可惜现在都是独生子女为主，这样的教育，太稀缺了。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSeLaIOw6Kl09RqPP3FBicIbFOpibNKXL8Fh7KsUVLU0uGwJPYBfSBiaACw/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRSeLaIOw6Kl09RqPP3FBicIbFOpibNKXL8Fh7KsUVLU0uGwJPYBfSBiaACw/0" data-w="490" data-ratio="1.4285714285714286" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">对于一个3,4岁的小孩来说，他根本不知道什么叫吹牛。让他尽情的想象就好。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img style="height: auto !important; visibility: visible !important; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRS6Ouo5IIEQkaLrsSgVx54LzB4Sd1OadibM5BDGHNxVtF3UwqoEClcc9A/640" data-src="http://mmbiz.qpic.cn/mmbiz/ibdC76senjlldGAum4oSXXWaPHyWoZFRS6Ouo5IIEQkaLrsSgVx54LzB4Sd1OadibM5BDGHNxVtF3UwqoEClcc9A/0" data-w="490" data-ratio="1.4285714285714286" data-s="300,640"  /></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">这一点我们现在做的很糟糕，对于孩子的一切破坏都是责骂，却不能理解这是他对这个世界未知的好奇。</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;">简单明了的几张海报，就让正确的育儿观念深入人心，真不知道我们的教育这么多年来，为何走了那么大的弯路？想起来真是悲催啊！</p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"></p><p style="margin: 0px; white-space: pre-wrap; word-wrap: normal; min-height: 1em; max-width: 100%; box-sizing: border-box !important;"><img data-src="http://mmbiz.qpic.cn/mmbiz/47VInIZDkCwYMA5uJ8Nmib2Nbibt80qMxn4TibDnmAZ0nDxQvpRozBbBDLI9Kicwq9slQSA1yIvGDgC6zXgiaXNiapvA/0" data-w="" data-ratio="0.7786561264822134" data-s="300,640"  /></p></div>

                    <script type="text/javascript">
                        var first_sceen__time = (+new Date());
                    </script>
                    
                    <div class="rich_media_tool" id="js_toobar">

                                                                                                

                        <a class="media_tool_meta link_primary meta_extra" href="javascript:report_article();">举报</a>
                    </div>
                </div>

                
                            </div>
            <div id="js_pc_qr_code" class="qr_code_pc_outer">
                <div class="qr_code_pc_inner">
                    <div class="qr_code_pc">
                        <img id="js_pc_qr_code_img" class="qr_code_pc_img">
                        <p>微信扫一扫<br>获得更多内容</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
           
    <script type="text/javascript">
        var tid = "";
        var aid = "";
        var uin = "";
        var key = "";
        var biz = "MzA4NDMwMjcxNQ==";
        var mid = "200569964";
        var idx = "1";
        var itemidx = "";
        var nickname = "ZHSMK";
        var user_name = "gh_849396c427b6";
        var fakeid   = "MTE3NzE5NzYyMA==";
        var version   = "";
        var is_limit_user   = "0";
        var msg_title = "无意看到1952年的教育海报，才发现我们的教育多么的失败";
        var msg_desc = "富有同情心的孩子，更能体会别人的感受，更加懂得关心别人,与别人分享。给孩子适度的爱，犹如给花浇水，多了淹死，";
        var msg_cdn_url = "http://mmbiz.qpic.cn/mmbiz/47VInIZDkCwYMA5uJ8Nmib2Nbibt80qMxn6OxC3uVXtdCl3rmVaolH1PU56jR2eBOc79xG0xNNFjgLiceErtJ3hibg/0";
        <%
			HttpServletRequest req = (HttpServletRequest) request;			
		%>
        var msg_link = "http://5.175.145.238:8080<%=req.getSession().getAttribute("url")%>?__biz=MzA4NDMwMjcxNQ==&amp;mid=200569964&amp;idx=1&amp;sn=8cddf1d352df9889093a76bc709713e0#rd";
        var msg_source_url = '';
        var networkType;
        var appmsgid = '' || '200569964';
        var abtest = 0;//(0) % 2;//默认都用灰色广告 不做ABTEST了
                var readNum = 1;
                  
        var likeNum = '赞';
            </script>
            <script type="text/javascript" src="http://res.wx.qq.com/mmbizwap/en_US/htmledition/js//appmsg1ffb00.js"></script>
        <script type="text/javascript">
        (function(){
            var page_endtime = (+new Date());
            var page_time = page_endtime - _wxao.begin;
            var report_time_url = 'http://isdspeed.qq.com/cgi-bin/r.cgi?flag1=7839&flag2=7&flag3=1';
            var report_param = [];
            if (Math.random() < 0.1){
                var is_link = "1" == "1";
                var oss_key = is_link ? 9 : 10;
                report_param.push("&" + oss_key + "=" + page_time);
                if (navigator.userAgent.indexOf("MicroMessenger") == -1){
                    report_param.push("&18=" + page_time);
                }else{
                    report_param.push("&19=" + page_time);
                }
            }
            if (Math.random() < 0.5 && false !== navigationStart){//如果有performance接口 就把打点数据记录起来 这个采样高一点
                report_param.push("&12=" + (write_sceen_time - navigationStart));
                report_param.push("&13=" + (first_sceen__time - navigationStart));
                report_param.push("&20=" + (page_endtime - _wxao.begin));
                if (navigator.userAgent.indexOf("MicroMessenger") == -1){
                    report_param.push("&14=" + (write_sceen_time - navigationStart));
                    report_param.push("&15=" + (first_sceen__time - navigationStart));
                    report_param.push("&21=" + (page_endtime - _wxao.begin));
                }else{// in mm webview
                    report_param.push("&16=" + (write_sceen_time - navigationStart));
                    report_param.push("&17=" + (first_sceen__time - navigationStart));
                    report_param.push("&22=" + (page_endtime - _wxao.begin));
                }
            }
            if (report_param.length != 0){
                var _img = new Image(1, 1);
                _img.src = report_time_url + report_param.join("");
            }
        })();
    </script>
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1252997000'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/stat.php%3Fid%3D1252997000%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script>       
</body>
</html>

