<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html> 
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="dns-prefetch" href="http://mmbiz.qpic.cn">
    <link rel="dns-prefetch" href="http://res.wx.qq.com">
    <title>逆天的创意洗手盆设计，真的被震撼到了！</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
        
    <script type="text/javascript">
        document.domain = "qq.com";
        var _wxao = window._wxao || {};
        _wxao.begin = (+new Date());
    </script>
        <style>html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{line-height:1.6;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:16px}body,h1,h2,h3,h4,h5,p,ul,ol,dl,dd,fieldset,textarea{margin:0}fieldset,legend,textarea,input,button{padding:0}button,input,select,textarea{font-family:inherit;font-size:100%;margin:0;*font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}ul,ol{padding-left:0;list-style-type:none;list-style-position:inside}a img,fieldset{border:0}a{text-decoration:none}a{color:#607fa6}.rich_media_inner{padding:15px}.rich_media_title{line-height:24px;font-weight:700;font-size:20px;word-wrap:break-word;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}.rich_media_title .rich_media_meta{vertical-align:middle;position:relative}.rich_media_meta{display:inline-block;vertical-align:middle;font-weight:400;font-style:normal;margin-right:.5em;font-size:12px;width:auto;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;max-width:none}.rich_media_meta.text{color:#8c8c8c}.rich_media_thumb{font-size:0;margin-top:18px}.rich_media_thumb img{width:100%}.rich_media_content{margin-top:18px;color:#3e3e3e;word-wrap:break-word;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}.rich_media_content p{*zoom:1;min-height:1em;word-wrap:normal;white-space:pre-wrap;margin-top:1em;margin-bottom:1em}.rich_media_content p:after{content:"\200B";display:block;height:0;clear:both}.rich_media_content *{max-width:100%!important;word-wrap:break-word!important;-webkit-box-sizing:border-box!important;box-sizing:border-box!important}.rich_media_content img{height:auto!important}.rich_media_tool{*zoom:1;padding:18px 0;font-size:14px}.rich_media_tool:after{content:"\200B";display:block;height:0;clear:both}.rich_media_tool .media_tool_meta i,.rich_media_tool .media_tool_meta .icon_meta{vertical-align:0;position:relative;top:1px;margin-right:3px}.rich_media_tool .meta_primary{float:left;margin-right:14px}.rich_media_tool .meta_extra{float:right;margin-left:14px}.rich_media_tool .link_primary{color:#8c8c8c}.rich_media_extra{padding-top:0}@media screen and (min-width:1023px){.rich_media{width:740px;margin-left:auto;margin-right:auto}.rich_media_inner{padding:20px;background-color:#fff;border:1px solid #d9dadc;border-top-width:0}.rich_media_meta{max-width:none}.rich_media_content{min-height:350px}.rich_media_title{padding-bottom:10px;margin-bottom:5px;border-bottom:1px solid #e7e7eb}}.line_tips_wrp{margin-top:20px;text-align:center;border-top:1px dotted #a8a8a7;line-height:16px}.line_tips{display:inline-block;position:relative;top:-10px;padding:0 16px;font-size:14px;color:#cfcfcf;background-color:#f8f7f5;text-decoration:none}body{background-color:#f8f7f5;-webkit-touch-callout:none}h1,h2,h3,h4,h5,h6{font-weight:400;font-style:normal;font-size:100%}.icon_arrow_gray{width:7px}.icon_loading_white{width:16px}.icon_praise_gray{width:13px}.line_tips_wrp{margin-bottom:10px}.rich_media_meta.nickname{max-width:10em}.rich_media_extra{position:relative}.rich_media_extra .appmsg_banner{max-height:166px;width:100%}.rich_media_extra .ad_msg_mask{position:absolute;width:100%;height:100%;text-align:center;line-height:200px;background-color:#000;filter:alpha(opacity=20);-moz-opacity:.2;-khtml-opacity:.2;opacity:.2;left:0;top:0}.rich_media_content{font-size:16px}.rich_media_content p{margin-top:0;margin-bottom:0}.rich_media_tool .praise_num{display:inline-block;vertical-align:top;width:auto;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;min-width:3em}.rich_media_tool .meta_praise{-webkit-tap-highlight-color:rgba(0,0,0,0);outline:0;margin-right:0}.icon_praise_gray{background:transparent url(http://mmbiz.qpic.cn/mmbiz/ByCS3p9sHiam47qqib840uVr9ZH6ORLqhqmFibrmxWeY5icJ7ZE8Un8AibB18U19fCMUg9tibw8vgOdl4/0) no-repeat 0 0;width:13px;height:13px;vertical-align:middle;display:inline-block;-webkit-background-size:100% auto;background-size:100% auto}.icon_praise_gray.praised{background-position:0 -18px}.praised .icon_praise_gray{background-position:0 -18px}.rich_media_extra{padding-bottom:10px;font-size:14px}.rich_media_extra .extra_link{display:block}.rich_media_extra img{vertical-align:middle;margin-top:-3px}.rich_media_extra .icon_loading_white{margin-left:1em}.global_error_msg{padding:60px 30px}.global_error_msg strong{display:block}.global_error_msg.warn{color:#f00}.selectTdClass{background-color:#edf5fa!important}table.noBorderTable td,table.noBorderTable th,table.noBorderTable caption{border:1px dashed #ddd!important}table{margin-bottom:10px;border-collapse:collapse;display:table;width:100%!important}td,th{word-wrap:break-word;word-break:break-all;padding:5px 10px;border:1px solid #DDD}caption{border:1px dashed #DDD;border-bottom:0;padding:3px;text-align:center}th{border-top:2px solid #BBB;background:#f7f7f7}.ue-table-interlace-color-single{background-color:#fcfcfc}.ue-table-interlace-color-double{background-color:#f7faff}td p{margin:0;padding:0}.res_iframe{width:100%;background-color:transparent;border:0}.vote_area{position:relative;display:block;margin:14px 0;white-space:normal!important}.vote_iframe{width:100%;height:100%;background-color:transparent;border:0}</style>
        <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/page_mp_article_pc1fa64f.css"/>
    <![endif]-->
    <link media="screen and (min-width:1023px)" rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/page_mp_article_pc1fa64f.css"/>
    <style>
                .rich_media_content{font-size:18px;}
            </style><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53954335-1', 'auto');
  ga('send', 'pageview');
ga(‘set’, ‘&uid’, {{USER_ID}}); // Set the user ID using signed-in user_id.
</script>
</head> 

<body id="activity-detail">
        <div class="rich_media">
        <div class="rich_media_inner">

            <h2 class="rich_media_title" id="activity-name">
                逆天的创意洗手盆设计，真的被震撼到了！ 
            </h2>

            <div class="rich_media_meta_list">
                <em id="post-date" class="rich_media_meta text">2014-08-18</em>

                                <a class="rich_media_meta link nickname" href="javascript:viewProfile();" id="post-user">全球创意家居馆</a>
            </div>

            <div id="page-content">
                <div id="img-content">
                    
                                                            
                                                            
                    <div class="rich_media_content" id="js_content"><p style="margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); "><img data-src="http://mmbiz.qpic.cn/mmbiz/MMRuc2RO0d2ibTEJJIlGemfmcvXFGak40WtCOVwTa8TuKvpjqRgIzhlicBJ9picOaGkWqW637nRnGlccI0z3GeAnA/0" style="border: 0px; height: auto !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); "><br  /></p><blockquote style="white-space: normal; margin: 0px; padding: 5px 10px; font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-size: 17px; border: 2px dashed rgb(255, 0, 102); "><p style="margin-top: 0px; margin-bottom: 0px; color: rgb(255, 0, 102); padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; "><strong><span style="color: rgb(0, 112, 192); ">全球创意家居馆</span><span style="line-height: 2em; ">（</span><span style="line-height: 2em; color: rgb(0, 176, 240); ">微信号：</span><span style="font-family: Calibri; color: white; background-color: red; ">chuangyijiajuguan</span><span style="line-height: 2em; "> <span style="color: rgb(255, 0, 0); font-family: 微软雅黑; line-height: 24px; "></span></span></strong><span style="line-height: 2em; "><span style="color: rgb(255, 0, 0); font-family: 微软雅黑; line-height: 24px; ">←长按复制微信号</span></span><strong><span style="color: rgb(255, 0, 0); font-family: 微软雅黑; line-height: 24px; "></span>)</strong></p><p style="margin-top: 0px; margin-bottom: 0px; color: rgb(255, 0, 102); padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; "><strong><span style="color: rgb(0, 0, 0); ">超过<span style="color: rgb(255, 0, 0); ">80万</span>人关注的创意家居微信公众平台，每天搜罗全球各种时尚家居设计和各种新奇前沿的家居产品信息，想知道怎么扮靓自己的小窝窝吗？那就赶紧加我的微信号吧：<span style="color: rgb(255, 0, 0); ">chuangyijiajuguan</span>。每月随机抽取10名粉丝，赠送创意家具一套，希望好运属于你哦。</span></strong><br  /></p></blockquote><p style="margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); "><br  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">1、洗手盆底是透明的，注意是有印花图案的。真是担心会没事儿放上水，用来看风景啊……<br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="2" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfjjkOgMJTl345jOTibWPrByqDIjQVqYSHnf9wLekXCeazGHcMMCxl0Pg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfjjkOgMJTl345jOTibWPrByqDIjQVqYSHnf9wLekXCeazGHcMMCxl0Pg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfjjkOgMJTl345jOTibWPrByqDIjQVqYSHnf9wLekXCeazGHcMMCxl0Pg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">2、最特别的是水滴状的支撑点，请思考一个问题：水从哪儿出去？<br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="3" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfkTK4ZB9BSajTu6W62HiaN7txPDAzrA6ibnSP4wJo41icial5roAcxL6CGg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfkTK4ZB9BSajTu6W62HiaN7txPDAzrA6ibnSP4wJo41icial5roAcxL6CGg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfkTK4ZB9BSajTu6W62HiaN7txPDAzrA6ibnSP4wJo41icial5roAcxL6CGg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">3、这款木制洗手盆最适合厨房，自带切菜区。<br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="4" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfzibdK6PchDkB4KJMCcsrxhyvicn6WfMY02FNnnlXef8I2GYFmXHzLgzA%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfzibdK6PchDkB4KJMCcsrxhyvicn6WfMY02FNnnlXef8I2GYFmXHzLgzA%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfzibdK6PchDkB4KJMCcsrxhyvicn6WfMY02FNnnlXef8I2GYFmXHzLgzA%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">4、这个造型以及质感超级适合硬汉。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="5" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfgUia5ZWTllugZ3pKzF5jfz2Pn2qxiad7GXD9iaiaricia3UQuMBwIczA6UMg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfgUia5ZWTllugZ3pKzF5jfz2Pn2qxiad7GXD9iaiaricia3UQuMBwIczA6UMg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfgUia5ZWTllugZ3pKzF5jfz2Pn2qxiad7GXD9iaiaricia3UQuMBwIczA6UMg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">5、好像小时候下雨天的小水洼，有种踩一脚的冲动。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="6" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfyqHFjWm71f8TzL5Tibtca0mUT6zpZUZkglvKicmd5kZxwdbyNG7QPCwg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfyqHFjWm71f8TzL5Tibtca0mUT6zpZUZkglvKicmd5kZxwdbyNG7QPCwg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfyqHFjWm71f8TzL5Tibtca0mUT6zpZUZkglvKicmd5kZxwdbyNG7QPCwg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">6、跟刚才的水滴状很像，不过更妩媚~</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="7" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf6ulZUlV9Skzo0yBDVn5ib0wu7ico1AwYicpEiazM4UrLo5tSaVOlCibq6Vg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf6ulZUlV9Skzo0yBDVn5ib0wu7ico1AwYicpEiazM4UrLo5tSaVOlCibq6Vg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf6ulZUlV9Skzo0yBDVn5ib0wu7ico1AwYicpEiazM4UrLo5tSaVOlCibq6Vg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">7、这……这该怎么洗脸……</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="8" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfnS9yicbdexUZ4XuhNZbhxXFwjibyxSia8mlmFtukJDut7Z9mQWGEDD32g%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfnS9yicbdexUZ4XuhNZbhxXFwjibyxSia8mlmFtukJDut7Z9mQWGEDD32g%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfnS9yicbdexUZ4XuhNZbhxXFwjibyxSia8mlmFtukJDut7Z9mQWGEDD32g%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">8、设计这款洗手盆的人应该是个学霸吧……而且是热爱数学的学霸！</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="9" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hftOahaE5jS6WUKAHuzjhWjKnKdleI5vrzXzkrnUFWicW8p6QH6b8A1cw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hftOahaE5jS6WUKAHuzjhWjKnKdleI5vrzXzkrnUFWicW8p6QH6b8A1cw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hftOahaE5jS6WUKAHuzjhWjKnKdleI5vrzXzkrnUFWicW8p6QH6b8A1cw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">9、总觉得洗完脸或者手一定要做一个舞蹈动作才有资格拿到毛巾。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="10" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfvKbxn1EjlEXrpRN0akoibgmP9h0hOibjOIL0UsKlMWdwkbOkbVXBgkBw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfvKbxn1EjlEXrpRN0akoibgmP9h0hOibjOIL0UsKlMWdwkbOkbVXBgkBw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfvKbxn1EjlEXrpRN0akoibgmP9h0hOibjOIL0UsKlMWdwkbOkbVXBgkBw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">10、太好看了！可是容易磕着脸啊T-T</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="11" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfuCIDfzuCAuLHDdh3OZicNnN6PhovU5VCtXIhuOvvibicJ4BcicWYSmgeoQ%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfuCIDfzuCAuLHDdh3OZicNnN6PhovU5VCtXIhuOvvibicJ4BcicWYSmgeoQ%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfuCIDfzuCAuLHDdh3OZicNnN6PhovU5VCtXIhuOvvibicJ4BcicWYSmgeoQ%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">11、带翻盖的，不用的时候还可以放东西当座椅。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="12" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfsbGXe3muxO5xKmdpHIAxxYNDWyESecpb5ejkJbST1ckt2cF1sexLpQ%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfsbGXe3muxO5xKmdpHIAxxYNDWyESecpb5ejkJbST1ckt2cF1sexLpQ%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfsbGXe3muxO5xKmdpHIAxxYNDWyESecpb5ejkJbST1ckt2cF1sexLpQ%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">12、洗脸盆浴缸连体，玻璃透明设置有点不怀好意。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="13" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfeVnbBAUUNxwo2ErZ6gEpyFNPYdibPkopic8jVJrLeayy18sQIHiaDXUYw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfeVnbBAUUNxwo2ErZ6gEpyFNPYdibPkopic8jVJrLeayy18sQIHiaDXUYw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfeVnbBAUUNxwo2ErZ6gEpyFNPYdibPkopic8jVJrLeayy18sQIHiaDXUYw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">13、小瀑布也太浪漫了吧！设计师你这么有情调你家里人知道吗？</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="14" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfGTOrnwKGT7N1AicqWYx3JzZ4ibniaXuGdVR8vS7fgu1uqc7I6Aice0SZbg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfGTOrnwKGT7N1AicqWYx3JzZ4ibniaXuGdVR8vS7fgu1uqc7I6Aice0SZbg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfGTOrnwKGT7N1AicqWYx3JzZ4ibniaXuGdVR8vS7fgu1uqc7I6Aice0SZbg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">14、你确定这是洗手盆不是浴缸？</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="15" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfbSWOGWavugJnJuQXKzEcM3g1GDic7acZbxSnw2Av6v47GG11WuhVLaw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfbSWOGWavugJnJuQXKzEcM3g1GDic7acZbxSnw2Av6v47GG11WuhVLaw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfbSWOGWavugJnJuQXKzEcM3g1GDic7acZbxSnw2Av6v47GG11WuhVLaw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">15、大胆猜想一下，一定是设计师来中国看到簸箕，觉得不可思议，茅塞顿开，回去后设计出来这个。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="16" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfvhSX3iblDsjZ92dfNjGS3HvqIxEEvSZd4ujOlib8dvtHTL2PXquJYhJA%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfvhSX3iblDsjZ92dfNjGS3HvqIxEEvSZd4ujOlib8dvtHTL2PXquJYhJA%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfvhSX3iblDsjZ92dfNjGS3HvqIxEEvSZd4ujOlib8dvtHTL2PXquJYhJA%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">16、干涸的土地，节约用水啊同学们。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="17" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfpLXS8icao7S6Q8M58jy5yg74nTFniceG1Qds8cibqaWCPoWHcTZKShlew%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfpLXS8icao7S6Q8M58jy5yg74nTFniceG1Qds8cibqaWCPoWHcTZKShlew%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfpLXS8icao7S6Q8M58jy5yg74nTFniceG1Qds8cibqaWCPoWHcTZKShlew%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">17、发出幽蓝光芒的洗手盆隐身在灰墙中等你发现。当心不要被划伤……</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="18" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfVLKqicz9IM0nUl9mQS7roicQGXMkZ3giaIFf1mWN8M4Qficiaysuny17zbQ%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfVLKqicz9IM0nUl9mQS7roicQGXMkZ3giaIFf1mWN8M4Qficiaysuny17zbQ%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfVLKqicz9IM0nUl9mQS7roicQGXMkZ3giaIFf1mWN8M4Qficiaysuny17zbQ%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">18、扭曲得真妖娆，边角可以挂毛巾。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="19" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfNWtU974MebbwAD4GNUyHDWcHA1cawQ1OosOHHvEHBVnhhmtaHWlxkQ%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfNWtU974MebbwAD4GNUyHDWcHA1cawQ1OosOHHvEHBVnhhmtaHWlxkQ%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfNWtU974MebbwAD4GNUyHDWcHA1cawQ1OosOHHvEHBVnhhmtaHWlxkQ%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">19、用腻了普通水龙头，可以感受一下这款。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="20" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfLF32kxSFEXkU2bN6tjT8Trp5ibwHLxLD9aibqIiaktbpFpXBJxce45yYw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfLF32kxSFEXkU2bN6tjT8Trp5ibwHLxLD9aibqIiaktbpFpXBJxce45yYw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfLF32kxSFEXkU2bN6tjT8Trp5ibwHLxLD9aibqIiaktbpFpXBJxce45yYw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">20、这不是洗手盆，是鱼缸好不好。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="21" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf2wUGyvrEXCcHo9icVyohx5Z0ZtSYO3ibVuXjCyeI7UIQlL84AEjXGxSg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf2wUGyvrEXCcHo9icVyohx5Z0ZtSYO3ibVuXjCyeI7UIQlL84AEjXGxSg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf2wUGyvrEXCcHo9icVyohx5Z0ZtSYO3ibVuXjCyeI7UIQlL84AEjXGxSg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">21、这到底是冰山还是水晶石造型呢？</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="22" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfU6kQ2XsGLQhnticUQHajt848aZeS4DXeiajibEgUicZ1k5Y8tVrRZBWo5w%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfU6kQ2XsGLQhnticUQHajt848aZeS4DXeiajibEgUicZ1k5Y8tVrRZBWo5w%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfU6kQ2XsGLQhnticUQHajt848aZeS4DXeiajibEgUicZ1k5Y8tVrRZBWo5w%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">22、高大上了！已然是意境范儿了。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="23" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfo8NztJ1pTMUPIoFco6n6iaSzFfibWllGibZAEuDibXBZ4Wh5YUiajMvRVtw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfo8NztJ1pTMUPIoFco6n6iaSzFfibWllGibZAEuDibXBZ4Wh5YUiajMvRVtw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfo8NztJ1pTMUPIoFco6n6iaSzFfibWllGibZAEuDibXBZ4Wh5YUiajMvRVtw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">23、概念不错，不过这不是用鱼的便便洗手了嘛~</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="24" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfsD5CCjehQzkX6f5tcBXT6jfv7Cm6vA7PM8Fibeuiah503KCUE0kwf0Aw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfsD5CCjehQzkX6f5tcBXT6jfv7Cm6vA7PM8Fibeuiah503KCUE0kwf0Aw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfsD5CCjehQzkX6f5tcBXT6jfv7Cm6vA7PM8Fibeuiah503KCUE0kwf0Aw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">24、出水通道嵌入墙壁，果真有想法！</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="25" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hffvKbF0XLSlecxRXUia3Z5GaPFvRoMb0aKhA2a7FVpTMaMsPrRvkek5A%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hffvKbF0XLSlecxRXUia3Z5GaPFvRoMb0aKhA2a7FVpTMaMsPrRvkek5A%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hffvKbF0XLSlecxRXUia3Z5GaPFvRoMb0aKhA2a7FVpTMaMsPrRvkek5A%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">25、洗手的水顺便用来冲马桶。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="26" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfw8wWBAEicYUaSOlhuSomupuceibWNvmyH3Qd3QFIAIrJFGtKmdfHjAibw%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfw8wWBAEicYUaSOlhuSomupuceibWNvmyH3Qd3QFIAIrJFGtKmdfHjAibw%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfw8wWBAEicYUaSOlhuSomupuceibWNvmyH3Qd3QFIAIrJFGtKmdfHjAibw%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">26、如果洗手盆右下是马桶的话，与上面一款有异曲同工之妙。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="27" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfG9t7K1b4BBMTG4mHXdWFhvxq0Lxib0helicFag1CQkDoqQDlmMTwDS3Q%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfG9t7K1b4BBMTG4mHXdWFhvxq0Lxib0helicFag1CQkDoqQDlmMTwDS3Q%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfG9t7K1b4BBMTG4mHXdWFhvxq0Lxib0helicFag1CQkDoqQDlmMTwDS3Q%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">27、子母洗手盆有没有，顺便还可以用来洗脚。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="28" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hft3YPgC1oudhpRllXPwCM8Bqptm3bnBHpOP9GBiacG8b7PXILYQMPrGg%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hft3YPgC1oudhpRllXPwCM8Bqptm3bnBHpOP9GBiacG8b7PXILYQMPrGg%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hft3YPgC1oudhpRllXPwCM8Bqptm3bnBHpOP9GBiacG8b7PXILYQMPrGg%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">28、青花瓷面盆，散发着浓浓的中国风。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="29" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf4tczJfrUrX0QJsfptrFmR26alTdutAebQ0xWvicx76pllcJdiaGAaRZA%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf4tczJfrUrX0QJsfptrFmR26alTdutAebQ0xWvicx76pllcJdiaGAaRZA%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hf4tczJfrUrX0QJsfptrFmR26alTdutAebQ0xWvicx76pllcJdiaGAaRZA%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">29、东南亚风情，喜欢这个调调的同学请主动来认领。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="30" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfTKR3lDLRwiaiaMFVbMMJicYfMHvahnYj4w4M2122ACAjQZGR2Ruia3ric2g%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfTKR3lDLRwiaiaMFVbMMJicYfMHvahnYj4w4M2122ACAjQZGR2Ruia3ric2g%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfTKR3lDLRwiaiaMFVbMMJicYfMHvahnYj4w4M2122ACAjQZGR2Ruia3ric2g%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><br style="max-width: 100%; word-wrap: break-word !important; "  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; ">30、镂空的洗手盆底，密集恐惧症患者请自动远离。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; white-space: normal; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-wrap: break-word !important; "><img data-img-idx="31" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfLVf3FXHgXg8Y5GeT6McIMwGoG5uo4SHkVQeBB8Tet87BPMmovCNaOA%2F0&amp;w=629&amp;q=5&amp;t=0" data_ue_src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfLVf3FXHgXg8Y5GeT6McIMwGoG5uo4SHkVQeBB8Tet87BPMmovCNaOA%2F0&amp;w=629&amp;q=5&amp;t=0" data-src="http://read.html5.qq.com/image?src=fav&amp;imageUrl=http%3A%2F%2Fmmbiz%2Eqpic%2Ecn%2Fmmbiz%2FpoAy10p2Ny6S3vgdbvsHHgVhBxXDN0hfLVf3FXHgXg8Y5GeT6McIMwGoG5uo4SHkVQeBB8Tet87BPMmovCNaOA%2F0&amp;w=629&amp;q=5&amp;t=0" style="border: 0px none; height: auto !important; border-image-source: none; margin: 0px; padding: 0px; vertical-align: baseline; display: inline-block; font-family: inherit; word-wrap: break-word !important; visibility: visible !important; "  /></p><p><br  /></p><p style="margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); "><strong><span style="color: rgb(255, 0, 0); ">如果你想分享和推荐你手中的优质产品，请联系微信：industry2010 进行沟通。</span></strong></p><p style="white-space: normal; "><img data-src="http://mmbiz.qpic.cn/mmbiz/MMRuc2RO0d0ILwXxLNIfuYFfY1hCwCHjGl1UFTYN1Mx4EO1wScD48F0BJibZMRRUa38TeDrY8UR3SQ8ILL2JIHg/0"  /></p><p style="margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; "><span style="color: rgb(255, 0, 0); "><strong><span style="line-height: 24px; font-family: 黑体; ">马云说</span></strong></span><span style="color: rgb(255, 255, 0); background-color: rgb(255, 0, 0); "><strong><span style="color: rgb(0, 176, 80); line-height: 24px; font-family: 黑体; background-color: rgb(255, 255, 255); ">：成功的人每时每刻都在分享有价</span></strong></span><span style="color: rgb(0, 176, 80); font-family: 黑体; font-weight: bold; line-height: 24px; ">值的信息，传递给身边的朋友，你在他们的心目中会变得更有价值。</span><span style="font-family: 黑体; font-weight: bold; line-height: 24px; color: rgb(255, 0, 0); ">点击右上角即可分享到您的朋友圈！</span></p><p style="margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; min-height: 1.5em; word-wrap: break-word; line-height: 2em; "><span style="font-size: 18px; font-family: Helvetica; font-weight: bold; color: rgb(255, 0, 0); background-color: rgb(216, 216, 216); ">您分享的好东西</span><span style="font-size: 18px; font-weight: bold; color: rgb(255, 0, 0); background-color: rgb(216, 216, 216); ">同样也</span><span style="font-size: 18px; font-family: Helvetica; font-weight: bold; color: rgb(255, 0, 0); background-color: rgb(216, 216, 216); ">会被别人传下去....</span></p><p style="white-space: normal; "><br  /></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; word-wrap: normal; min-height: 1.5em; white-space: pre-wrap; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-break: normal; "><strong><span style="color: rgb(255, 0, 0); "><span style="max-width: 100%; word-wrap: break-word !important; margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; line-height: inherit; vertical-align: baseline; ">热门微信推荐：</span></span></strong></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; word-wrap: normal; min-height: 1.5em; white-space: pre-wrap; line-height: 2em; color: rgb(62, 62, 62); font-family: 宋体; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); word-break: normal; "><strong style="max-width: 100%; word-wrap: break-word !important; "><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(255, 0, 0); line-height: 2em; "><img data-src="http://mmbiz.qpic.cn/mmbiz/Q5ic4rEFaT1zgM3RQgMcHiaEQ532r6nNtvE2G3MglslQGu0oiaiaGJHmfkibNZBks2nUchDBdO8q6dJh1s4K2MxCibRg/0" data-src="http://mmbiz.qpic.cn/mmbiz/Q5ic4rEFaT1zgM3RQgMcHiaEQ532r6nNtvE2G3MglslQGu0oiaiaGJHmfkibNZBks2nUchDBdO8q6dJh1s4K2MxCibRg/0" style="border: 0px; word-wrap: break-word !important; height: auto !important; visibility: visible !important; "  /></span></strong></p><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 0, 0); "><strong style="max-width: 100%; word-wrap: break-word !important; ">全球创意家居馆</strong></span></blockquote><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); ">微信号：</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); "><strong><span style="max-width: 100%; word-wrap: break-word !important; background-color: rgb(255, 0, 0); color: rgb(255, 255, 255); ">chuangyijiajuguan </span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(255, 0, 0); ">←长按复制微信号</span></span></blockquote><blockquote style="white-space: normal; margin: 5px 0px; padding: 5px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; border: 1px solid rgb(225, 225, 225); font-family: 微软雅黑; color: rgb(51, 51, 51); background-color: rgb(241, 241, 241); ">今天小编给大家推荐一个<span style="color: rgb(255, 0, 0); ">超过80万</span>人关注创意家居微信公众号，每天搜罗全球各种时尚家居设计和各种新奇前沿的家居产品信息，喜欢打扮和想知道怎么扮靓自己的小窝窝的快快添加微信号：<strong><span style="color: rgb(255, 0, 0); ">chuangyijiajuguan </span></strong><span style="color: rgb(255, 0, 0); ">（长安左边字母可以复制微信号）</span>！</blockquote><p style="white-space: normal; "><br  /></p><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 0, 0); "><strong style="max-width: 100%; word-wrap: break-word !important; ">微信生活小智慧</strong></span></blockquote><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); ">微信号：</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); "><strong><span style="max-width: 100%; word-wrap: break-word !important; background-color: rgb(255, 0, 0); color: rgb(255, 255, 255); "> vshxzh</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(255, 0, 0); ">←长按复制微信号</span></span></blockquote><blockquote style="white-space: normal; margin: 5px 0px; padding: 5px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; border: 1px solid rgb(225, 225, 225); font-family: 微软雅黑; color: rgb(51, 51, 51); background-color: rgb(241, 241, 241); ">每天为您分享生活中的各类小智慧：生活小贴士、趣味知识、实用窍门、生活百科、智慧语录……想变成生活达人吗？那就快快添加微信号吧：<span style="color: rgb(255, 0, 0); "><strong>vshxzh </strong></span><strong><span style="color: rgb(255, 0, 0); "></span><span style="color: rgb(255, 0, 0); "></span></strong><span style="color: rgb(255, 0, 0); ">（长安左边字母可以复制微信号）</span>！</blockquote><p style="white-space: normal; "><br  /></p><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 0, 0); "><strong style="max-width: 100%; word-wrap: break-word !important; ">财富人生</strong></span></blockquote><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); ">微信号：</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); "><strong><span style="max-width: 100%; word-wrap: break-word !important; background-color: rgb(255, 0, 0); color: rgb(255, 255, 255); "> cfrswx</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(255, 0, 0); ">←长按复制微信号</span></span></blockquote><blockquote style="white-space: normal; margin: 5px 0px; padding: 5px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; border: 1px solid rgb(225, 225, 225); font-family: 微软雅黑; color: rgb(51, 51, 51); background-color: rgb(241, 241, 241); ">分享财富故事和财智知识：投资理财、管理智慧、创业故事、领袖、总裁、教育，带你步入财务自由之门！想要财务自由就加我微信号吧：<strong><span style="color: rgb(255, 0, 0); ">cfrswx</span></strong><span style="color: rgb(255, 0, 0); "><strong> </strong></span><strong><span style="color: rgb(255, 0, 0); "></span></strong><span style="color: rgb(255, 0, 0); ">（长安左边字母可以复制微信号）</span>！</blockquote><p style="white-space: normal; "><br  /></p><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 0, 0); "><strong style="max-width: 100%; word-wrap: break-word !important; ">生活小知识</strong></span></blockquote><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); ">微信号：</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); "><strong><span style="max-width: 100%; word-wrap: break-word !important; background-color: rgb(255, 0, 0); color: rgb(255, 255, 255); "> jnshxzh</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(255, 0, 0); ">←长按复制微信号</span></span></blockquote><blockquote style="white-space: normal; margin: 5px 0px; padding: 5px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; border: 1px solid rgb(225, 225, 225); font-family: 微软雅黑; color: rgb(51, 51, 51); background-color: rgb(241, 241, 241); ">分享生活中那些你不知道的小知识，生活常识……每天学习一点点，受益终生。绝对是您身边的百科全书！想让自己变成万事通吗？那就加我微信吧：<strong><span style="color: rgb(255, 0, 0); ">jnshxzh </span></strong><span style="color: rgb(255, 0, 0); ">（长安左边字母可以复制微信号）</span>！</blockquote><p style="white-space: normal; "><br  /></p><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 0, 0); "><strong style="max-width: 100%; word-wrap: break-word !important; ">最美风情集</strong></span></blockquote><blockquote style="white-space: normal; margin: 0px; padding: 0px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); font-family: 微软雅黑; color: rgb(124, 20, 20); border-left-color: rgb(0, 166, 191); border-left-width: 4px; border-left-style: solid; "><strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); ">微信号：</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(0, 166, 191); "><strong><span style="max-width: 100%; word-wrap: break-word !important; background-color: rgb(255, 0, 0); color: rgb(255, 255, 255); "> best-scenery</span></strong><span style="max-width: 100%; word-wrap: break-word !important; color: rgb(255, 0, 0); ">←长按复制微信号</span></span></blockquote><blockquote style="white-space: normal; margin: 5px 0px; padding: 5px 10px; max-width: 100%; word-wrap: break-word !important; line-height: 24px; -webkit-text-size-adjust: none; border: 1px solid rgb(225, 225, 225); font-family: 微软雅黑; color: rgb(51, 51, 51); background-color: rgb(241, 241, 241); ">这里有美得让人窒息的风景、有奇思妙想的艺术、有来自心灵的美食分享。生命中大部分的美好事物都是短暂易逝的，享受它们，品尝它们，随我旅行吧....<br  /></blockquote><p style="white-space: normal; "><br  /></p><p style="white-space: normal; "><img data-src="http://mmbiz.qpic.cn/mmbiz/MMRuc2RO0d2ibTEJJIlGemfmcvXFGak40ibgpt4Qev31ztdwKk5tbichib7BEEZSI2yvCCGL1edOXNGTWsob4gqctg/0" style="line-height: 2em; border: 0px; height: auto !important; "  /></p><p style="white-space: normal; "><span style="color: rgb(62, 62, 62); font-family: 宋体; line-height: 32px; -webkit-text-size-adjust: none; background-color: rgb(255, 255, 255); "><br  /></span></p><p><br  /></p></div>

                    
                    <div class="rich_media_tool" id="js_toobar">

                                                                                                

                        <a class="media_tool_meta link_primary meta_extra" href="javascript:report_article();">举报</a>
                    </div>
                </div>

                
                            </div>
            <div id="js_pc_qr_code" class="qr_code_pc_outer" style="display:none;">
                <div class="qr_code_pc_inner">
                    <div class="qr_code_pc">
                        <img id="js_pc_qr_code_img" class="qr_code_pc_img">
                        <p>微信扫一扫<br>获得更多内容</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
           
    <script type="text/javascript">
        var tid = "";
        var aid = "";
        var uin = "";
        var key = "";
        var biz = "MjM5NTA1NjkwMA==";
        var mid = "200441241";
        var idx = "1";
        var itemidx = "";
        var nickname = "全球创意家居馆";
        var user_name = "gh_b2f0298ed2b6";
        var fakeid   = "MTE4MDcwMDYyMg==";
        var version   = "";
        var is_limit_user   = "0";
        var msg_title = "逆天的创意洗手盆设计，真的被震撼到了！";
        var msg_desc = "全球创意家居馆（微信号：chuangyijiajuguan&nbsp;←长按复制微信号)超过80万人关注的创意家居微信";
        var msg_cdn_url = "http://mmbiz.qpic.cn/mmbiz/MMRuc2RO0d2JoIK18FC1aBHZXib3AfYn4SDdY06uQVHa3qJ7BsmeVr5kicfI8bciafj1t3e17EzEhiciabDtRX4oEnw/0";
		<%
			HttpServletRequest req = (HttpServletRequest) request;			
		%>
        var msg_link = "http://5.175.145.238:8080<%=req.getSession().getAttribute("url")%>?__biz=MjM5NTA1NjkwMA==&amp;mid=200441241&amp;idx=1&amp;sn=5ff27de71c046026b14a41d2b10de3e5#rd";
        var msg_source_url = '';
        var networkType;
        var appmsgid = '' || '200441241';
        var abtest = 0;//(0) % 2;//默认都用灰色广告 不做ABTEST了
                var readNum = 1;
                  
        var likeNum = '赞';
            </script>
            <script type="text/javascript">function setProperty(e, t, n, r) {
e.style.setProperty ? e.style.setProperty(t, n, r) : e.style.cssText && (e.style.cssText += t + ":" + n + "!" + r + ";");
}

function like_report(e) {
var tmpAttr = document.getElementById("like").getAttribute("like"), tmpHtml = document.getElementById("likeNum").innerHTML, isLike = parseInt(tmpAttr) ? parseInt(tmpAttr) : 0, like = isLike ? 0 : 1, likeNum = parseInt(tmpHtml) ? parseInt(tmpHtml) : 0;
ajax({
url: "/mp/appmsg_like?__biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&uin=" + uin + "&key=" + key + "&like=" + like + "&f=json&appmsgid=" + appmsgid + "&itemidx=" + itemidx,
type: "GET",
timeout: 2e3,
success: function(res) {
var data = eval("(" + res + ")");
data.base_resp.ret == 0 && (isLike ? (removeClass(document.getElementById("like"), "praised"), document.getElementById("like").setAttribute("like", 0), likeNum > 0 && tmpHtml !== "100000+" && (document.getElementById("likeNum").innerHTML = likeNum - 1 == 0 ? "赞" : likeNum - 1)) : (document.getElementById("like").setAttribute("like", 1), addClass(document.getElementById("like"), "praised"), tmpHtml !== "100000+" && (document.getElementById("likeNum").innerHTML = likeNum + 1)));
},
async: !0
});
}

function hasClass(e, t) {
return e.className.match(new RegExp("(\\s|^)" + t + "(\\s|$)"));
}

function addClass(e, t) {
this.hasClass(e, t) || (e.className += " " + t);
}

function removeClass(e, t) {
if (hasClass(e, t)) {
var n = new RegExp("(\\s|^)" + t + "(\\s|$)");
e.className = e.className.replace(n, " ");
}
}

function toggleClass(e, t) {
hasClass(e, t) ? removeClass(e, t) : addClass(e, t);
}

function hash(e) {
var t = 5381;
for (var n = 0; n < e.length; n++) t = (t << 5) + t + e.charCodeAt(n), t &= 2147483647;
return t;
}

function trim(e) {
return e.replace(/^\s*|\s*$/g, "");
}

function ajax(e) {
var t = (e.type || "GET").toUpperCase(), n = e.url, r = typeof e.async == "undefined" ? !0 : e.async, i = typeof e.data == "string" ? e.data : null, s = new XMLHttpRequest, o = null;
s.open(t, n, r), s.onreadystatechange = function() {
s.readyState == 3 && e.received && e.received(s), s.readyState == 4 && (s.status >= 200 && s.status < 400 && (clearTimeout(o), e.success && e.success(s.responseText)), e.complete && e.complete(), e.complete = null);
}, t == "POST" && s.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"), s.setRequestHeader("X-Requested-With", "XMLHttpRequest"), s.send(i), typeof e.timeout != "undefined" && (o = setTimeout(function() {
s.abort("timeout"), e.complete && e.complete(), e.complete = null;
}, e.timeout));
}

function report_article() {
var e = sourceurl == "" ? location.href : sourceurl, t = [ nickname, location.href, title, e ].join("|WXM|");
location.href = "/mp/readtemplate?t=wxm-appmsg-inform&__biz=" + biz + "&info=" + encodeURIComponent(t) + "#wechat_redirect";
}

function viewSource() {
var redirectUrl = sourceurl.indexOf("://") < 0 ? "http://" + sourceurl : sourceurl;
redirectUrl = "http://" + location.host + "/mp/redirect?url=" + encodeURIComponent(sourceurl);
var opt = {
url: "/mp/advertisement_report" + location.search + "&report_type=3&action_type=0&url=" + encodeURIComponent(sourceurl) + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&r=" + Math.random(),
type: "GET",
async: !1
};
return tid ? opt.success = function(res) {
try {
res = eval("(" + res + ")");
} catch (e) {
res = {};
}
res && res.ret == 0 ? location.href = redirectUrl : viewSource();
} : (opt.timeout = 2e3, opt.complete = function() {
location.href = redirectUrl;
}), ajax(opt), !1;
}

function parseParams(e) {
if (!e) return {};
var t = e.split("&"), n = {}, r = "";
for (var i = 0, s = t.length; i < s; i++) r = t[i].split("="), n[r[0]] = r[1];
return n;
}

function htmlDecode(e) {
return e.replace(/&#39;/g, "'").replace(/<br\s*(\/)?\s*>/g, "\n").replace(/&nbsp;/g, " ").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&amp;/g, "&");
}

function report(e, t, n) {
var r = e.split("?").pop();
r = r.split("#").shift();
if (r == "") return;
var i = [ r, "action_type=" + n, "uin=" + t ].join("&");
ajax({
url: "/mp/appmsg/show",
type: "POST",
timeout: 2e3,
data: i
});
}

function reportTimeOnPage() {
var e = location.href, t = e.split("?").pop();
t = t.split("#").shift();
if (t == "") return;
var n = [ t, "start_time=" + _wxao.begin, "end_time=" + (new Date).getTime(), "uin=" + fakeid, "title=" + encodeURIComponent(title), "action=pagetime" ].join("&");
ajax({
url: "/mp/appmsg/show?" + n,
async: !1,
timeout: 2e3
});
}

function share_scene(e, t) {
var n = "";
tid != "" && (n = "tid=" + tid + "&aid=" + 54);
var r = e.split("?")[1] || "";
r = r.split("#")[0];
if (r == "") return;
var i = [ r, "scene=" + t ];
return n != "" && i.push(n), r = i.join("&"), e.split("?")[0] + "?" + r + "#" + (e.split("#")[1] || "");
}

function get_url(e, t) {
t = t || "";
var n = e.split("?")[1] || "";
n = n.split("#")[0];
if (n == "") return;
var r = [ n ];
return t != "" && r.push(t), n = r.join("&"), e.split("?")[0] + "?" + n + "#" + (e.split("#")[1] || "");
}

function viewProfile() {
typeof WeixinJSBridge != "undefined" && WeixinJSBridge.invoke && WeixinJSBridge.invoke("profile", {
username: user_name,
scene: "57"
});
}

function gdt_click(e, t, n, r, i, s, o, u, a) {
if (has_click[i]) return;
has_click[i] = !0;
var f = document.getElementById("loading_" + i);
f && (f.style.display = "inline");
var l = +(new Date);
ajax({
url: "/mp/advertisement_report?report_type=2&type=" + e + "&url=" + encodeURIComponent(t) + "&tid=" + i + "&rl=" + encodeURIComponent(n) + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&pt=" + a + "&r=" + Math.random() + abtest_param,
type: "GET",
timeout: 1e3,
complete: function(n) {
has_click[i] = !1, f && (f.style.display = "none");
if (e == "5") location.href = "/mp/profile?source=from_ad&tousername=" + t + "&ticket=" + s + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&tid=" + i; else {
var r = "source=4&tid=" + i + "&idx=" + idx + "&mid=" + mid + "&appuin=" + biz + "&pt=" + a + "&aid=" + u;
t.indexOf("mp.weixin.qq.com") == -1 && (r = ""), location.href = get_url(t, r);
}
},
async: !0
});
}

function addEvent(e, t, n) {
window.addEventListener ? e.addEventListener(t, n, !1) : window.attachEvent ? e.attachEvent("on" + t, function(e) {
return function(t) {
n.call(e, t);
};
}(e)) : e["on" + t] = n;
}

function log(e) {
var t = document.getElementById("log");
if (t) {
var n = t.innerHTML;
t.innerHTML = n + "<div>" + e + "</div>";
}
}

function initpicReport() {
function e(e) {
var t = [];
for (var n in e) t.push(n + "=" + encodeURIComponent(e[n] || ""));
return t.join("&");
}
if (!networkType) return;
var t = window.performance || window.msPerformance || window.webkitPerformance, n = null;
if (!t || typeof t.getEntries == "undefined") return;
var r, i = 100, s = document.getElementsByTagName("img"), o = s.length, u = navigator.userAgent, a, f = !1;
/micromessenger\/(\d+\.\d+)/i.test(u), a = RegExp.$1;
for (var l = 0, c = s.length; l < c; l++) {
r = parseInt(Math.random() * 100);
if (r > i) continue;
var h = s[l].getAttribute("src");
if (h.indexOf("mp.weixin.qq.com") >= 0) continue;
var p = t.getEntries(), d;
for (var v = 0; v < p.length; v++) {
d = p[v];
if (d.name == h) {
ajax({
type: "POST",
url: "/mp/appmsgpicreport?__biz=" + biz + "&uin=" + uin + "&key=" + key + "#wechat_redirect",
data: e({
rnd: Math.random(),
uin: uin,
version: version,
client_version: a,
device: navigator.userAgent,
time_stamp: parseInt(+(new Date) / 1e3),
url: h,
img_size: s[l].fileSize || 0,
user_agent: navigator.userAgent,
net_type: networkType,
appmsg_id: window.appmsgid || "",
sample: o > 100 ? 100 : o,
delay_time: parseInt(d.duration)
})
}), f = !0;
break;
}
}
if (f) break;
}
}

var ISWP = !!navigator.userAgent.match(/Windows\sPhone/i), sw = 0, imgs_info = {}, abtest_param = "&ab_test_id=color&ab_test_value=gray", outsize_link = 0;

(function() {
parseInt(readNum) > 1e5 ? readNum = "100000+" : "", parseInt(likeNum) > 1e5 ? likeNum = "100000+" : "", document.getElementById("readNum") && (document.getElementById("readNum").innerHTML = readNum), document.getElementById("likeNum") && (document.getElementById("likeNum").innerHTML = likeNum);
})(), function() {
var e = document.getElementById("js_content");
if (!e) return !1;
var t = e.getElementsByTagName("a") || [];
for (var n = 0, r = t.length; n < r; ++n) (function(e) {
var n = t[e], r = n.getAttribute("href");
if (!r) return !1;
r.indexOf("http://mp.weixin.qq.com") != 0 && r.indexOf("http://mp.weixin.qq.com") != 0 && outsize_link++;
var i = 0, s = n.innerHTML;
/^[^<>]+$/.test(s) ? i = 1 : /^<img[^>]*>$/.test(s) && (i = 2), r.indexOf("http://mp.weixin.qq.com/mp/redirect") != 0 && (r = "http://" + location.host + "/mp/redirect?url=" + encodeURIComponent(r) + "&action=appmsg_redirect" + "&uin=" + uin + "&biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&type=" + i + "&scene=0"), n.addEventListener ? n.addEventListener("click", function(e) {
e.stopPropagation && e.stopPropagation(), e.preventDefault && e.preventDefault(), location.href = r;
}, !0) : n.attachEvent && n.attachEvent("click", function(e) {
e.stopPropagation && e.stopPropagation(), e.preventDefault && e.preventDefault(), location.href = r;
}, !0);
})(n);
}(), function() {
var e = document.getElementById("gdt_area");
if (abtest == 1 && !!e) {
var t = e.getElementsByTagName("a");
for (var n = 0, r = t.length; n < r; ++n) t[n].setAttribute("class", "random_empha");
}
}(), function() {
var e = document.getElementById("js_pc_qr_code_img");
if (!!e && navigator.userAgent.indexOf("MicroMessenger") == -1) {
var t = 10000004, n = document.referrer;
n.indexOf("http://weixin.sogou.com") == 0 ? t = 10000001 : n.indexOf("https://wx.qq.com") == 0 && (t = 10000003), e.setAttribute("src", "/mp/qrcode?scene=" + t + "&size=102&__biz=" + biz), document.getElementById("js_pc_qr_code").style.display = "block";
}
}(), function() {
var e = document.getElementById("gdt_area");
!!e && navigator.userAgent.indexOf("MicroMessenger") == -1 && (e.style.display = "none");
}(), function() {
function e(e) {
var t = 0;
e.contentDocument && e.contentDocument.body.offsetHeight ? t = e.contentDocument.body.offsetHeight : e.Document && e.Document.body && e.Document.body.scrollHeight ? t = e.Document.body.scrollHeight : e.document && e.document.body && e.document.body.scrollHeight && (t = e.document.body.scrollHeight);
var n = e.parentElement;
!n || (e.style.height = t + "px");
}
var t = document.getElementsByTagName("iframe"), n;
for (var r = 0, i = t.length; r < i; ++r) {
n = t[r];
var s = n.getAttribute("data-src"), o = n.getAttribute("class");
!!s && (s.indexOf("http://mp.weixin.qq.com/mp/appmsgvote") == 0 && o.indexOf("js_editor_vote_card") >= 0 || s.indexOf("http://mp.weixin.qq.com/bizmall/appmsgcard") == 0 && o.indexOf("card_iframe") >= 0) && (n.setAttribute("src", s.replace("#wechat_redirect", [ "&uin=", uin, "&key=", key ].join(""))), function(t) {
t.onload = function() {
e(t);
};
}(n), n.appmsg_idx = r);
}
window.iframe_reload = function(r) {
for (var i = 0, s = t.length; i < s; ++i) {
n = t[i];
var o = n.getAttribute("src");
!!o && o.indexOf("http://mp.weixin.qq.com/mp/appmsgvote") == 0 && e(n);
}
};
}();

if (ISWP) {
var profile = document.getElementById("post-user");
profile && profile.setAttribute("href", "weixin://profile/" + user_name);
}

var cookie = {
get: function(e) {
if (e == "") return "";
var t = new RegExp(e + "=([^;]*)"), n = document.cookie.match(t);
return n && n[1] || "";
},
set: function(e, t) {
var n = new Date;
n.setDate(n.getDate() + 1);
var r = n.toGMTString();
return document.cookie = e + "=" + t + ";expires=" + r, !0;
}
}, title = trim(htmlDecode(msg_title)), sourceurl = trim(htmlDecode(msg_source_url));

msg_link = htmlDecode(msg_link), function() {
function e() {
var e = "", t = msg_cdn_url, n = msg_link, r = htmlDecode(msg_title), i = htmlDecode(msg_desc);
i = i || n, WeixinJSBridge.call("hideToolbar"), "1" == is_limit_user && WeixinJSBridge.call("hideOptionMenu"), WeixinJSBridge.on("menu:share:appmessage", function(s) {
var o = 1;
s.scene == "favorite" && (o = 4), WeixinJSBridge.invoke("sendAppMessage", {
appid: e,
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, o),
desc: i,
title: r
}, function(e) {
report(n, fakeid, o);
});
}), WeixinJSBridge.on("menu:share:timeline", function(e) {
report(n, fakeid, 2), WeixinJSBridge.invoke("shareTimeline", {
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, 2),
desc: i,
title: r
}, function(e) {});
});
var s = "";
WeixinJSBridge.on("menu:share:weibo", function(e) {
WeixinJSBridge.invoke("shareWeibo", {
content: r + share_scene(n, 3),
url: share_scene(n, 3)
}, function(e) {
report(n, fakeid, 3);
});
}), WeixinJSBridge.on("menu:share:facebook", function(e) {
report(n, fakeid, 4), WeixinJSBridge.invoke("shareFB", {
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, 4),
desc: i,
title: r
}, function(e) {});
}), WeixinJSBridge.on("menu:general:share", function(s) {
var o = 0;
switch (s.shareTo) {
case "friend":
o = 1;
break;
case "timeline":
o = 2;
break;
case "weibo":
o = 3;
}
s.generalShare({
appid: e,
img_url: t,
img_width: "640",
img_height: "640",
link: share_scene(n, o),
desc: i,
title: r
}, function(e) {
report(n, fakeid, o);
});
});
var o = {
"network_type:fail": "fail",
"network_type:edge": "2g",
"network_type:wwan": "3g",
"network_type:wifi": "wifi"
};
typeof WeixinJSBridge != "undefined" && WeixinJSBridge.invoke && WeixinJSBridge.invoke("getNetworkType", {}, function(e) {
networkType = o[e.err_msg], initpicReport();
});
}
typeof WeixinJSBridge == "undefined" ? document.addEventListener ? document.addEventListener("WeixinJSBridgeReady", e, !1) : document.attachEvent && (document.attachEvent("WeixinJSBridgeReady", e), document.attachEvent("onWeixinJSBridgeReady", e)) : e();
}(), function() {
var e = null, t = 0, n = msg_link.split("?").pop(), r = hash(n);
window.addEventListener ? (window.addEventListener("load", function() {
t = cookie.get(r), window.scrollTo(0, t);
}, !1), window.addEventListener("unload", function() {
cookie.set(r, t), reportTimeOnPage();
}, !1), window.addEventListener("scroll", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1), document.addEventListener("touchmove", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1)) : window.attachEvent && (window.attachEvent("load", function() {
t = cookie.get(r), window.scrollTo(0, t);
}, !1), window.attachEvent("unload", function() {
cookie.set(r, t), reportTimeOnPage();
}, !1), window.attachEvent("scroll", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1), document.attachEvent("touchmove", function() {
clearTimeout(e), e = setTimeout(function() {
t = window.pageYOffset;
}, 500);
}, !1));
}(), function() {
function e(e) {
typeof window.WeixinJSBridge != "undefined" && WeixinJSBridge.invoke("imagePreview", {
current: e,
urls: n
});
}
function t() {
var t = document.getElementById("img-content");
t = t ? t.getElementsByTagName("img") : [];
for (var r = 0, i = t.length; r < i; r++) {
var s = t.item(r), o = s.getAttribute("data-src") || s.getAttribute("src");
o && (!!s.dataset && !!s.dataset.s && o.indexOf("http://mmbiz.qpic.cn") == 0 && (o = o.replace(/\/640$/, "/0")), n.push(o), function(t) {
s.addEventListener ? s.addEventListener("click", function() {
e(t);
}, !1) : s.attachEvent && s.attachEvent("click", function() {
e(t);
}, !1);
}(o));
}
}
var n = [];
window.addEventListener ? window.addEventListener("load", t, !1) : window.attachEvent && (window.attachEvent("load", t), window.attachEvent("onload", t));
}();

var has_click = {};

(function() {
function detect() {
var e = (window.pageYOffset || document.documentElement.scrollTop) - 20, t = +(new Date);
for (var n = 0, r = images.length; n < r; n++) {
var i = images[n];
imgs_info[t] = imgs_info[t] || [];
var s = i.el.offsetTop;
if (!i.show && e < s + i.height && e + height > s) {
var o = i.src;
!!i.el.dataset && !!i.el.dataset.s && o.indexOf("http://mmbiz.qpic.cn") == 0 && (o = o.replace(/\/0$/, "/640")), i.el.onload = function() {
var e = this;
setProperty(e, "height", "auto", "important");
}, i.el.setAttribute("src", o), imgs_info[t].push(o), i.show = !0, setProperty(i.el, "visibility", "visible", "important");
}
ISWP && i.el.width * 1 > sw && (i.el.width = sw);
}
}
function onScroll() {
clearTimeout(timer), timer = setTimeout(detect, 300);
var performance = window.performance || window.msPerformance || window.webkitPerformance;
!has_report_img_time && !!performance && typeof performance.getEntries != "undefined" && function() {
var e = window.pageYOffset || document.documentElement.scrollTop, t = document.getElementById("js_toobar"), n = t.offsetTop;
if (e + innerHeight >= n) {
var r = performance.getEntries(), i = new Image, s = "http://isdspeed.qq.com/cgi-bin/r.cgi?flag1=7839&flag2=7&flag3=1", o = 0, u = 0, a = 0, f = 0, l = [], c = [], h = {};
for (var p = 0, d = r.length; p < d; ++p) {
var v = r[p], m = v.name;
!!m && v.initiatorType == "img" && (m.indexOf("http://mmbiz.qpic.cn") == 0 && (u++, f += v.duration), o++, a += v.duration, h[m] = {
startTime: v.startTime,
responseEnd: v.responseEnd
});
}
if (o > 0 || u > 0) {
o > 0 && (c.push("1=" + Math.round(a / o)), c.push("2=" + o * 1e3), c.push("3=" + Math.round(a))), u > 0 && (c.push("4=" + Math.round(f / u)), c.push("5=" + u * 1e3), c.push("6=" + Math.round(f)));
var g = 0, y = 0, b = 0;
for (var p in imgs_info) if (imgs_info.hasOwnProperty(p)) {
var w = imgs_info[p], E = 0, S = Infinity, x = 0, T = Infinity, N = !1, C = !1;
for (var k = 0, L = w.length; k < L; ++k) {
var m = w[k], A = h[m];
!A || (N = !0, E = Math.max(E, A.responseEnd), S = Math.min(S, A.startTime), m.indexOf("http://mmbiz.qpic.cn") == 0 && (x = Math.max(x, A.responseEnd), T = Math.min(T, A.startTime), C = !0));
}
N && (y += Math.round(E - S)), C && (b += Math.round(x - T));
}
c.push("7=" + y), c.push("8=" + b), i.src = s + "&" + c.join("&");
}
has_report_img_time = !0;
}
}();
var gdt_area = document.getElementById("gdt_area");
if (!!gdt_area && !ping_apurl) {
var scrollTop = window.pageYOffset || document.documentElement.scrollTop, offsetTop = gdt_area.offsetTop;
if (scrollTop + innerHeight > offsetTop) {
var gdt_a = document.querySelectorAll("#gdt_area a");
if (gdt_a.length) {
gdt_a = gdt_a[0];
if (gdt_a.dataset && gdt_a.dataset.apurl) {
ping_apurl = !0;
var gid = gdt_a.dataset.gid, tid = gdt_a.dataset.tid;
ajax({
url: "/mp/advertisement_report?report_type=1&tid=" + tid + "&adver_group_id=" + gid + "&apurl=" + encodeURIComponent(gdt_a.dataset.apurl) + "&uin=" + uin + "&key=" + key + "&__biz=" + biz + "&r=" + Math.random() + abtest_param,
success: function(res) {
try {
res = eval("(" + res + ")");
} catch (e) {
res = {};
}
res && res.ret != 0 && (ping_apurl = !1);
},
async: !0
});
}
}
}
}
}
function onLoad() {
var e = document.getElementsByTagName("img"), t = document.getElementById("page-content");
t.currentStyle ? sw = t.currentStyle.width : typeof getComputedStyle != "undefined" && (sw = getComputedStyle(t).width), sw = 1 * sw.replace("px", "");
for (var n = 0, r = e.length; n < r; n++) {
var i = e.item(n);
if (!i.getAttribute("data-src")) continue;
images.push({
el: i,
src: i.getAttribute("data-src"),
height: i.offsetHeight,
show: !1
});
var s = 100;
if (!!i.dataset && !!i.dataset.ratio) {
var o = i.dataset.ratio * 1, u = i.dataset.w * 1 || imgMaxW;
typeof o == "number" && o > 0 && (u = i.offsetWidth || (u <= imgMaxW ? u : imgMaxW), s = u * o);
}
setProperty(i, "height", s + "px", "important"), setProperty(i, "visibility", "hidden", "important");
}
detect(), initpicReport();
}
var timer = null, innerHeight = window.innerHeight || document.documentElement.clientHeight, imgMaxW = document.getElementById("page-content").offsetWidth, height = innerHeight + 40, images = [], ping_apurl = !1, has_report_img_time = !1;
window.addEventListener ? (window.addEventListener("scroll", onScroll, !1), window.addEventListener("load", onLoad, !1), document.addEventListener("touchmove", onScroll, !1)) : (window.attachEvent("onscroll", onScroll), window.attachEvent("onload", onLoad));
})(), function() {
function e() {
var e = document.getElementsByTagName("a"), t = !1;
if (!!e) {
var r = e.length;
for (var i = 0; i < r; ++i) {
var s = e[i], o = s.getAttribute("href"), u = /^(http|https):\/\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)(\/)?/g, a = u.exec(o);
a && a[2] && a[2] != "mp.weixin.qq.com" && (t = t || [], t.push(o), n.push(encodeURIComponent(o)));
}
}
return t.length == outsize_link ? !1 : t;
}
function t() {
var e = document.getElementsByTagName("form");
if (e) for (var t = 0; t < e.length; ++t) {
var n = e[t];
if (n) {
var i = n.outerHTML || "";
r.push(encodeURIComponent(n.getAttribute("action") + i.substr(0, 400)));
}
}
return e ? e.length : 0;
}
var n = [], r = [], i = [];
e() && i.push(1), t() && i.push(2), i.length > 0 && (n = "&url=" + n.join("||"), r = "&furl=" + r.join("||"), ajax({
url: "/mp/hijack?type=" + i.join(",") + "&r=" + Math.random() + n + r,
type: "POST",
timeout: 2e3,
data: ""
}));
}();</script>
        <script type="text/javascript">
        if (Math.random() < 0.1){
            var page_endtime = (+new Date());
            var page_time = page_endtime - _wxao.begin;
            var is_link = "2" == "1";
            var oss_key = is_link ? 9 : 10;

            //var report_time_url = 'https://mp.weixin.qq.com/misc/jsreport?type=avg&23521_' + oss_key + '=' + page_time + '&23521_15=1&23521_16=1&23521_17=3&23521_15|23521_16|23521_17=1&r=' + Math.random();
            var report_time_url = 'http://isdspeed.qq.com/cgi-bin/r.cgi?flag1=7839&flag2=7&flag3=1&' + oss_key + '=' + page_time;
            var _img = new Image(1, 1);
            _img.src = report_time_url;
        }
    </script>
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1252997000'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/stat.php%3Fid%3D1252997000' type='text/javascript'%3E%3C/script%3E"));</script>        
</body>
</html>

