<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.pineapple.collection.*" %>
<%@ page import="com.pineapple.entity.*" %>
<%@ page import="com.pineapple.util.*"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin</title>
</head>
<body>
<h1>Nodes <% out.write(NodeList.getInstance().getTheList().size()+""); %>, Links <% out.write(LinkList.getInstance().getTheList().size()+""); %></h1><br/>
<h1>Nodes in DB: <% out.write(Util.getRowNumFromDB(Node.TABLE_NAME)+""); %>, Links in DB: <% out.write(Util.getRowNumFromDB(Link.TABLE_NAME)+""); %></h1><br/>
<a href="action.jsp?action=save">Save</a><br/>
<a href="action.jsp?action=load">Load</a><br/>
<a href="showList.jsp">Go showList</a><br/>
</body>
</html>