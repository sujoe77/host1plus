<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.pineapple.util.*"%>
<%@ page import="com.pineapple.entity.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Page</title>
</head>
<body>
	<%
		String action = request.getParameter("action");
		int nodeCount = 0, linkCount = 0;
		if ("save".equals(action)) {
			nodeCount = Util.writeListToDB(Node.TABLE_NAME, false);
			linkCount = Util.writeListToDB(Link.TABLE_NAME, false);
		}
		if ("load".equals(action)) {
			nodeCount = Util.loadListFromDB(Node.TABLE_NAME);
			linkCount = Util.loadListFromDB(Link.TABLE_NAME);
		}
		out.write(action + " nodes: " + nodeCount + "<br/>");
		out.write(action + " links: " + linkCount);
	%><br/>
	<a href="Admin.jsp">Back</a>
</body>
</html>