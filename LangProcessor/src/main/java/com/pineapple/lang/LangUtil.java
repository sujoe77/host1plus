package com.pineapple.lang;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;

public class LangUtil {

	public static final Charset CHARSET_UTF8 = Charset.forName("UTF-8");
	private static final int MAX_LENGTH = 200;
	private static final int MIN_LENGTH = 180;
	private static final String DASH_NEWLINE = "-\n";
	private static final String CRLF = "\r\n";
	private static final String LF = "\n";
	private static final String DOUBLE_NEWLINE = "\n\n";

	public static void main(String[] args) throws IOException {
		String ret = FileUtils.readFileToString(new File(args[0]), CHARSET_UTF8);
		ret = reformat(ret);
		System.out.println(ret);
		FileUtils.write(new File(args[1]), ret, CHARSET_UTF8);
	}

	public static String reformat(String input) {
		String ret = toUnixFormat(input);
		String[] paragraphs = splitParagraph(ret);
		StringBuilder sb = new StringBuilder("");
		for (String paragraph : paragraphs) {
			sb.append(getParagrahStr(paragraph));
		}
		return sb.toString();
	}

	protected static String getParagrahStr(String paragraph) {
		String pStr = reconnectSplitedWord(paragraph);
		pStr = getReformattedParagraph(paragraph, MAX_LENGTH, MIN_LENGTH);
		return pStr + DOUBLE_NEWLINE;
	}

	protected static String getReformattedParagraph(String input, int maxLength, int minLength) {
		StringBuilder sb = new StringBuilder("");
		String paragrah = input.replace("\n", " ");
		while (paragrah.length() > maxLength) {
			int idx = minLength;
			String section = paragrah.substring(idx, maxLength);
			idx += section.indexOf(" ");
			sb.append(paragrah.substring(0, idx)).append(LF);
			paragrah = paragrah.substring(idx + 1);// remove heading space
		}
		sb.append(paragrah);
		return sb.toString();
	}

	protected static String[] splitParagraph(String input) {
		String ret = toUnixFormat(input);
		return ret.split(DOUBLE_NEWLINE);
	}

	protected static String reconnectSplitedWord(String input) {
		return input.replace(DASH_NEWLINE, "");
	}

	protected static String toUnixFormat(String input) {
		return input.replace(CRLF, LF);
	}
}
